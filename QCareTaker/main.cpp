#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "test.h"

#include <QElapsedTimer>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    // Initialize QKnx
    QKnxGlobal::init();

    // testing
    Test test;
    test.test_server();

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
