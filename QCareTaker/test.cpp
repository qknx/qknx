#include "test.h"

#include <QElapsedTimer>
#include <QSharedPointer>

#include "qknxstream.h"
#include "netip/qknxframe.h"

#include "netip/qknxnetwork.h"

#include "io/qknxudp.h"

#include "qknxqueue.h"

#include "types/qknxtypes.h"

#include <QThread>
#include <QQueue>

Test::Test(QObject *parent) : QObject(parent)
{
    m_Timer.setSingleShot(false);
    m_Timer.setInterval(1);
    //m_Timer.start();
    connect(&m_Timer,SIGNAL(timeout()),this,SLOT(handleTimer()));

    // connect knx server
    connect(&m_Server,SIGNAL(telegramReceived(QKnxTelegram)),this,SLOT(acceptTelegram(QKnxTelegram)));

    m_UdpFrames = 0;
}

void Test::test_server()
{

    QString search_res = "06100202004c0801c0a80ac80e573601020000010000010203040506e000170c08002776d05d6b6e786400000000000000000000000000000000000000000000000000000802020104010501";
    QByteArray data_in = QByteArray::fromHex(search_res.toLatin1());
    QByteArray data_out;

    //QKnxNetIp::HeaderLength header_len_res = QKnxNetIp::HeaderLengthNone;
    //QKnxNetIp::ProtocolVersion protocol_version_res = QKnxNetIp::ProtocolVersionNone;
    //QKnxNetIp::ServiceType service_type_res = QKnxNetIp::SERVICE_NONE;

    int n = 1000000;
    //int n = 1000;
    //int n = 1;

    QElapsedTimer t;
    t.start();

    for (int c = 0; c < n; c++) {

    }

    qint64 nsecs = t.nsecsElapsed();

    qDebug() << "Elapsed:" << (double)nsecs / 1000 << "usecs ( per object:" << (double)nsecs / 1000 / n << ")";

    // start server
    m_Server.setProtocol(QAbstractSocket::AnyIPProtocol);
    m_Server.setModeServer();
    m_Server.setModeRouting();
    m_Server.setModeTunneling();
    m_Server.serverStart();

}

void Test::handleTimer()
{
    //testKnxSending();

    /*
    if (!m_UdpTimer.isValid()) {
        m_UdpTimer.start();
    }

    //qDebug() << "insert frame into cache";
    for (int i=0; i < 1000; i++) {
        QKnxFrame* f = new QKnxFrame;
        bool ret = m_Udp.writeFrame(f);
        if (!ret) {
            delete f;
        }
    }

    m_UdpFrames++;
    if (m_UdpFrames == 1000) {
        qDebug() << "Sent 1 mio frames in" << m_UdpTimer.restart() << "msecs";
        m_UdpFrames = 0;
    }
    */
}

void Test::acceptTelegram(QKnxTelegram tg)
{
    //qDebug() << "got telegram: from" << tg.source().toString() << "to" << tg.target().toString() << "value" << QString(tg.content().toHex());
}

void Test::testKnxSending()
{
    QKnxTelegram tg;
    tg.setSource((quint16)27);
    tg.setTarget((quint16)11);
    tg.setContent(QByteArray::fromHex("ACAB"));
    m_Server.writeTelegram(tg);
}

/*
void Test::test(QKnxNetIpSearchResponse resp)
{
    qDebug() << "Got response from:" << resp.localAddress();
    qDebug() << "Endpoint:";
    qDebug() << " -" << resp.endpoint().hostAddress() << "@" << resp.endpoint().port();
    qDebug() << " -" << resp.endpoint().hostProtocolCodeString();
    qDebug() << "Device Info";
    //QKnxNetIpDeviceInfo* dev = resp.dibMap().dataPtr<QKnxNetIpDeviceInfo>();
    //QKnxNetIpDibMap map = resp.dibMap();
    //QKnxNetIpDeviceInfo* dev = map.dataPtr<QKnxNetIpDeviceInfo>();
    QKnxNetIpDeviceInfo* dev = resp.dibPtr<QKnxNetIpDeviceInfo>();
    QKnxNetIpDeviceInfo dev2 = resp.dib<QKnxNetIpDeviceInfo>();
    if (dev) {
        qDebug() << "1-" << dev->mediumsString();
        qDebug() << "1-" << dev->serialString();
        qDebug() << "1-" << dev->macAddress().toString();
        qDebug() << "1-" << dev->multicastAddress();
    }
    qDebug() << "2-" << dev2.mediumsString();
    qDebug() << "2-" << dev2.serialString();
    qDebug() << "2-" << dev2.macAddress().toString();
    qDebug() << "2-" << dev2.multicastAddress();
}
*/
