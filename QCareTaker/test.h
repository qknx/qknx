#ifndef TEST_H
#define TEST_H

#include <QObject>

#include "netip/qknxserver.h"

#include "io/qknxudp.h"

#include <QElapsedTimer>
#include <QNetworkConfigurationManager>
#include <QNetworkSession>
#include <QTimer>

class Test : public QObject
{
    Q_OBJECT
public:
    explicit Test(QObject *parent = nullptr);

    void test_server();

signals:

public slots:

private:
    QKnxServer m_Server;

    QTimer m_Timer;

    QKnxUdp m_Udp;
    quint64 m_UdpFrames;
    QElapsedTimer m_UdpTimer;

private slots:
    void handleTimer();
    void acceptTelegram(QKnxTelegram tg);
    void testKnxSending();
};

#endif // TEST_H
