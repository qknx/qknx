TEMPLATE = subdirs

SUBDIRS += \
    QKnx \
    QKnxTest \
    QCareTaker

OTHER_FILES += \
    README.md

QKnxTest.depends = QKnx
QCareTaker.depends = QKnx
