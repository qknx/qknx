#-------------------------------------------------
#
# Project created by QtCreator 2017-05-22T17:45:44
#
#-------------------------------------------------

include(defaults.pri)

QT       -= gui

TARGET = QKnx
TEMPLATE = lib

DEFINES += QKNX_LIBRARY

SOURCES += \
    qknxglobal.cpp \
    qknxobject.cpp \
    qknxaddress.cpp \
    log/qknxlog.cpp \
    log/qknxlogconsole.cpp \
    log/qknxlogoutput.cpp \
    dpt/qknxdpt.cpp \
    dpt/qknxdptinfo.cpp \
    qknxtelegram.cpp \
    netip/dib/qknxdeviceinfo.cpp \
    netip/qknxmacaddress.cpp \
    netip/qknxendpoint.cpp \
    qknxstream.cpp \
    netip/dib/qknxsuppsvcs.cpp \
    netip/qknxserver.cpp \
    netip/qknxclient.cpp \
    netip/qknxnetwork.cpp \
    netip/qknxnetworkworker.cpp \
    netip/qknxtunnel.cpp \
    netip/qknxconnectinfo.cpp \
    netip/qknxnetip.cpp \
    netip/qknxframe.cpp \
    netip/qknxcemi.cpp \
    io/qknxio.cpp \
    io/qknxudp.cpp \
    io/qknxioworker.cpp \
    io/qknxudpworker.cpp \
    types/qknxfloat16.cpp \
    types/qknxdatetime.cpp \
    types/qknxdate.cpp \
    types/qknxtimeofday.cpp \
    types/qknxbool.cpp \
    types/qknxtypes.cpp


HEADERS += \
    qknxglobal.h \
    qknxobject.h \
    qknxaddress.h \
    log/qknxlog.h \
    log/qknxlogoutput.h \
    log/qknxlogmessage.h \
    log/qknxlogconsole.h \
    dpt/qknxdpt.h \
    dpt/qknxdpt1bit.h \
    dpt/qknxdpt2bit.h \
    dpt/qknxdptinfo.h \
    qknxtelegram.h \
    netip/qknxnetip.h \
    netip/dib/qknxdeviceinfo.h \
    netip/qknxendpoint.h \
    netip/qknxmacaddress.h \
    qknxstream.h \
    netip/dib/qknxsuppsvcs.h \
    netip/qknxserver.h \
    netip/qknxclient.h \
    netip/qknxnetwork.h \
    netip/qknxnetworkworker.h \
    netip/qknxtunnel.h \
    netip/qknxconnectinfo.h \
    netip/qknxframe.h \
    netip/qknxcemi.h \
    io/qknxio.h \
    io/qknxudp.h \
    io/qknxioworker.h \
    io/qknxudpworker.h \
    qknxqueue.h \
    dpt/qknxdpt4bit.h \
    dpt/qknxdpt1byte.h \
    dpt/qknxdpt2byte.h \
    dpt/qknxdpt3byte.h \
    dpt/qknxdpt4byte.h \
    dpt/qknxdpt8byte.h \
    dpt/qknxdpt14byte.h \
    types/qknxfloat16.h \
    types/qknxdatetime.h \
    types/qknxdate.h \
    types/qknxtimeofday.h \
    types/qknxbool.h \
    types/qknxtypes.h



unix {
    target.path = /usr/lib
    INSTALLS += target
}

