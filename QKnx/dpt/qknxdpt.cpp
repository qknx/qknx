#include "qknxdpt.h"
#include "qknxstream.h"

#include "qknxdpt1bit.h"
#include "qknxdpt2bit.h"
#include "qknxdpt4bit.h"
#include "qknxdpt1byte.h"
#include "qknxdpt2byte.h"
#include "qknxdpt3byte.h"
#include "qknxdpt4byte.h"
#include "qknxdpt8byte.h"
#include "qknxdpt14byte.h"

#include "types/qknxtypes.h"

#include <QMetaEnum>
#include <QMutex>
#include <QMutexLocker>
#include <QCache>

#include <climits>

static bool registered = QKnxDpt::registerDpts();

QKnxDpt::QKnxDpt() : m_BasicProperties(), m_ExtendedProperties(Q_NULLPTR)
{

}

QKnxDpt::QKnxDpt(const QKnxDpt::Name &dpt_name) : m_BasicProperties(basicProperties(dpt_name)), m_ExtendedProperties(Q_NULLPTR)
{

}

QKnxDpt::~QKnxDpt()
{
    delete m_ExtendedProperties;
}

bool QKnxDpt::registerDpts()
{
    Q_UNUSED(registered)

    dptBasicPropertyMutex().lock();
    bool defined = !dptBasicPropertyCache().isEmpty();
    dptBasicPropertyMutex().unlock();

    if (defined)  {
        return true;
    }

    // register meta types for knx custom types
    registerType<QKnxBool>();

    registerType<QKnxChar>();
    registerType<QKnxInt8>();
    registerType<QKnxUInt8>();

    registerType<QKnxInt16>();
    registerType<QKnxUInt16>();

    registerType<QKnxInt32>();
    registerType<QKnxUInt32>();

    registerType<QKnxFloat16>();
    registerType<QKnxFloat32>();

    registerType<QKnxDate>();
    registerType<QKnxTimeOfDay>();
    registerType<QKnxDateTime>();

    // register different datapoint groups
    QKnxDpt1Bit::registerDpts();
    QKnxDpt2Bit::registerDpts();
    QKnxDpt4Bit::registerDpts();
    QKnxDpt1Byte::registerDpts();
    QKnxDpt2Byte::registerDpts();
    QKnxDpt3Byte::registerDpts();
    QKnxDpt4Byte::registerDpts();
    QKnxDpt8Byte::registerDpts();
    QKnxDpt14Byte::registerDpts();

    // debug only, remove
    qDebug() << "reg count:" << dptBasicPropertyCache().count();
    QMap<Name, Properties::Basic> pmap;
    foreach (Name dpt_name, dptBasicPropertyCache().keys())
    {
        Properties::Basic* p = dptBasicPropertyCache().object(dpt_name);
        if (!p) {
            continue;
        }
        pmap.insert(dpt_name,*p);
    }

    /*
    int bitlen = 0;
    foreach (Properties::Basic p, pmap.values()) {
        if (bitlen != p.bitLength) {
            bitlen = p.bitLength;
            qDebug() << "\nBitlength =" << bitlen << "\n";
        }

        Properties::Extended* pe = dptExtendedPropertyCache().object(p.name);
        if (!pe) {
            continue;
        }

        qDebug() << nameString(p.name).toStdString().c_str();
        qDebug() << " - min:" << pe->minValue << " max:" << pe->maxValue;
        if (p.flags & HasScaling) {
            qDebug() << " - scaleMin:" << pe->minScaled << " scaleMax:" << pe->maxScaled;
        }
        qDebug() << "";
    }
    */

    return true;
}

QKnxDpt::Properties::Basic QKnxDpt::basicProperties(const QKnxDpt::Name &dpt_name)
{
    QMutexLocker lock(&dptBasicPropertyMutex());
    Properties::Basic* prop_ptr = dptBasicPropertyCache().object(dpt_name);
    if (prop_ptr)
    {
        return *prop_ptr;
    }
    return Properties::Basic();
}

QKnxDpt::Properties::Extended QKnxDpt::extendedProperties(const QKnxDpt::Name &dpt_name)
{
    QMutexLocker lock(&dptExtendedPropertyMutex());
    Properties::Extended* prop_ptr = dptExtendedPropertyCache().object(dpt_name);
    if (prop_ptr)
    {
        return *prop_ptr;
    }
    return Properties::Extended();
}

void QKnxDpt::addProperties(const QKnxDpt::Properties& properties)
{
    if (properties.basic.name == DPT_None) {
        return;
    }

    // save basic properties
    QMutexLocker lock_basic(&dptBasicPropertyMutex());
    if (dptBasicPropertyCache().maxCost() == dptBasicPropertyCache().count()) {
        dptBasicPropertyCache().setMaxCost(dptBasicPropertyCache().maxCost()+64);
    }
    dptBasicPropertyCache().insert(properties.basic.name,new QKnxDpt::Properties::Basic(properties.basic));

    // save extended properties
    QMutexLocker lock_extended(&dptExtendedPropertyMutex());
    if (dptExtendedPropertyCache().maxCost() == dptExtendedPropertyCache().count()) {
        dptExtendedPropertyCache().setMaxCost(dptExtendedPropertyCache().maxCost()+64);
    }
    dptExtendedPropertyCache().insert(properties.basic.name,new QKnxDpt::Properties::Extended(properties.extended));
}

QCache<QKnxDpt::Name, QKnxDpt::Properties::Basic> &QKnxDpt::dptBasicPropertyCache()
{
    static QCache<QKnxDpt::Name, QKnxDpt::Properties::Basic> basic_property_cache;
    return basic_property_cache;
}

QMutex &QKnxDpt::dptBasicPropertyMutex()
{
    static QMutex basic_property_mutex;
    return basic_property_mutex;
}

QCache<QKnxDpt::Name, QKnxDpt::Properties::Extended> &QKnxDpt::dptExtendedPropertyCache()
{
    static QCache<QKnxDpt::Name, QKnxDpt::Properties::Extended> extended_property_cache;
    return extended_property_cache;
}

QMutex &QKnxDpt::dptExtendedPropertyMutex()
{
    static QMutex extended_property_mutex;
    return extended_property_mutex;
}

QCache<QKnxDpt::Type, QKnxDpt::ValueType> &QKnxDpt::dptTypeObjectsCache()
{
    static QCache<QKnxDpt::Type, QKnxDpt::ValueType> type_objects_cache;
    return type_objects_cache;
}

QMutex &QKnxDpt::dptTypeObjectsMutex()
{
    static QMutex type_objects_mutex;
    return type_objects_mutex;
}

QKnxDpt::Properties::Extended *QKnxDpt::readExtendedProperties()
{
    if (m_ExtendedProperties) {
        return m_ExtendedProperties;
    }
    QMutexLocker lock(&dptExtendedPropertyMutex());
    Properties::Extended* prop_ptr = dptExtendedPropertyCache().object(m_BasicProperties.name);
    if (prop_ptr)
    {
        m_ExtendedProperties = new Properties::Extended(*prop_ptr);
    }
    return m_ExtendedProperties;
}

QKnxStream &operator>>(QKnxStream &fs, QKnxDpt &data)
{
    QKnxDpt::Type tx = data.m_BasicProperties.type;
    if (tx == QKnxDpt::TypeNone) {
        return fs;
    }

    QKnxDpt::ValueType* vt = Q_NULLPTR;
    data.dptTypeObjectsMutex().lock();
    QKnxDpt::ValueType* vt_tmp = data.dptTypeObjectsCache().object(tx);
    if (vt_tmp) {
        vt = vt_tmp->create();
    }
    data.dptTypeObjectsMutex().unlock();

    if (!vt) {
        return fs;
    }

    vt->deserialize(fs);
    data.m_Value.set(tx,vt);
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, QKnxDpt &data)
{
    QKnxDpt::ValueType* vt = data.m_Value.data();
    if (!vt) {
        return fs;
    }
    vt->serialize(fs);
    return fs;
}

//
// min() / max() template specializations
//

/*
// specialisation for QKnxFloat16
template<> QVariant QKnxDpt::min<QKnxFloat16>() { return QVariant::fromValue<QKnxFloat16>(QKnxFloat16::min()); }
template<> QVariant QKnxDpt::max<QKnxFloat16>() { return QVariant::fromValue<QKnxFloat16>(QKnxFloat16::max()); }

// specialisation for float
template<> QVariant QKnxDpt::min<float>() { return QVariant::fromValue<float>(-std::numeric_limits<float>::max()); }
template<> QVariant QKnxDpt::max<float>() { return QVariant::fromValue<float>(std::numeric_limits<float>::max()); }

// specialisation for QKnxTimeOfDay
template<> QVariant QKnxDpt::min<QKnxTimeOfDay>() { return QVariant::fromValue<QKnxTimeOfDay>(QKnxTimeOfDay::min()); }
template<> QVariant QKnxDpt::max<QKnxTimeOfDay>() { return QVariant::fromValue<QKnxTimeOfDay>(QKnxTimeOfDay::max()); }

// specialisation for QKnxDate
template<> QVariant QKnxDpt::min<QKnxDate>() { return QVariant::fromValue<QKnxDate>(QKnxDate::min()); }
template<> QVariant QKnxDpt::max<QKnxDate>() { return QVariant::fromValue<QKnxDate>(QKnxDate::max()); }

// specialisation for QKnxDateTime
template<> QVariant QKnxDpt::min<QKnxDateTime>() { return QVariant::fromValue<QKnxDateTime>(QKnxDateTime::min()); }
template<> QVariant QKnxDpt::max<QKnxDateTime>() { return QVariant::fromValue<QKnxDateTime>(QKnxDateTime::max()); }
*/

//
// serialize() / deserialize() template specializations
//

/*

QKnxDpt::QKnxDpt()
{

}

QKnxDpt::~QKnxDpt()
{

}

int QKnxDpt::dptMetaId()
{
    return QMetaType::type(dptClassName().append("*").toLatin1());
}

bool QKnxDpt::isValid()
{
    if (dptPrimaryId() < 0 || dptSecondaryId() < 0) {
        qDebug() << "invalid dpt. primary and/or secondary id < 0";
        return false;
    }
    if (!contentLengthCheck()) {
        qDebug() << "content length check failure.";
        return false;
    }
    bool test_value = false;
    QVariant val_tmp = value(&test_value);
    if (!test_value || !val_tmp.isValid()) {
        qDebug() << "value is invalid";
        return false;
    }
    return true;
}

int QKnxDpt::metaIdbyId(int dpt_id)
{
    QMutexLocker lock(&idMutex());
    return metaIdByIdHash().value(dpt_id,QMetaType::UnknownType);
}

int QKnxDpt::metaIdByClassName(QString dpt_class_name)
{
    QMutexLocker lock(&idMutex());
    return metaIdByClassNameHash().value(dpt_class_name,QMetaType::UnknownType);
}

int QKnxDpt::metaIdByIdName(QString dpt_id_name)
{
    QMutexLocker lock(&idMutex());
    return metaIdByIdNameHash().value(dpt_id_name,QMetaType::UnknownType);
}

int QKnxDpt::metaIdByDpst(QString dpt_dpst)
{
    QStringList list = dpt_dpst.split("_");
    if (list.size() != 3) {
        return QMetaType::UnknownType;
    }
    if (list.at(0) != "DPST") {
        return QMetaType::UnknownType;
    }

    QString num_str = list.at(1);
    num_str = num_str.append(list.at(2));
    return metaIdbyId(num_str.toInt());
}

int QKnxDpt::metaIdByIdString(QString dpt_id_str)
{
    QStringList list = dpt_id_str.split(".");
    if (list.size() != 2) {
        return QMetaType::UnknownType;
    }

    QString num_str = list.at(0);
    num_str = num_str.append(list.at(1));
    return metaIdbyId(num_str.toInt());
}

int QKnxDpt::metaIdByString(QString dpt_str)
{
    int type_id = metaIdByClassName(dpt_str);
    if (type_id != QMetaType::UnknownType) {
        return type_id;
    }

    type_id = metaIdByIdName(dpt_str);
    if (type_id != QMetaType::UnknownType) {
        return type_id;
    }

    type_id = metaIdByDpst(dpt_str);
    if (type_id != QMetaType::UnknownType) {
        return type_id;
    }

    type_id = metaIdByIdString(dpt_str);
    return type_id;
}

int QKnxDpt::idByMetaId(int meta_id)
{
    QMutexLocker lock(&idMutex());
    return metaIdByIdHash().key(meta_id,DPT_Unknown);
}

QString QKnxDpt::classNameByMetaId(int meta_id)
{
    QMutexLocker lock(&idMutex());
    return metaIdByClassNameHash().key(meta_id,"");
}

QString QKnxDpt::idNameByMetaId(int meta_id)
{
    QMutexLocker lock(&idMutex());
    return metaIdByIdNameHash().key(meta_id,"");
}

void QKnxDpt::registerDpts()
{
    registerDpt<QKnxDpt*>();
    QKnxDpt1Bit::registerDpts();
    QKnxDpt2Bit::registerDpts();
}

QKnxDpt *QKnxDpt::createByMetaId(int type_id)
{
    QMutexLocker lock(&idMutex());
    if (!metaIdCreateableHash().value(type_id,false)) {
        qDebug() << "type id " << type_id << " unknown or not createable";
        return 0;
    }

    const QMetaObject* m_obj = QMetaType::metaObjectForType(type_id);
    if (!m_obj) {
        qDebug() << "got nullptr meta object for type id" << type_id;
        return 0;
    }

    QKnxDpt* obj_ = qobject_cast<QKnxDpt*>(m_obj->newInstance());
    if (!obj_) {
        qDebug() << "got dpt nullptr object";
        return 0;
    }

    return obj_;
}

QString QKnxDpt::staticEnumValueString(QString enumName, int enumValue)
{
    int index = staticMetaObject.indexOfEnumerator(enumName.toStdString().c_str());
    if (index != -1) {
        return staticMetaObject.enumerator(index).valueToKey(enumValue);
    }
    return "";
}

QString QKnxDpt::dptDpstById(const int &dpt_id)
{
    QString id_str = QString::number(dpt_id);
    if (id_str.length() < 4) {
        qDebug() << "id to short";
        return "";
    }

    return QString("DPST_%1_%2").arg(id_str.left(id_str.length()-3)).arg(id_str.right(3));
}

QString QKnxDpt::dptIdStringById(const int &dpt_id)
{
    QString id_str = QString::number(dpt_id);
    if (id_str.length() < 4) {
        qDebug() << "id to short";
        return "";
    }

    return QString("%1.%2").arg(id_str.left(id_str.length()-3)).arg(id_str.right(3));
}

int QKnxDpt::dptPrimaryIdById(const int &dpt_id)
{
    QString id_str = QString::number(dpt_id);
    if (id_str.length() < 4) {
        qDebug() << "id to short";
        return -1;
    }
    return id_str.left(id_str.length()-3).toInt();
}

int QKnxDpt::dptSecondaryIdById(const int &dpt_id)
{
    QString id_str = QString::number(dpt_id);
    if (id_str.length() < 4) {
        qDebug() << "id to short";
        return -1;
    }
    return id_str.right(3).toInt();
}

bool QKnxDpt::contentLengthCheck(bool adjust_size)
{
    if (bitLength() < 1) {
        qDebug() << "bit length < 1";
        return false;
    }

    int bit_len = bitLength();
    if (bit_len < 8 ) {
        bit_len = 8;
    }

    int con_bit_len = contentLengthReal() * 8;
    if (bit_len == con_bit_len) {
        return true;
    }

    if (!adjust_size) {
        return false;
    }

    // size mismatch but adjusting required
    m_Content.clear();
    while (bit_len >= 8) {
        m_Content.append((char)0x00);
        bit_len -= 8;
    }
    return true;
}

bool QKnxDpt::rangeTest(QVariant value)
{
    if (value < min() || value > max() ) {
        qDebug() << "out of limits";
        return false;
    }
    return true;
}

QMutex &QKnxDpt::idMutex()
{
    static QMutex id_mutex;
    return id_mutex;
}

QHash<int, int> &QKnxDpt::metaIdByIdHash()
{
    static QHash<int,int> meta_id_by_dpt_id;
    return meta_id_by_dpt_id;
}

QHash<QString, int> &QKnxDpt::metaIdByClassNameHash()
{
    static QHash<QString, int> meta_id_by_class_name;
    return meta_id_by_class_name;
}

QHash<QString, int> &QKnxDpt::metaIdByIdNameHash()
{
    static QHash<QString, int> meta_id_by_name;
    return meta_id_by_name;
}

QHash<int, bool> &QKnxDpt::metaIdCreateableHash()
{
    static QHash<int, bool> meta_id_createable;
    return meta_id_createable;
}

bool QKnxDpt::registerDptMetaType(int type_id)
{
    //qDebug() << "registering type id:" << type_id;

    if (type_id == QMetaType::UnknownType) {
        qDebug() << "UnknowType id given";
        return false;
    }

    if (!QMetaType::isRegistered(type_id)) {
        qDebug() << "type id" << type_id << "not registered.";
        return false;
    }

    const QMetaObject* m_obj = QMetaType::metaObjectForType(type_id);
    if (!m_obj) {
        qDebug() << "got nullptr meta object for type id" << type_id;
        return false;
    }

    QKnxDpt* obj_ = qobject_cast<QKnxDpt*>(m_obj->newInstance());
    if (!obj_) {
        qDebug() << "got dpt nullptr object";
        return false;
    }

    QMutexLocker lock(&idMutex());
    if (obj_->dptId() != DPT_Unknown) {
        metaIdByIdHash().insert(obj_->dptId(),type_id);
    }
    if (!obj_->dptIdName().isEmpty() && obj_->dptIdName() != dptNameById(DPT_Unknown)) {
        metaIdByIdNameHash().insert(obj_->dptIdName(),type_id);
    }
    metaIdByClassNameHash().insert(obj_->dptClassName(),type_id);
    metaIdCreateableHash().insert(type_id,obj_->isCreateable());

    qDebug() << "registered: class:" << obj_->dptClassName() << "name:" << obj_->dptIdName() << "meta id:" << obj_->dptMetaId() << "id:" << obj_->dptId() << "createable:" << obj_->isCreateable();

    return true;
}

*/














