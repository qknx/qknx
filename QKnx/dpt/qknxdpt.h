#ifndef QKNXDPT_H
#define QKNXDPT_H

#include "qknxstream.h"

#include <QMetaEnum>
#include <QMutex>
#include <QCache>

class QKNX_SHARED_EXPORT QKnxDpt
{
    Q_GADGET
public:
    enum Name {
        DPT_None = 0,

        // 1-bit data point types
        DPT_Switch = 1001,
        DPT_Bool = 1002,
        DPT_Enable = 1003,
        DPT_Ramp = 1004,
        DPT_Alarm = 1005,
        DPT_BinaryValue = 1006,
        DPT_Step = 1007,
        DPT_UpDown = 1008,
        DPT_OpenClose = 1009,
        DPT_Start = 1010,
        DPT_State = 1011,
        DPT_Invert = 1012,
        DPT_DimSendStyle = 1013,
        DPT_InputSource = 1014,
        DPT_Reset = 1015,
        DPT_Ack = 1016,
        DPT_Trigger = 1017,
        DPT_Occupancy = 1018,
        DPT_Window_Door = 1019,
        DPT_LogicalFunction = 1021,
        DPT_Scene_AB = 1022,

        // 2-bit data point types
        DPT_Switch_Control = 2001,
        DPT_Bool_Control = 2002,
        DPT_Enable_Control = 2003,
        DPT_Ramp_Control = 2004,
        DPT_Alarm_Control = 2005,
        DPT_BinaryValue_Control = 2006,
        DPT_Step_Control = 2007,
        DPT_Direction1_Control = 2008,
        DPT_Direction2_Control = 2009,
        DPT_Start_Control = 2010,
        DPT_State_Control = 2011,
        DPT_Invert_Control = 2012,

        // 4-bit data point types
        DPT_Control_Dimming = 3007,
        DPT_Control_Blinds = 3008,
        DPT_Model_Boiler = 3009,

        // 8-bit character types
        DPT_Char_ASCII = 4001,
        DPT_Char_8859_1 = 4002,

        // 8-bit unsigned data point types
        DPT_Scaling = 5001,
        DPT_Angle = 5003,
        DPT_Percent_U8 = 5004,
        DPT_Value_1_Ucount = 5010,

        // 8-bit signed data point types
        DPT_Percent_V8 = 6001,
        DPT_Value_1_Count = 6010,

        // 2 byte unsigned data point types
        DPT_Value_2_Ucount = 7001,

        // 2 byte signed data point types
        DPT_Value_2_Count = 8001,
        DPT_Percent_V16 = 8010,

        // 2 byte float data point types
        DPT_Value_Temp = 9001,
        DPT_Value_Tempd = 9002,
        DPT_Value_Tempa = 9003,
        DPT_Value_Lux = 9004,
        DPT_Value_Wsp = 9005,
        DPT_Value_Pres = 9006,
        DPT_Value_Humidity = 9007,
        DPT_Value_AirQuality = 9008,
        DPT_Value_Time1 = 9010,
        DPT_Value_Time2 = 9011,
        DPT_Value_Volt = 9020,
        DPT_Value_Curr = 9021,
        DPT_PowerDensity = 9022,
        DPT_KelvinPerPercent = 9023,
        DPT_Power = 9024,
        DPT_Value_Volume_Flow = 9025,

        // time data types
        DPT_TimeOfDay = 10001,

        // date data type
        DPT_Date = 11001,

        // 4 byte unsigned data type
        DPT_Value_4_Ucount = 12001,

        // 4 byte signed data type
        DPT_Value_4_Count = 13001,

        // 4 byte float data points
        DPT_Value_AngleDeg = 14007,
        DPT_Value_Electric_Current = 14019,
        DPT_Value_Electric_Potential = 14027,
        DPT_Value_Electric_PotentialDifference = 14028,
        DPT_Value_Energy = 14031,
        DPT_Value_Force = 14032,
        DPT_Value_Frequency = 14033,
        DPT_Value_Heat_FlowRate = 14036,
        DPT_Value_Heat_Quantity = 14037,
        DPT_Value_Impedance = 14038,
        DPT_Value_Length = 14039,
        DPT_Value_Mass = 14051,
        DPT_Value_Power = 14056,
        DPT_Value_Speed = 14065,
        DPT_Value_Stress = 14066,
        DPT_Value_Surface_Tension = 14067,
        DPT_Value_Common_Temperature = 14068,
        DPT_Value_Absolute_Temperature = 14069,
        DPT_Value_TemperatureDifference = 14070,
        DPT_Value_Weight = 14078,
        DPT_Value_Work = 14079,

        // access control data point types
        DPT_Access_Data = 15000,

        // scene activation data types
        DPT_SceneNumber = 17001,

        // scene management data types
        DPT_SceneControl = 18001,

        // time and date data type
        DPT_DateTime = 19001,

        // HVAC data point types
        DPT_BuildingMode = 20002,
        DPT_OccMode = 20003,
        DPT_HVACMode = 20102,
        DPT_HVACContrMode = 20105
    };
    Q_ENUM(Name)

    enum Type {
        TypeNone = 0x00,
        TypeBool,
        TypeSteps,
        Type4BitUInt,
        Type1ByteInt,
        Type1ByteUInt,
        Type1ByteChar,
        Type2ByteInt,
        Type2ByteUInt,
        Type2ByteFloat,
        Type3ByteTime,
        Type3ByteDate,
        Type4ByteInt,
        Type4ByteUInt,
        Type4ByteFloat,
        Type8ByteDateTime,
        Type14ByteText
    };
    Q_ENUM(Type)

    enum Flag {
        FlagsNone = 0,
        HasControl = 1 << 0,
        HasDirection = 1 << 1,
        HasScaling = 1 << 2
    };
    Q_DECLARE_FLAGS(Flags,Flag)
    Q_FLAG(Flags)

    explicit QKnxDpt();
    explicit QKnxDpt(const Name& dpt_name);
    ~QKnxDpt();

    struct Properties
    {
        struct Basic
        {
            Basic() : name(DPT_None), type(TypeNone), bitLength(0) {}
            Name name;
            Type type;
            Flags flags;
            int bitLength;
            int contentLength() const { return (bitLength <= 6) ? 1 : 1+bitLength/8; }
        } basic;
        struct Extended
        {
            Extended() : minScaled(0.0f), maxScaled(0.0f) {}
            QVariant minValue;
            QVariant maxValue;
            //QVariant minScaled;
            //QVariant maxScaled;
            float minScaled;
            float maxScaled;
            QString unit;
        } extended;
    };

    class ValueType
    {
    public:
        ValueType() {}
        virtual ~ValueType() {}

        virtual ValueType* clone() { return Q_NULLPTR; }
        virtual ValueType* create() { return Q_NULLPTR; }

        virtual void serialize(QKnxStream& fs) { Q_UNUSED(fs); }
        virtual void deserialize(QKnxStream& fs) { Q_UNUSED(fs); }
    };

    template <class T> class ValueTypeCRTP : public ValueType
    {
    public:
        ValueTypeCRTP() : ValueType() {}
        virtual ~ValueTypeCRTP() {}

        virtual ValueType* clone() { return new T(static_cast<T const&>(*this)); }
        virtual ValueType* create() { return new T(); }

        virtual void serialize(QKnxStream& fs)
        {
            T* t = static_cast<T*>(this);
            fs << *t;
        }

        virtual void deserialize(QKnxStream& fs)
        {
            T* t = static_cast<T*>(this);
            fs >> *t;
        }
    };

    class Value
    {
    public:
        Value() : m_Type(TypeNone), m_Value(Q_NULLPTR) { }
        Value(const Value& other) : m_Type(TypeNone), m_Value(Q_NULLPTR)
        {
            m_Type = other.m_Type;
            if (other.m_Value) {
                m_Value = other.m_Value->clone();
            }
        }
        ~Value() { unset(); }

        Value& operator =(const Value& other)
        {
            m_Type = other.m_Type;
            if (m_Value) {
                delete m_Value;
                m_Value = Q_NULLPTR;
            }
            if (other.m_Value) {
                m_Value = other.m_Value->clone();
            }
            return *this;
        }

        ValueType* data() { return m_Value; }

        template <typename T> T get()
        {
            Type tx = QKnxDpt::type<T>();
            if (!m_Value || tx == TypeNone || tx != m_Type) {
                return T();
            }
            T* ret = dynamic_cast<T*>(m_Value);
            if (!ret) {
                return T();
            }
            return T(*ret);
        }

        template <typename T> T* getPtr()
        {
            Type tx = QKnxDpt::type<T>();
            if (!m_Value || tx == TypeNone || tx != m_Type) {
                return Q_NULLPTR;
            }
            T* ret = dynamic_cast<T*>(m_Value);
            return ret;
        }

        void set(Type t, ValueType* vt)
        {
            m_Type = t;
            if (m_Value) {
                delete m_Value;
            }
            m_Value = vt;
        }

        template <typename T> bool set(const T& t)
        {
            Type tx = QKnxDpt::type<T>();
            if (tx == TypeNone) {
                return false;
            }
            if (m_Value) {
                delete m_Value;
            }
            m_Type = tx;
            m_Value = new T(t);
            return true;
        }

        template <typename T> bool set(T* t)
        {
            Type tx = QKnxDpt::type<T>();
            if (!t || tx == TypeNone) {
                return false;
            }
            if (m_Value) {
                delete m_Value;
            }
            m_Type = tx;
            m_Value = t;
            return true;
        }

        void unset()
        {
            m_Type = TypeNone;
            delete m_Value;
            m_Value = Q_NULLPTR;
        }

        bool isSet() { return (m_Value); }

    private:
        Type m_Type;
        ValueType* m_Value;
    };

    static bool registerDpts();

    static QString nameString(Name dpt_name) { return QMetaEnum::fromType<Name>().valueToKey(dpt_name); }
    static QString dataString(Type dpt_type) { return QMetaEnum::fromType<Type>().valueToKey(dpt_type); }
    static QString flagString(Flag dpt_flag) { return QMetaEnum::fromType<Flags>().valueToKey(dpt_flag); }
    static QString flagsString(Flags dpt_flags) { return QMetaEnum::fromType<Flags>().valueToKey(dpt_flags); }

    // basic properties
    static Properties::Basic basicProperties(const Name& dpt_name);
    Properties::Basic basicProperties() const { return m_BasicProperties; }

    // extended properties
    static Properties::Extended extendedProperties(const Name& dpt_name);
    Properties::Extended extendedProperties() { return (readExtendedProperties()) ? Properties::Extended(*readExtendedProperties()) : Properties::Extended(); }

    // property adding
    static void addProperties(const Properties &properties);

    Name name() const { return m_BasicProperties.name; }
    QString nameString() { return nameString(m_BasicProperties.name); }
    void setName(const Name& dpt_name)  { m_BasicProperties = basicProperties(dpt_name); }

    Type data() const { return m_BasicProperties.type; }
    QString dataString() { return dataString(m_BasicProperties.type); }

    Flags flags() const { return m_BasicProperties.flags; }
    QString flagString(Flags dpt_flags) { return flagsString(dpt_flags); }

    int bitLength() const { return m_BasicProperties.bitLength; }
    int contentLength() const { return m_BasicProperties.contentLength(); }

    /*
    QVariant minValue() { return m_Properties.minValue; }
    QVariant maxValue() { return m_Properties.maxValue; }
    QVariant minScaled() { return m_Properties.minScaled; }
    QVariant maxScaled() { return m_Properties.maxScaled; }
    QVariant unit() { return m_Properties.unit; }
    */

    template <typename T> static Type type() { return TypeNone; }
    template <typename T> static bool isType(Type is_type) { return (is_type != TypeNone && type<T>() == is_type); }
    template <typename T> bool isType() const { return (QKnxDpt::isType<T>(m_BasicProperties.type)); }
    template <typename T> static bool registerType(bool* register_var = Q_NULLPTR)
    {
        Q_UNUSED(register_var);
        if (type<T>() == TypeNone) {
            return false;
        }
        qRegisterMetaType<T>();
        if (!QMetaType::hasRegisteredDebugStreamOperator<T>()) {
            QMetaType::registerDebugStreamOperator<T>();
        }
        T* t = new T;
        dptTypeObjectsMutex().lock();
        if (dptTypeObjectsCache().maxCost() == dptTypeObjectsCache().count()) {
            dptTypeObjectsCache().setMaxCost(dptTypeObjectsCache().maxCost()+64);
        }
        dptTypeObjectsCache().insert(type<T>(),t);
        dptTypeObjectsMutex().unlock();
        return true;
    }

    template <typename T> T value()
    {
        if (!isType<T>()) {
            return T();
        }
        return m_Value.get<T>();
    }

    template <typename T> T* valuePtr()
    {
        if (!isType<T>()) {
            return Q_NULLPTR;
        }
        return m_Value.getPtr<T>();
    }

    template <typename T> bool setValue(const T& t)
    {
        if (!isType<T>()) {
            return false;
        }
        return m_Value.set<T>(t);
    }

    template <typename T> bool setValue(T* t)
    {
        if (!isType<T>()) {
            return false;
        }
        return m_Value.set<T>(t);
    }

private:
    static QCache<QKnxDpt::Name, QKnxDpt::Properties::Basic>& dptBasicPropertyCache();
    static QMutex& dptBasicPropertyMutex();
    static QCache<QKnxDpt::Name, QKnxDpt::Properties::Extended>& dptExtendedPropertyCache();
    static QMutex& dptExtendedPropertyMutex();

    static QCache<QKnxDpt::Type, QKnxDpt::ValueType>& dptTypeObjectsCache();
    static QMutex& dptTypeObjectsMutex();

    Properties::Basic m_BasicProperties;
    Properties::Extended* m_ExtendedProperties;
    Value m_Value;

    Properties::Extended* readExtendedProperties();

    friend QKnxStream& operator>>(QKnxStream& fs, QKnxDpt& data);
    friend QKnxStream& operator<<(QKnxStream& fs, QKnxDpt& data);

};
Q_DECLARE_OPERATORS_FOR_FLAGS(QKnxDpt::Flags)

QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QKnxDpt& data);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, QKnxDpt& data);

//
// min() / max() template specializations
//

/*
// specialization for QKnxFloat16
template <> QKNX_SHARED_EXPORT QVariant QKnxDpt::min<QKnxFloat16>();
template <> QKNX_SHARED_EXPORT QVariant QKnxDpt::max<QKnxFloat16>();

// specialization for float
template <> QKNX_SHARED_EXPORT QVariant QKnxDpt::min<float>();
template <> QKNX_SHARED_EXPORT QVariant QKnxDpt::max<float>();

// specialization for QKnxTimeOfDay
template <> QKNX_SHARED_EXPORT QVariant QKnxDpt::min<QKnxTimeOfDay>();
template <> QKNX_SHARED_EXPORT QVariant QKnxDpt::max<QKnxTimeOfDay>();

// specialization for QKnxDate
template <> QKNX_SHARED_EXPORT QVariant QKnxDpt::min<QKnxDate>();
template <> QKNX_SHARED_EXPORT QVariant QKnxDpt::max<QKnxDate>();

// specialization for QKnxDateTime
template <> QKNX_SHARED_EXPORT QVariant QKnxDpt::min<QKnxDateTime>();
template <> QKNX_SHARED_EXPORT QVariant QKnxDpt::max<QKnxDateTime>();
*/

//
// serialize() / deserialize() template specializations
//

//template <> QKNX_SHARED_EXPORT void QKnxDpt::serialize<QKnxBool>(QKnxStream& fs) const;
//template <> QKNX_SHARED_EXPORT void QKnxDpt::deserialize<QKnxBool>(QKnxStream& fs);

/*
class QKNX_SHARED_EXPORT QKnxDpt : public QKnxObject
{
    Q_OBJECT
public:
    enum DptName {
        DPT_Unknown = 0,
        DPT_Switch = 1001,
        DPT_Bool = 1002,
        DPT_Enable = 1003,
        DPT_Ramp = 1004,
        DPT_Alarm = 1005,
        DPT_BinaryValue = 1006,
        DPT_Step = 1007,
        DPT_UpDown = 1008,
        DPT_OpenClose = 1009,
        DPT_Start = 1010,
        DPT_State = 1011,
        DPT_Invert = 1012,
        DPT_DimSendStyle = 1013,
        DPT_InputSource = 1014,
        DPT_Reset = 1015,
        DPT_Ack = 1016,
        DPT_Trigger = 1017,
        DPT_Occupancy = 1018,
        DPT_Window_Door = 1019,
        DPT_LogicalFunction = 1021,
        DPT_Scene_AB = 1022,

        DPT_Switch_Control = 2001,
        DPT_Bool_Control = 2002,
        DPT_Enable_Control = 2003,
        DPT_Ramp_Control = 2004,
        DPT_Alarm_Control = 2005,
        DPT_BinaryValue_Control = 2006,
        DPT_Step_Control = 2007,
        DPT_Direction1_Control = 2008,
        DPT_Direction2_Control = 2009,
        DPT_Start_Control = 2010,
        DPT_State_Control = 2011,
        DPT_Invert_Control = 2012
    };
    Q_ENUMS(DptName)

    Q_INVOKABLE QKnxDpt();
    virtual ~QKnxDpt();

    // properties
    QString dptClassName() { return metaObject()->className(); }
    int dptMetaId();
    int dptPrimaryId() { return dptPrimaryIdById(dptId()); }
    int dptSecondaryId() { return dptSecondaryIdById(dptId()); }
    QString dptDpst() { return dptDpstById(dptId()); }
    QString dptIdString() { return dptIdStringById(dptId()); }

    // dpt individual properties
    virtual int dptId() { return DPT_Unknown; }
    virtual QString dptIdName() { return dptNameById(dptId()); }
    virtual bool isCreateable() { return false; }

    // dpt individual limits & unit
    virtual int bitLength() { return 0; }
    virtual QVariant min() { return QVariant(); }
    virtual QVariant max() { return QVariant(); }
    virtual QVariant scaledMin() { return QVariant(); }
    virtual QVariant scaledMax() { return QVariant(); }
    virtual QString unit() { return ""; }

    // value functions - override in childs
    virtual QVariant value(bool* ok = Q_NULLPTR) { if (ok) *ok = false; return QVariant(); }
    virtual bool setValue(QVariant value) { Q_UNUSED(value); return false; }

    virtual bool control(bool* ok = Q_NULLPTR) { if (ok) *ok = false; return false; }
    virtual bool setControl(bool control) { Q_UNUSED(control); return false; }
    virtual bool hasControl() { return false; }

    virtual QVariant scaled(bool* ok = Q_NULLPTR) { if (ok) *ok = false; return QVariant(); }
    virtual bool setScaled(QVariant value) { Q_UNUSED(value); return false; }
    virtual bool hasScaled() { return false; }

    // date/time functions - override in childs
    virtual int dayOfWeek(bool* ok = Q_NULLPTR) { if(ok) *ok = false; return -1; }
    virtual bool setDayOfWeek(int day_of_week) { Q_UNUSED(day_of_week); return false; }
    virtual bool hasDayOfWeek() { return false; }

    virtual bool dateTimeFault(bool* ok = Q_NULLPTR) { if(ok) *ok = false; return false; }
    virtual bool setDateTimeFault(bool fault) { Q_UNUSED(fault); return false; }
    virtual bool hasDateTimeFault() { return false; }

    virtual bool workingDay(bool* ok = Q_NULLPTR) { if(ok) *ok = false; return false; }
    virtual bool setWorkingDay(bool working_day) { Q_UNUSED(working_day); return false; }
    virtual bool hasWorkingDay() { return false; }

    virtual bool noWorkingDay(bool* ok = Q_NULLPTR) { if(ok) *ok = false; return false; }
    virtual bool setNoWorkingDay(bool no_working_day) { Q_UNUSED(no_working_day); return false; }
    virtual bool hasNoWorkingDay() { return false; }

    virtual bool noYear(bool* ok = Q_NULLPTR) { if(ok) *ok = false; return false; }
    virtual bool setNoYear(bool no_year) { Q_UNUSED(no_year); return false; }
    virtual bool hasNoYear() { return false; }

    virtual bool noDate(bool* ok = Q_NULLPTR) { if(ok) *ok = false; return false; }
    virtual bool setNoDate(bool no_date) { Q_UNUSED(no_date); return false; }
    virtual bool hasNoDate() { return false; }

    virtual bool noDayOfWeek(bool* ok = Q_NULLPTR) { if(ok) *ok = false; return false; }
    virtual bool setNoDayOfWeek(bool no_day_of_week) { Q_UNUSED(no_day_of_week); return false; }
    virtual bool hasNoDayOfWeek() { return false; }

    virtual bool noTime(bool* ok = Q_NULLPTR) { if(ok) *ok = false; return false; }
    virtual bool setNoTime(bool no_time) { Q_UNUSED(no_time); return false; }
    virtual bool hasNoTime() { return false; }

    virtual bool standardSummerTime(bool* ok = Q_NULLPTR) { if(ok) *ok = false; return false; }
    virtual bool setStandardSummerTime(bool standard_summertime) { Q_UNUSED(standard_summertime); return false; }
    virtual bool hasStandardSummerTime() { return false; }

    virtual bool qualityOfClock(bool* ok = Q_NULLPTR) { if(ok) *ok = false; return false; }
    virtual bool setQualityOfClock(bool quality_of_clock) { Q_UNUSED(quality_of_clock); return false; }
    virtual bool hasQualityOfClock() { return false; }

    // content functions
    QByteArray content() { return m_Content; }
    bool setContent(QByteArray content) { m_Content = content; return isValid(); }
    int contentLengthReal() { return m_Content.length(); }
    virtual int contentLengthTelegram() { return (bitLength() <= 6) ? contentLengthReal() : contentLengthReal() + 1; }
    //virtual bool setContentLength(int len) { Q_UNUSED(len); return false; }
    virtual bool isValid();

    // static meta id functions
    static int metaIdbyId(int dpt_id);
    static int metaIdByClassName(QString dpt_class_name);
    static int metaIdByIdName(QString dpt_id_name);
    static int metaIdByDpst(QString dpt_dpst);
    static int metaIdByIdString(QString dpt_id_str);
    static int metaIdByString(QString dpt_str);

    static int idByMetaId(int meta_id);
    static QString classNameByMetaId(int meta_id);
    static QString idNameByMetaId(int meta_id);
    static QString dpstByMetaId(int meta_id) { return dptDpstById(idByMetaId(meta_id)); }
    static QString idStringByMetaId(int meta_id) { return dptIdStringById(idByMetaId(meta_id)); }
    static int primaryIdByMetaId(int meta_id) { return dptPrimaryIdById(idByMetaId(meta_id)); }
    static int secondaryIdByMetaId(int meta_id) { return dptSecondaryIdById(idByMetaId(meta_id)); }

    // dpt registration
    static QString dptBaseClass() { return QKnxDpt::staticMetaObject.className(); }
    static void registerDpts();
    template <class T>
    static bool registerDpt() { return registerDptMetaType(qRegisterMetaType<T>()); }

    // dpt creation
    static QKnxDpt* create(int dpt_id) { return createById(dpt_id); }
    static QKnxDpt* create(QString str) { return createByString(str); }

    static QKnxDpt* createByMetaId(int type_id);
    static QKnxDpt* createById(int dpt_id) { return createByMetaId(metaIdbyId(dpt_id)); }
    static QKnxDpt* createByClassName(QString class_name) { return createByMetaId(metaIdByClassName(class_name)); }
    static QKnxDpt* createByIdName(QString dpt_name) { return createByMetaId(metaIdByIdName(dpt_name)); }
    static QKnxDpt* createByDpst(QString dpst) { return createByMetaId(metaIdByDpst(dpst)); }
    static QKnxDpt* createByIdString(QString id_str) { return createByMetaId(metaIdByIdString(id_str)); }
    static QKnxDpt* createByString(QString str) { return createByMetaId(metaIdByString(str)); }

    struct QKNX_SHARED_EXPORT Properties
    {

    };

protected:
    // internal data storage
    QByteArray m_Content;

    // enum values to string
    static QString staticEnumValueString(QString enumName, int enumValue);
    static QString dptNameById(const int& dpt_id) { return staticEnumValueString(QKNX_TYPE_TO_STRING(DptName),dpt_id); }
    static QString dptDpstById(const int& dpt_id);
    static QString dptIdStringById(const int& dpt_id);
    static int dptPrimaryIdById(const int& dpt_id);
    static int dptSecondaryIdById(const int& dpt_id);

    // content functions
    bool contentLengthCheck(bool adjust_size = false);
    bool rangeTest(QVariant value);

private:
    static QMutex& idMutex();
    static QHash<int,int>& metaIdByIdHash();
    static QHash<QString, int>& metaIdByClassNameHash();
    static QHash<QString, int>& metaIdByIdNameHash();
    static QHash<int, bool>& metaIdCreateableHash();

    static bool registerDptMetaType(int type_id);
};

*/

#endif // QKNXDPT_H
