#ifndef QKNXDPT14BYTE_H
#define QKNXDPT14BYTE_H

#include "qknxdpt.h"

class QKNX_SHARED_EXPORT QKnxDpt14Byte
{
public:
    static void registerDpts()
    {
        /*
        QKnxDpt::Properties p_3byte;

        // DPT_TimeOfDay
        p_3byte.dptName = QKnxDpt::DPT_TimeOfDay;
        p_3byte.dptData = QKnxDpt::Type3ByteTime;
        p_3byte.dptFlags = QKnxDpt::FlagsNone;
        p_3byte.bitLength = 24;

        p_3byte.minValue = QKnxDpt::min<QKnxTimeOfDay>();
        p_3byte.maxValue = QKnxDpt::max<QKnxTimeOfDay>();
        p_3byte.minScaled = QVariant();
        p_3byte.maxScaled = QVariant();
        p_3byte.unit = "";

        QKnxDpt::addProperties(p_3byte);

        // DPT_Date
        p_3byte.dptName = QKnxDpt::DPT_Date;
        p_3byte.dptData = QKnxDpt::Type3ByteDate;
        p_3byte.dptFlags = QKnxDpt::FlagsNone;
        p_3byte.bitLength = 24;

        p_3byte.minValue = QKnxDpt::min<QKnxDate>();
        p_3byte.maxValue = QKnxDpt::max<QKnxDate>();
        p_3byte.minScaled = QVariant();
        p_3byte.maxScaled = QVariant();
        p_3byte.unit = "";

        QKnxDpt::addProperties(p_3byte);
        */
    }
};

#endif // QKNXDPT14BYTE_H
