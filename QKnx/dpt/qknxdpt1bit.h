#ifndef QKNXDPT1BIT_H
#define QKNXDPT1BIT_H

#include "qknxdpt.h"
#include "types/qknxbool.h"

class QKNX_SHARED_EXPORT QKnxDpt1Bit
{
public:
    static void registerDpts()
    {
        // define basic properties
        QKnxDpt::Properties p_1bit;
        p_1bit.basic.name = QKnxDpt::DPT_None; // replace with real dpt id
        p_1bit.basic.type = QKnxDpt::TypeBool;
        p_1bit.basic.flags = QKnxDpt::FlagsNone;
        p_1bit.basic.bitLength = 1;

        p_1bit.extended.minValue = QKnxBool::min();
        p_1bit.extended.maxValue = QKnxBool::max();
        p_1bit.extended.minScaled = 0.0f;
        p_1bit.extended.maxScaled = 0.0f;
        p_1bit.extended.unit = "";

        // register 1 bit dpts
        registerDpt(p_1bit,QKnxDpt::DPT_Switch);
        registerDpt(p_1bit,QKnxDpt::DPT_Bool);
        registerDpt(p_1bit,QKnxDpt::DPT_Enable);
        registerDpt(p_1bit,QKnxDpt::DPT_Ramp);
        registerDpt(p_1bit,QKnxDpt::DPT_Alarm);
        registerDpt(p_1bit,QKnxDpt::DPT_BinaryValue);
        registerDpt(p_1bit,QKnxDpt::DPT_Step);
        registerDpt(p_1bit,QKnxDpt::DPT_UpDown);
        registerDpt(p_1bit,QKnxDpt::DPT_OpenClose);
        registerDpt(p_1bit,QKnxDpt::DPT_Start);
        registerDpt(p_1bit,QKnxDpt::DPT_State);
        registerDpt(p_1bit,QKnxDpt::DPT_Invert);
        registerDpt(p_1bit,QKnxDpt::DPT_DimSendStyle);
        registerDpt(p_1bit,QKnxDpt::DPT_InputSource);
        registerDpt(p_1bit,QKnxDpt::DPT_Reset);
        registerDpt(p_1bit,QKnxDpt::DPT_Ack);
        registerDpt(p_1bit,QKnxDpt::DPT_Trigger);
        registerDpt(p_1bit,QKnxDpt::DPT_Occupancy);
        registerDpt(p_1bit,QKnxDpt::DPT_Window_Door);
        registerDpt(p_1bit,QKnxDpt::DPT_LogicalFunction);
        registerDpt(p_1bit,QKnxDpt::DPT_Scene_AB);
    }

private:
    static void registerDpt(const QKnxDpt::Properties &prop, const QKnxDpt::Name &dpt_name)
    {
        QKnxDpt::Properties px(prop);
        px.basic.name = dpt_name;
        QKnxDpt::addProperties(px);
    }
};

#endif // QKNXDPT1BIT_H
