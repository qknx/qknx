#ifndef QKNXDPT1BYTE_H
#define QKNXDPT1BYTE_H

#include "qknxdpt.h"
#include "types/qknxtypes.h"

class QKNX_SHARED_EXPORT QKnxDpt1Byte
{
public:
    static void registerDpts()
    {
        QKnxDpt::Properties p_1byte;

        // 8-bit Char types (4.xxx)

        // DPT_Char_ASCII
        p_1byte.basic.name = QKnxDpt::DPT_Char_ASCII;
        p_1byte.basic.type = QKnxDpt::Type1ByteChar;
        p_1byte.basic.flags = QKnxDpt::FlagsNone;
        p_1byte.basic.bitLength = 8;

        p_1byte.extended.minValue = QKnxChar::min();
        p_1byte.extended.maxValue = QKnxChar::max();
        p_1byte.extended.minScaled = 0.0f;
        p_1byte.extended.maxScaled = 0.0f;
        p_1byte.extended.unit = "";

        QKnxDpt::addProperties(p_1byte);

        // DPT_Char_8859_1
        p_1byte.basic.name = QKnxDpt::DPT_Char_8859_1;
        p_1byte.basic.type = QKnxDpt::Type1ByteChar;
        p_1byte.basic.flags = QKnxDpt::FlagsNone;
        p_1byte.basic.bitLength = 8;

        p_1byte.extended.minValue = QKnxChar::min();
        p_1byte.extended.maxValue = QKnxChar::max();
        p_1byte.extended.minScaled = 0.0f;
        p_1byte.extended.maxScaled = 0.0f;
        p_1byte.extended.unit = "";

        QKnxDpt::addProperties(p_1byte);

        // 8-bit unsigned data point types (5.xxx)

        // DPT_Scaling
        p_1byte.basic.name = QKnxDpt::DPT_Scaling;
        p_1byte.basic.type = QKnxDpt::Type1ByteUInt;
        p_1byte.basic.flags = QKnxDpt::HasScaling;
        p_1byte.basic.bitLength = 8;

        p_1byte.extended.minValue = QKnxUInt8::min(); // 0
        p_1byte.extended.maxValue = QKnxUInt8::max(); // 255
        p_1byte.extended.minScaled = 0.0f;
        p_1byte.extended.maxScaled = 100.0f;
        p_1byte.extended.unit = "%";

        QKnxDpt::addProperties(p_1byte);

        // DPT_Angle
        p_1byte.basic.name = QKnxDpt::DPT_Angle;
        p_1byte.basic.type = QKnxDpt::Type1ByteUInt;
        p_1byte.basic.flags = QKnxDpt::HasScaling;
        p_1byte.basic.bitLength = 8;

        p_1byte.extended.minValue = QKnxUInt8::min(); // 0
        p_1byte.extended.maxValue = QKnxUInt8::max(); // 255
        p_1byte.extended.minScaled = 0.0f;
        p_1byte.extended.maxScaled = 360.0f;
        p_1byte.extended.unit = "°";

        QKnxDpt::addProperties(p_1byte);

        // DPT_Percent_U8
        p_1byte.basic.name = QKnxDpt::DPT_Percent_U8;
        p_1byte.basic.type = QKnxDpt::Type1ByteUInt;
        p_1byte.basic.flags = QKnxDpt::FlagsNone;
        p_1byte.basic.bitLength = 8;

        p_1byte.extended.minValue = QKnxUInt8::min(); // 0
        p_1byte.extended.maxValue = QKnxUInt8::max(); // 255
        p_1byte.extended.minScaled = 0.0f;
        p_1byte.extended.maxScaled = 0.0f;
        p_1byte.extended.unit = "%";

        QKnxDpt::addProperties(p_1byte);

        // DPT_Value_1_Ucount
        p_1byte.basic.name = QKnxDpt::DPT_Value_1_Ucount;
        p_1byte.basic.type = QKnxDpt::Type1ByteUInt;
        p_1byte.basic.flags = QKnxDpt::FlagsNone;
        p_1byte.basic.bitLength = 8;

        p_1byte.extended.minValue = QKnxUInt8::min(); // 0
        p_1byte.extended.maxValue = QKnxUInt8::max(); // 255
        p_1byte.extended.minScaled = 0.0f;
        p_1byte.extended.maxScaled = 0.0f;
        p_1byte.extended.unit = "counter pulses";

        QKnxDpt::addProperties(p_1byte);

        // 8-bit signed data point types (6.xxx)

        // DPT_Percent_V8
        p_1byte.basic.name = QKnxDpt::DPT_Percent_V8;
        p_1byte.basic.type = QKnxDpt::Type1ByteInt;
        p_1byte.basic.flags = QKnxDpt::FlagsNone;
        p_1byte.basic.bitLength = 8;

        p_1byte.extended.minValue = QKnxInt8::min(); // -128
        p_1byte.extended.maxValue = QKnxInt8::max(); // 127
        p_1byte.extended.minScaled = 0.0f;
        p_1byte.extended.maxScaled = 0.0f;
        p_1byte.extended.unit = "%";

        QKnxDpt::addProperties(p_1byte);

        // DPT_Value_1_Count
        p_1byte.basic.name = QKnxDpt::DPT_Value_1_Count;
        p_1byte.basic.type = QKnxDpt::Type1ByteInt;
        p_1byte.basic.flags = QKnxDpt::FlagsNone;
        p_1byte.basic.bitLength = 8;

        p_1byte.extended.minValue = QKnxInt8::min(); // -128
        p_1byte.extended.maxValue = QKnxInt8::max(); // 127
        p_1byte.extended.minScaled = 0.0f;
        p_1byte.extended.maxScaled = 0.0f;
        p_1byte.extended.unit = "counter pulses";

        QKnxDpt::addProperties(p_1byte);
    }
};

#endif // QKNXDPT1BYTE_H
