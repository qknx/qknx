#ifndef QKNXDPT2BIT_H
#define QKNXDPT2BIT_H

#include "qknxdpt.h"
#include "types/qknxbool.h"

class QKNX_SHARED_EXPORT QKnxDpt2Bit
{
public:
    static void registerDpts()
    {
        // define basic properties
        QKnxDpt::Properties p_2bit;
        p_2bit.basic.name = QKnxDpt::DPT_None; // replace with real dpt id
        p_2bit.basic.type = QKnxDpt::TypeBool;
        p_2bit.basic.flags = QKnxDpt::HasControl;
        p_2bit.basic.bitLength = 2;

        p_2bit.extended.minValue = QKnxBool::min();
        p_2bit.extended.maxValue = QKnxBool::max();
        p_2bit.extended.minScaled = 0.0f;
        p_2bit.extended.maxScaled = 0.0f;
        p_2bit.extended.unit = "";

        // register 2 bit dpts
        registerDpt(p_2bit,QKnxDpt::DPT_Switch_Control);
        registerDpt(p_2bit,QKnxDpt::DPT_Bool_Control);
        registerDpt(p_2bit,QKnxDpt::DPT_Enable_Control);
        registerDpt(p_2bit,QKnxDpt::DPT_Ramp_Control);
        registerDpt(p_2bit,QKnxDpt::DPT_Alarm_Control);
        registerDpt(p_2bit,QKnxDpt::DPT_BinaryValue_Control);
        registerDpt(p_2bit,QKnxDpt::DPT_Step_Control);
        registerDpt(p_2bit,QKnxDpt::DPT_Direction1_Control);
        registerDpt(p_2bit,QKnxDpt::DPT_Direction2_Control);
        registerDpt(p_2bit,QKnxDpt::DPT_Start_Control);
        registerDpt(p_2bit,QKnxDpt::DPT_State_Control);
        registerDpt(p_2bit,QKnxDpt::DPT_Invert_Control);
    }

private:
    static void registerDpt(const QKnxDpt::Properties &prop, const QKnxDpt::Name &dpt_name)
    {
        QKnxDpt::Properties px(prop);
        px.basic.name = dpt_name;
        QKnxDpt::addProperties(px);
    }
};

#endif // QKNXDPT2BIT_H
