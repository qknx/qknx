#ifndef QKNXDPT2BYTE_H
#define QKNXDPT2BYTE_H

#include "qknxdpt.h"
#include "types/qknxfloat16.h"
#include "types/qknxtypes.h"

class QKNX_SHARED_EXPORT QKnxDpt2Byte
{
public:
    static void registerDpts()
    {
        QKnxDpt::Properties p_2byte;

        // 16-bit unsigned integer (7.xxx)

        // DPT_Value_2_Ucount
        p_2byte.basic.name = QKnxDpt::DPT_Value_2_Ucount;
        p_2byte.basic.type = QKnxDpt::Type2ByteUInt;
        p_2byte.basic.flags = QKnxDpt::FlagsNone;
        p_2byte.basic.bitLength = 16;

        p_2byte.extended.minValue = QKnxUInt16::min(); // 0
        p_2byte.extended.maxValue = QKnxUInt16::max(); // 65535
        p_2byte.extended.minScaled = 0.0f;
        p_2byte.extended.maxScaled = 0.0f;
        p_2byte.extended.unit = "counter pulses";

        QKnxDpt::addProperties(p_2byte);

        // 16-bit signed integer (8.xxx)

        // DPT_Value_2_Count
        p_2byte.basic.name = QKnxDpt::DPT_Value_2_Count;
        p_2byte.basic.type = QKnxDpt::Type2ByteInt;
        p_2byte.basic.flags = QKnxDpt::FlagsNone;
        p_2byte.basic.bitLength = 16;

        p_2byte.extended.minValue = QKnxInt16::min(); // -32768
        p_2byte.extended.maxValue = QKnxInt16::max(); // 32767
        p_2byte.extended.minScaled = 0.0f;
        p_2byte.extended.maxScaled = 0.0f;
        p_2byte.extended.unit = "counter pulses";

        QKnxDpt::addProperties(p_2byte);

        // DPT_Percent_V16
        p_2byte.basic.name = QKnxDpt::DPT_Percent_V16;
        p_2byte.basic.type = QKnxDpt::Type2ByteInt;
        p_2byte.basic.flags = QKnxDpt::HasScaling;
        p_2byte.basic.bitLength = 16;

        p_2byte.extended.minValue = QKnxInt16::min(); // -32768
        p_2byte.extended.maxValue = QKnxInt16::max(); // 32767
        p_2byte.extended.minScaled = (float)p_2byte.extended.minValue.toInt() / 100;
        p_2byte.extended.maxScaled = (float)p_2byte.extended.maxValue.toInt() / 100;
        p_2byte.extended.unit = "%";

        QKnxDpt::addProperties(p_2byte);

        // 16-bit float (9.xxx)
        qreal common_min = -670760.0f;
        qreal common_max = 670760.0f;

        p_2byte.basic.name = QKnxDpt::DPT_None; // replace
        p_2byte.basic.type = QKnxDpt::Type2ByteFloat;
        p_2byte.basic.flags = QKnxDpt::FlagsNone;
        p_2byte.basic.bitLength = 16;

        p_2byte.extended.minValue = QKnxFloat16::min(); // replace
        p_2byte.extended.maxValue = QKnxFloat16::max(); // replace
        p_2byte.extended.minScaled = 0.0f;
        p_2byte.extended.maxScaled = 0.0f;
        p_2byte.extended.unit = ""; // replace

        registerDpt(p_2byte,QKnxDpt::DPT_Value_Temp,-273.0f,common_max,"°C");
        registerDpt(p_2byte,QKnxDpt::DPT_Value_Tempd,common_min,common_max,"K");
        registerDpt(p_2byte,QKnxDpt::DPT_Value_Tempa,common_min,common_max,"K/h");
        registerDpt(p_2byte,QKnxDpt::DPT_Value_Lux,0.0f,common_max,"Lux");
        registerDpt(p_2byte,QKnxDpt::DPT_Value_Wsp,0.0f,common_max,"m/s");
        registerDpt(p_2byte,QKnxDpt::DPT_Value_Pres,0.0f,common_max,"Pa");
        registerDpt(p_2byte,QKnxDpt::DPT_Value_Humidity,0.0f,common_max,"%");
        registerDpt(p_2byte,QKnxDpt::DPT_Value_AirQuality,0.0f,common_max,"ppm");
        registerDpt(p_2byte,QKnxDpt::DPT_Value_Time1,common_min,common_max,"s");
        registerDpt(p_2byte,QKnxDpt::DPT_Value_Time2,common_min,common_max,"ms");
        registerDpt(p_2byte,QKnxDpt::DPT_Value_Volt,common_min,common_max,"mV");
        registerDpt(p_2byte,QKnxDpt::DPT_Value_Curr,common_min,common_max,"mA");
        registerDpt(p_2byte,QKnxDpt::DPT_PowerDensity,common_min,common_max,"W/m²");
        registerDpt(p_2byte,QKnxDpt::DPT_KelvinPerPercent,common_min,common_max,"K/%");
        registerDpt(p_2byte,QKnxDpt::DPT_Power,common_min,common_max,"kW");
        registerDpt(p_2byte,QKnxDpt::DPT_Value_Volume_Flow,common_min,common_max,"l/h");
    }

private:
    static void registerDpt(const QKnxDpt::Properties& prop, QKnxDpt::Name dpt_name, qreal min, qreal max, QString unit)
    {
        QKnxDpt::Properties px(prop);
        px.basic.name = dpt_name;
        px.extended.minValue = QKnxFloat16(min);
        px.extended.maxValue = QKnxFloat16(max);
        px.extended.unit = unit;
        QKnxDpt::addProperties(px);
    }
};

#endif // QKNXDPT2BYTE_H
