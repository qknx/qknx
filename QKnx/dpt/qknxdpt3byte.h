#ifndef QKNXDPT3BYTE_H
#define QKNXDPT3BYTE_H

#include "qknxdpt.h"

#include "types/qknxdate.h"
#include "types/qknxtimeofday.h"

class QKNX_SHARED_EXPORT QKnxDpt3Byte
{
public:
    static void registerDpts()
    {
        QKnxDpt::Properties p_3byte;

        // DPT_TimeOfDay
        p_3byte.basic.name = QKnxDpt::DPT_TimeOfDay;
        p_3byte.basic.type = QKnxDpt::Type3ByteTime;
        p_3byte.basic.flags = QKnxDpt::FlagsNone;
        p_3byte.basic.bitLength = 24;

        p_3byte.extended.minValue = QKnxTimeOfDay::min();
        p_3byte.extended.maxValue = QKnxTimeOfDay::max();
        p_3byte.extended.minScaled = 0.0f;
        p_3byte.extended.maxScaled = 0.0f;
        p_3byte.extended.unit = "";

        QKnxDpt::addProperties(p_3byte);

        // DPT_Date
        p_3byte.basic.name = QKnxDpt::DPT_Date;
        p_3byte.basic.type = QKnxDpt::Type3ByteDate;
        p_3byte.basic.flags = QKnxDpt::FlagsNone;
        p_3byte.basic.bitLength = 24;

        p_3byte.extended.minValue = QKnxDate::min();
        p_3byte.extended.maxValue = QKnxDate::max();
        p_3byte.extended.minScaled = 0.0f;
        p_3byte.extended.maxScaled = 0.0f;
        p_3byte.extended.unit = "";

        QKnxDpt::addProperties(p_3byte);

    }
};

#endif // QKNXDPT3BYTE_H
