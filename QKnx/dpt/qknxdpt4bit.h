#ifndef QKNXDPT4BIT_H
#define QKNXDPT4BIT_H

#include "qknxdpt.h"

class QKNX_SHARED_EXPORT QKnxDpt4Bit
{
public:
    static void registerDpts()
    {
        // define basic properties
        QKnxDpt::Properties p_4bit;
        p_4bit.basic.name = QKnxDpt::DPT_None; // replace with real dpt id
        p_4bit.basic.type = QKnxDpt::TypeSteps;
        p_4bit.basic.flags = QKnxDpt::HasDirection;
        p_4bit.basic.bitLength = 4;

        p_4bit.extended.minValue = QVariant::fromValue<quint8>(0);
        p_4bit.extended.maxValue = QVariant::fromValue<quint8>(7);
        p_4bit.extended.minScaled = 0.0f;
        p_4bit.extended.maxScaled = 0.0f;
        p_4bit.extended.unit = "";

        // register 4 bit dpts
        registerDpt(p_4bit,QKnxDpt::DPT_Control_Dimming);
        registerDpt(p_4bit,QKnxDpt::DPT_Control_Blinds);
        registerDpt(p_4bit,QKnxDpt::DPT_Model_Boiler);
    }

private:
    static void registerDpt(const QKnxDpt::Properties &prop, const QKnxDpt::Name &dpt_name)
    {
        QKnxDpt::Properties px(prop);
        px.basic.name = dpt_name;
        QKnxDpt::addProperties(px);
    }
};

#endif // QKNXDPT4BIT_H
