#ifndef QKNXDPT4BYTE_H
#define QKNXDPT4BYTE_H

#include "qknxdpt.h"
#include "types/qknxtypes.h"

class QKNX_SHARED_EXPORT QKnxDpt4Byte
{
public:
    static void registerDpts()
    {
        QKnxDpt::Properties p_4byte;

        // 32-bit unsigned integer (12.xxx)

        // DPT_Value_4_Ucount
        p_4byte.basic.name = QKnxDpt::DPT_Value_4_Ucount;
        p_4byte.basic.type = QKnxDpt::Type4ByteUInt;
        p_4byte.basic.flags = QKnxDpt::FlagsNone;
        p_4byte.basic.bitLength = 32;

        p_4byte.extended.minValue = QKnxUInt32::min();
        p_4byte.extended.maxValue = QKnxUInt32::max();
        p_4byte.extended.minScaled = 0.0f;
        p_4byte.extended.maxScaled = 0.0f;
        p_4byte.extended.unit = "counter pulses";

        QKnxDpt::addProperties(p_4byte);

        // 32-bit signed integer (13.xxx)

        // DPT_Value_4_Count
        p_4byte.basic.name = QKnxDpt::DPT_Value_4_Count;
        p_4byte.basic.type = QKnxDpt::Type4ByteInt;
        p_4byte.basic.flags = QKnxDpt::FlagsNone;
        p_4byte.basic.bitLength = 32;

        p_4byte.extended.minValue = QKnxInt32::min();
        p_4byte.extended.maxValue = QKnxInt32::max();
        p_4byte.extended.minScaled = 0.0f;
        p_4byte.extended.maxScaled = 0.0f;
        p_4byte.extended.unit = "counter pulses";

        QKnxDpt::addProperties(p_4byte);

        // 32-bit floating point numbers

        p_4byte.basic.name = QKnxDpt::DPT_None; // replace
        p_4byte.basic.type = QKnxDpt::Type4ByteFloat;
        p_4byte.basic.flags = QKnxDpt::FlagsNone;
        p_4byte.basic.bitLength = 32;

        p_4byte.extended.minValue = QKnxFloat32::min();
        p_4byte.extended.maxValue = QKnxFloat32::max();
        p_4byte.extended.minScaled = 0.0f;
        p_4byte.extended.maxScaled = 0.0f;
        p_4byte.extended.unit = ""; // replace

        registerDpt(p_4byte,QKnxDpt::DPT_Value_AngleDeg,"°");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Electric_Current,"A");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Electric_Potential,"V");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Electric_PotentialDifference,"V");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Energy,"J");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Force,"N");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Frequency,"Hz");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Heat_FlowRate,"W");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Heat_Quantity,"J");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Impedance,"Ohm");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Length,"m");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Mass,"kg");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Power,"W");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Speed,"m/s²");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Stress,"Pa");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Surface_Tension,"N/m");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Common_Temperature,"°C");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Absolute_Temperature,"K");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_TemperatureDifference,"K");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Weight,"N");
        registerDpt(p_4byte,QKnxDpt::DPT_Value_Work,"J");

        // TODO: 15.xxx Access control
    }

private:
    static void registerDpt(const QKnxDpt::Properties& prop, const QKnxDpt::Name& dpt_name, const QString& unit)
    {
        QKnxDpt::Properties px(prop);
        px.basic.name = dpt_name;
        px.extended.unit = unit;
        QKnxDpt::addProperties(px);
    }
};

#endif // QKNXDPT4BYTE_H
