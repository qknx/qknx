#ifndef QKNXDPT8BYTE_H
#define QKNXDPT8BYTE_H

#include "qknxdpt.h"

#include "types/qknxdatetime.h"

class QKNX_SHARED_EXPORT QKnxDpt8Byte
{
public:
    static void registerDpts()
    {
        QKnxDpt::Properties p_3byte;

        // DPT_DateTime
        p_3byte.basic.name = QKnxDpt::DPT_DateTime;
        p_3byte.basic.type = QKnxDpt::Type8ByteDateTime;
        p_3byte.basic.flags = QKnxDpt::FlagsNone;
        p_3byte.basic.bitLength = 64;

        p_3byte.extended.minValue = QKnxDateTime::min();
        p_3byte.extended.maxValue = QKnxDateTime::max();
        p_3byte.extended.minScaled = 0.0f;
        p_3byte.extended.maxScaled = 0.0f;
        p_3byte.extended.unit = "";

        QKnxDpt::addProperties(p_3byte);
    }
};

#endif // QKNXDPT8BYTE_H
