#ifndef QKNXDPTINFO_H
#define QKNXDPTINFO_H

//#include "qknx_global.h"

#include "qknxdpt.h"

/*

#include <QMutex>
#include <QMutexLocker>

class QKNX_SHARED_EXPORT QKnxDptInfo
{
public:
    //explicit QKnxDptInfo(int dpt_id = QKnxDpt::DPT_Unknown, QString dpt_name = "", QString dpt_desc = "");
    explicit QKnxDptInfo(int dpt_id = QKnxDpt::DPT_Unknown);
    virtual ~QKnxDptInfo();

    // meta id used for internal storage
    int dptMetaId() const { return m_DptMetaId; }
    bool setDptMetaId(const int& dpt_meta_id);

    // ets name and description
    //QString dptName() const { return m_DptName; }
    //void setDptName(const QString& dpt_name) { m_DptName = dpt_name; }

    //QString dptDesc() const { return m_DptDesc; }
    //void setDptDesc(const QString& dpt_desc) { m_DptDesc = dpt_desc; }

    bool isValid() { return (m_DptMetaId > QMetaType::UnknownType); }

    // properties from dpt statics, set meta id by property
    int dptId() const { return QKnxDpt::idByMetaId(m_DptMetaId); }
    bool setDptId(const int& dpt_id) { return setDptMetaId(QKnxDpt::metaIdbyId(dpt_id)); }

    QString dptIdName() const { return QKnxDpt::idNameByMetaId(m_DptMetaId); }
    bool setDptIdName(const QString& dpt_id_name) { return setDptMetaId(QKnxDpt::metaIdByIdName(dpt_id_name)); }

    QString dptIdString() const { return QKnxDpt::idStringByMetaId(m_DptMetaId); }
    bool setDptIdString(const QString& dpt_id_string) { return setDptMetaId(QKnxDpt::metaIdByIdString(dpt_id_string)); }

    QString dptClassName() const { return QKnxDpt::classNameByMetaId(m_DptMetaId); }
    bool setDptClassName(const QString& dpt_class_name) { return setDptMetaId(QKnxDpt::metaIdByClassName(dpt_class_name)); }

    QString dptDpst() const { return QKnxDpt::dpstByMetaId(m_DptMetaId); }
    bool setDptDpst(const QString& dpt_dpst) { return setDptMetaId(QKnxDpt::metaIdByDpst(dpt_dpst)); }

    // creation
    QKnxDpt* createDpt() { return QKnxDpt::createByMetaId(m_DptMetaId); }

private:
    int m_DptMetaId;
    //QString m_DptName;
    //QString m_DptDesc;
};
Q_DECLARE_METATYPE(QKnxDptInfo)

*/

#endif // QKNXDPTINFO_H
