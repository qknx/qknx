#include "qknxio.h"

#include "qknxioworker.h"
#include "netip/qknxframe.h"

#include <QThread>
#include <QMutexLocker>

QKnxIO::QKnxIO(QObject *parent) : QObject(parent), m_Worker(Q_NULLPTR), m_Thread(Q_NULLPTR)
{

}

QKnxIO::~QKnxIO()
{

}

QKnxFrame *QKnxIO::readFrame()
{
    return m_Worker->readCacheTake();
}

bool QKnxIO::writeFrame(const QKnxFrame &frame)
{
    QKnxFrame* f = new QKnxFrame(frame);
    int ret = m_Worker->writeCacheAppend(f);
    if (!ret) {
        delete f;
    }
    return ret;
}

bool QKnxIO::writeFrame(QKnxFrame *frame)
{
    return m_Worker->writeCacheAppend(frame);
}

void QKnxIO::init()
{
    m_Thread = new QThread();
    if (!m_Worker) {
        qDebug() << "Warning: No QKnxIOWorker defined!";
        return;
    }

    m_Worker->moveToThread(m_Thread);
    connect(m_Worker, SIGNAL(framePending()), this, SIGNAL(framePending()));
    connect(m_Thread, SIGNAL(started()), m_Worker, SLOT(start()));
    connect(m_Worker, SIGNAL(finished()), m_Thread, SLOT(quit()));
    connect(m_Worker, SIGNAL(finished()), m_Worker, SLOT(deleteLater()));
    connect(m_Thread, SIGNAL(finished()), m_Thread, SLOT(deleteLater()));
    m_Thread->start();
}
