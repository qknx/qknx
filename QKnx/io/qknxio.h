#ifndef QKNXIO_H
#define QKNXIO_H

#include "qknxglobal.h"

#include <QCache>
#include <QMutex>

class QThread;
class QKnxIOWorker;
class QKnxFrame;

class QKNX_SHARED_EXPORT QKnxIO : public QObject
{
    Q_OBJECT
public:
    explicit QKnxIO(QObject *parent = Q_NULLPTR);
    virtual ~QKnxIO();

    QKnxFrame* readFrame();

    bool writeFrame(const QKnxFrame& frame);
    bool writeFrame(QKnxFrame* frame);

signals:
    void framePending();

public slots:

protected:
    QKnxIOWorker* m_Worker;
    QThread* m_Thread;

    void init();

private:

private slots:


};

#endif // QKNXIO_H

