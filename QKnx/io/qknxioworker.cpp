#include "qknxioworker.h"

#include <QThread>

QKnxIOWorker::QKnxIOWorker() : m_ReadQueue(10000), m_WriteQueue(10000)
{
    // connect polling timer
    m_CacheTimer = new QTimer(this);
    m_CacheTimer->setInterval(1);
    m_CacheTimer->setSingleShot(false);
    connect(m_CacheTimer,SIGNAL(timeout()),this,SLOT(handleTimer()));
}

QKnxIOWorker::~QKnxIOWorker()
{

}

int QKnxIOWorker::readCacheAppend(QKnxFrame *frame)
{
    if (!frame) {
        return false;
    }
    int ret = m_ReadQueue.enqueue(frame);
    if (!ret) {
        qDebug() << "Warning: queue full!!";
    }
    return ret;
}

QKnxFrame *QKnxIOWorker::readCacheTake()
{
    return m_ReadQueue.dequeue();
}

int QKnxIOWorker::writeCacheAppend(QKnxFrame *frame)
{
    if (!frame) {
        return false;
    }
    int ret = m_WriteQueue.enqueue(frame);
    if (!ret) {
        qDebug() << "Warning: queue full!!";
    }
    return ret;
}

QKnxFrame *QKnxIOWorker::writeCacheTake()
{
    return m_WriteQueue.dequeue();
}

void QKnxIOWorker::start()
{
    m_CacheTimer->start();
}

void QKnxIOWorker::stop()
{
    m_CacheTimer->stop();
    emit(finished());
}

void QKnxIOWorker::handleTimer()
{
    // pure virtual
}

