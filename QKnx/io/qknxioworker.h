#ifndef QKNXIOWORKER_H
#define QKNXIOWORKER_H

#include "netip/qknxframe.h"
#include "qknxqueue.h"

#include <QTimer>

class QKNX_SHARED_EXPORT QKnxIOWorker : public QObject
{
    Q_OBJECT
public:
    explicit QKnxIOWorker();
    virtual ~QKnxIOWorker();

    // read cache
    int readCacheAppend(QKnxFrame* frame);
    QKnxFrame* readCacheTake();

    // write cache
    int writeCacheAppend(QKnxFrame* frame);
    QKnxFrame* writeCacheTake();

signals:
    void finished();
    void framePending();

protected:
    QTimer* m_CacheTimer;
    QKnxQueue<QKnxFrame*> m_ReadQueue;
    QKnxQueue<QKnxFrame*> m_WriteQueue;

protected slots:
    virtual void start();
    virtual void stop();

    virtual void handleTimer() = 0;

};

#endif // QKNXIOWORKER_H
