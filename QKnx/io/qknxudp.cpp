#include "qknxudp.h"

#include "qknxudpworker.h"

static int dummyQHostAddress = qRegisterMetaType<QHostAddress>("QHostAddress");

QKnxUdp::QKnxUdp(QObject* parent) : QKnxIO(parent)
{
    m_Worker = new QKnxUdpWorker();
    init();
}

QKnxUdp::~QKnxUdp()
{

}

void QKnxUdp::listen(QHostAddress listen_address, quint16 port, QHostAddress multicast_address)
{
    QKnxUdpWorker* worker = dynamic_cast<QKnxUdpWorker*>(m_Worker);
    if (worker) {
        worker->workerListen(listen_address,port,multicast_address);
    }
}

void QKnxUdp::close()
{
    QKnxUdpWorker* worker = dynamic_cast<QKnxUdpWorker*>(m_Worker);
    if (worker) {
        worker->workerClose();
    }
}

