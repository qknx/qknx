#ifndef QKNXUDP_H
#define QKNXUDP_H

#include "io/qknxio.h"

#include <QHostAddress>

class QKNX_SHARED_EXPORT QKnxUdp : public QKnxIO
{
    Q_OBJECT
public:
    explicit QKnxUdp(QObject *parent = Q_NULLPTR);
    virtual ~QKnxUdp();

    void listen(QHostAddress listen_address = QHostAddress::AnyIPv4, quint16 port = QKNX_UDP_DEFAULT_PORT, QHostAddress multicast_address = QHostAddress());

    void close();

signals:

public slots:

private:

};

#endif // QKNXUDP_H
