#include "qknxudpworker.h"

#include <QUdpSocket>
#include <QMetaEnum>
#include <QThread>

QKnxUdpWorker::QKnxUdpWorker() : m_Socket(Q_NULLPTR), m_Port(QKNX_UDP_DEFAULT_PORT)
{
    connect(this,SIGNAL(workerListen(QHostAddress,quint16,QHostAddress)),this,SLOT(listen(QHostAddress,quint16,QHostAddress)));
    connect(this,SIGNAL(workerClose()),this,SLOT(close()));
}

QKnxUdpWorker::~QKnxUdpWorker()
{

}

void QKnxUdpWorker::start()
{
    if (!m_Socket) {
        m_Socket = new QUdpSocket(this);
        connect(m_Socket,SIGNAL(readyRead()),this,SLOT(processPendingDatagrams()));
    }
    QKnxIOWorker::start();
}

void QKnxUdpWorker::stop()
{

    QKnxIOWorker::stop();
}

void QKnxUdpWorker::handleTimer()
{
    QKnxFrame* frame = writeCacheTake();
    while (frame) {
        QByteArray datagram;
        QKnxStream fs(&datagram,300);
        fs << *frame;
        fs.finishWrite();

        m_Socket->writeDatagram(datagram,frame->remoteHost,frame->remotePort);

        delete frame;

        // read next frame
        frame = writeCacheTake();
    }
}

void QKnxUdpWorker::listen(QHostAddress listen_address, quint16 port, QHostAddress multicast_address)
{
    if (!m_Socket) {
        return;
    }

    m_ListenAddress = listen_address;
    m_Port = port;
    m_MulticastAddress = multicast_address;

    bool ret = m_Socket->bind(m_ListenAddress,m_Port,QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint);
    if (!ret) {
        QString debug_msg("Warning: Bind failed on %1 port %2 with protocol %3.");
        QString protocol_str = QMetaEnum::fromType<QAbstractSocket::NetworkLayerProtocol>().valueToKey(m_ListenAddress.protocol());
        debug_msg = debug_msg.arg(m_ListenAddress.toString()).arg(QString::number(m_Port)).arg(protocol_str);
        qDebug() << debug_msg.toStdString().c_str();
        return;
    }

    m_Socket->setSocketOption(QUdpSocket::MulticastLoopbackOption,false);

    QHostAddress default_multicast;
    if (m_ListenAddress.protocol() == QAbstractSocket::IPv4Protocol) {
        default_multicast = QKnxEndpoint::defaultMulticastIPv4();
    } else if (m_ListenAddress.protocol() == QAbstractSocket::IPv6Protocol) {
        default_multicast = QKnxEndpoint::defaultMulticastIPv6();
    }

    if (!m_MulticastAddress.isNull() && !default_multicast.isNull()) {
        if (m_MulticastAddress.protocol() != m_ListenAddress.protocol()) {
            qDebug() << "Warning: Multicast address protocol does not match listen address protocol!";
            return;
        }
        m_Socket->joinMulticastGroup(default_multicast);
        if (m_MulticastAddress != default_multicast) {
            m_Socket->joinMulticastGroup(m_MulticastAddress);
        }
    }
}

void QKnxUdpWorker::close()
{
    if (!m_Socket) {
        return;
    }
    m_Socket->close();
}

void QKnxUdpWorker::processPendingDatagrams()
{
    QByteArray datagram;
    while (m_Socket->hasPendingDatagrams()) {
        QKnxFrame* frame = new QKnxFrame;
        datagram.resize(m_Socket->pendingDatagramSize());
        m_Socket->readDatagram(datagram.data(), datagram.size(), &frame->remoteHost, &frame->remotePort);

        // stream datagram to frame object
        QKnxStream fs(datagram);
        fs >> *frame;

        // try adding object to queue
        int ret = readCacheAppend(frame);
        if (!ret) {
            // TODO: Add handling of full queue
            delete frame;
        }

        if (ret == 1) {
            framePending();
        }
    }
}
