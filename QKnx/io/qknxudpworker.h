#ifndef QKNXUDPWORKER_H
#define QKNXUDPWORKER_H

#include "qknxioworker.h"

#include <QElapsedTimer>
#include <QMutex>
#include <QHostAddress>

class QUdpSocket;

class QKNX_SHARED_EXPORT QKnxUdpWorker : public QKnxIOWorker
{
  Q_OBJECT
public:
    explicit QKnxUdpWorker();
    virtual ~QKnxUdpWorker();

signals:
    void workerListen(QHostAddress listen_address, quint16 port, QHostAddress multicast_address);
    void workerClose();

protected slots:
    virtual void start();
    virtual void stop();

    virtual void handleTimer();

    virtual void listen(QHostAddress listen_address, quint16 port, QHostAddress multicast_address);
    virtual void close();

private:
    QUdpSocket* m_Socket;

    QHostAddress m_ListenAddress;
    quint16 m_Port;
    QHostAddress m_MulticastAddress;

private slots:
    void processPendingDatagrams();
};

#endif // QKNXUDPWORKER_H
