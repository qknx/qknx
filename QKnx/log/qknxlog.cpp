#include "qknxlog.h"

void MessageLogger(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    // add msg to queue (for gui etc)
    QKnxLogMessage log_msg;
    log_msg.type = type;
    log_msg.file = context.file;
    log_msg.line = context.line;
    log_msg.function = context.function;
    log_msg.message = msg;
    QKnxLog::log(log_msg);
}

QKnxLog::QKnxLog()
{
    qRegisterMetaType<QKnxLogMessage>();
}

QKnxLog *QKnxLog::instance()
{
    static QKnxLog log;
    return &log;
}

void QKnxLog::registerLogger(bool enable_console)
{
#ifdef Q_OS_ANDROID
    return;
#endif

    qInstallMessageHandler(MessageLogger);
    if (enable_console) {
        consoleEnable();
    }
}

void QKnxLog::attach(QKnxLogOutput *output)
{
    connect(instance(),SIGNAL(logReceived(QKnxLogMessage)),output,SLOT(acceptLog(QKnxLogMessage)));
}

void QKnxLog::detach(QKnxLogOutput *output)
{
    disconnect(instance(),SIGNAL(logReceived(QKnxLogMessage)),output,SLOT(acceptLog(QKnxLogMessage)));
}

QKnxLogConsole *QKnxLog::consoleInstance()
{
    static QKnxLogConsole console;
    if (!consoleThread()->isRunning()) {
        console.moveToThread(consoleThread());
        consoleThread()->start();
    }
    return &console;
}

QThread *QKnxLog::consoleThread()
{
    static QThread thread;
    return &thread;
}

