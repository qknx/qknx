#ifndef QKNXLOG_H
#define QKNXLOG_H

#include "qknxlogconsole.h"

#include <QObject>
#include <QThread>

class QKNX_SHARED_EXPORT QKnxLog : public QObject
{
    Q_OBJECT
public:
    explicit QKnxLog();
    virtual ~QKnxLog() {}

    // factory singleton
    static QKnxLog* instance();
    bool isInstance() { return (this == instance()); }

    // registration
    static void registerLogger(bool enable_console = true);
    static void enable(bool enable_console = true) { registerLogger(enable_console); }
    static void disable() { qInstallMessageHandler(0); }

    // add log messages to queue
    static void log(QKnxLogMessage msg) { instance()->logPrivate(msg); }

    // attaching
    static void attach(QKnxLogOutput* output);
    static void detach(QKnxLogOutput* output);

    // console
    static QKnxLogConsole* consoleInstance();
    static QThread* consoleThread();
    static void consoleEnable() { attach(consoleInstance()); }
    static void consoleDisable() { detach(consoleInstance()); }

signals:
    void logReceived(QKnxLogMessage msg);

private slots:
    void logPrivate(QKnxLogMessage msg) { emit(logReceived(msg)); }
};

#endif // QKNXLOG_H
