#include "qknxlogconsole.h"

#include <cstdlib>
#include <cstdio>

QKnxLogConsole::QKnxLogConsole()
{

}

void QKnxLogConsole::acceptLog(QKnxLogMessage msg)
{
    switch (msg.type) {
    case QtDebugMsg:
        //fprintf(stderr, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        //fprintf(stdout, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fprintf(stderr, "Debug: [%s] %s\n", qPrintable(msg.function), qPrintable(msg.message));
        break;
    case QtInfoMsg:
        //fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        //fprintf(stdout, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fprintf(stderr, "Info: [%s] %s\n", qPrintable(msg.function), qPrintable(msg.message));
        break;
    case QtWarningMsg:
        //fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg, msg.file, msg.line, msg.function);
        fprintf(stderr, "Warning: [%s] %s\n", qPrintable(msg.function), qPrintable(msg.message));
        break;
    case QtCriticalMsg:
        //fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fprintf(stderr, "Critical: [%s] %s\n", qPrintable(msg.function), qPrintable(msg.message));
        break;
    case QtFatalMsg:
        //fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fprintf(stderr, "Fatal: [%s] %s\n", qPrintable(msg.function), qPrintable(msg.message));
        abort();
    default:
        break;
    }
    fflush(stderr);
}
