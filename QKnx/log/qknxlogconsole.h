#ifndef QKNXLOGCONSOLE_H
#define QKNXLOGCONSOLE_H

#include "qknxlogoutput.h"

class QKNX_SHARED_EXPORT QKnxLogConsole : public QKnxLogOutput
{
    Q_OBJECT
public:
    explicit QKnxLogConsole();
    virtual ~QKnxLogConsole() {}

public slots:
    virtual void acceptLog(QKnxLogMessage msg);
};

#endif // QKNXLOGCONSOLE_H
