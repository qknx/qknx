#ifndef QKNXLOGMESSAGE_H
#define QKNXLOGMESSAGE_H

#include "qknxglobal.h"

class QKNX_SHARED_EXPORT QKnxLogMessage
{
public:
    explicit QKnxLogMessage() {
        QKNX_REGISTER_TYPE(QKnxLogMessage);
    }
    virtual ~QKnxLogMessage() {}

    QtMsgType type;
    QString file;
    QString line;
    QString function;
    QString message;
};
Q_DECLARE_METATYPE(QKnxLogMessage)

#endif // QKNXLOGMESSAGE_H
