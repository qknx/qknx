#include "qknxlogoutput.h"

#include "qknxlog.h"

QKnxLogOutput::QKnxLogOutput()
{
    qRegisterMetaType<QKnxLogMessage>();
}

void QKnxLogOutput::attach()
{
    QKnxLog::attach(this);
}

void QKnxLogOutput::detach()
{
    QKnxLog::detach(this);
}
