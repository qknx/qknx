#ifndef QKNXLOGOUTPUT_H
#define QKNXLOGOUTPUT_H

#include "qknxlogmessage.h"

class QKNX_SHARED_EXPORT QKnxLogOutput : public QObject
{
    Q_OBJECT
public:
    explicit QKnxLogOutput();
    virtual ~QKnxLogOutput() {}

    void attach();
    void detach();

public slots:
    virtual void acceptLog(QKnxLogMessage msg) { Q_UNUSED(msg); }
};

#endif // QKNXLOGOUTPUT_H
