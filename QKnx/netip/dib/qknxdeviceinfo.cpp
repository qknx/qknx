#include "qknxdeviceinfo.h"

QKnxDeviceInfo::QKnxDeviceInfo()
{
    m_ReadDibType = -1;
    m_Address.setFormat(QKnxAddress::FormatPhysical);
    m_ProjectInstallation = 0x0000;
}

QKnxDeviceInfo::QKnxDeviceInfo(const QByteArray &data)
{
    m_ReadDibType = -1;
    m_Address.setFormat(QKnxAddress::FormatPhysical);
    m_ProjectInstallation = 0x0000;
    setDeviceInfo(data);
}

QKnxDeviceInfo::~QKnxDeviceInfo()
{

}

bool QKnxDeviceInfo::setDeviceInfo(const QByteArray &data)
{
    QKnxStream fs(data);
    fs >> *this;
    return isValid();
}

QByteArray QKnxDeviceInfo::toByteArray() const
{
    QByteArray ret_value;
    QKnxStream fs(&ret_value,128);
    fs << *this;
    fs.finishWrite();
    return ret_value;
}

bool QKnxDeviceInfo::isValid() const
{
    if (m_ReadDibType != -1 && m_ReadDibType != dibType()) {
        return false;
    }

    // TODO: test more validity information

    return true;
}

void QKnxDeviceInfo::setSerial(const QByteArray &serial)
{
    m_Serial = serial;
    while (m_Serial.length() < QKNX_DEVICE_INFO_SERIAL_LEN) {
        m_Serial.prepend((char)0x00);
    }
    if (serial.length() > QKNX_DEVICE_INFO_SERIAL_LEN) {
        m_Serial = m_Serial.left(QKNX_DEVICE_INFO_SERIAL_LEN);
    }
}

void QKnxDeviceInfo::setFriendlyName(const QByteArray &friendly_name)
{
    if (m_FriendlyName.length() != QKNX_DEVICE_INFO_FRIENDLY_NAME_LEN) {
        m_FriendlyName.resize(QKNX_DEVICE_INFO_FRIENDLY_NAME_LEN);
    }

    int input_len = friendly_name.length();
    char* data = m_FriendlyName.data();
    for (int x=0; x < QKNX_DEVICE_INFO_FRIENDLY_NAME_LEN; x++) {
        if (x < input_len) {
            data[x] = friendly_name[x];
        } else {
            data[x] = 0x00;
        }
    }
}

QKnxStream &operator>>(QKnxStream &fs, QKnxDeviceInfo &data)
{
    // block length and type
    quint8 len, dib_type;
    fs >> len;
    fs >> dib_type;

    // on error return with invalid dib type
    data.m_ReadDibType = -1;
    if (dib_type != QKnxDeviceInfo::dibType()) {
        fs.setPosition(fs.position()-2);
        return fs;
    }

    // knx medium
    quint8 mediums;
    fs >> mediums;
    data.m_Mediums = (QKnxDeviceInfo::Mediums)mediums;

    // device status
    quint8 statuses;
    fs >> statuses;
    data.m_Statuses = (QKnxDeviceInfo::Statuses)statuses;

    // knx address
    data.m_Address.setFormat(QKnxAddress::FormatPhysical);
    fs >> data.m_Address;

    // project and installation id
    fs >> data.m_ProjectInstallation;

    // device serial
    if (data.m_Serial.length() != QKNX_DEVICE_INFO_SERIAL_LEN) {
        data.m_Serial.resize(QKNX_DEVICE_INFO_SERIAL_LEN);
    }
    fs >> data.m_Serial;

    // device multicast address
    quint32 mcast_address;
    fs >> mcast_address;
    data.m_MulticastAddress.setAddress(mcast_address);

    // device mac address
    fs >> data.m_MacAddress;

    // test for rest length of dib
    quint8 rest_len = len - 24;
    if (rest_len != QKNX_DEVICE_INFO_FRIENDLY_NAME_LEN) {
        qDebug() << "Warning: Rest length does not match friendly name length";
    }

    // friendly name
    if (data.m_FriendlyName.length() != QKNX_DEVICE_INFO_FRIENDLY_NAME_LEN) {
        data.m_FriendlyName.resize(QKNX_DEVICE_INFO_FRIENDLY_NAME_LEN);
    }
    fs >> data.m_FriendlyName;

    // on success set dib type
    if (!fs.error()) {
        data.m_ReadDibType = dib_type;
    }

    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const QKnxDeviceInfo &data)
{
    // write block length and type, remember pos for length insert
    int start_pos = fs.position();
    fs << (quint8)0x00;
    fs << (quint8)QKnxDeviceInfo::dibType();

    // knx medium
    fs << (quint8)data.m_Mediums;

    // device status
    fs << (quint8)data.m_Statuses;

    // knx address
    fs << data.m_Address;

    // project and installation id
    fs << data.m_ProjectInstallation;

    // device serial
    fs << data.m_Serial;

    // device multicast address
    fs << data.m_MulticastAddress.toIPv4Address();

    // device mac address
    fs << data.m_MacAddress;

    // friendly name
    fs << data.m_FriendlyName;

    // add length
    quint8 len = fs.position()-start_pos;
    fs.setByte(start_pos,len);

    return fs;
}
