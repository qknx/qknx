#ifndef QKNXDEVICEINFO_H
#define QKNXDEVICEINFO_H

#include "netip/qknxnetip.h"
#include "netip/qknxmacaddress.h"

#define QKNX_DEVICE_INFO_SERIAL_LEN 6
#define QKNX_DEVICE_INFO_FRIENDLY_NAME_LEN 30

class QKNX_SHARED_EXPORT QKnxDeviceInfo : public QKnxNetIp
{
    Q_GADGET
public:
    explicit QKnxDeviceInfo();
    explicit QKnxDeviceInfo(const QByteArray& data);
    virtual ~QKnxDeviceInfo();

    static DibType dibType() { return DIB_DEVICE_INFO; }

    // import/export
    bool setDeviceInfo(const QByteArray& data);
    static QKnxDeviceInfo fromByteArray(const QByteArray& data) { return QKnxDeviceInfo(data); }
    QByteArray toByteArray() const;

    bool isReadOk() const { return (m_ReadDibType == dibType()); }
    bool isValid() const;

    // medium
    Mediums mediums() const { return m_Mediums; }
    QString mediumsString() const { return mediumsString(m_Mediums); }
    static QString mediumsString(const Mediums& mediums) { return QMetaEnum::fromType<Mediums>().valueToKeys(mediums); }
    void setMediums(const Mediums& mediums) { m_Mediums = mediums; }
    void setMediums(const int& mediums) { m_Mediums = Mediums(mediums); }
    bool hasMedium(const Medium& medium) const { return m_Mediums.testFlag(medium); }
    static QString mediumString(const Medium& medium) { return QMetaEnum::fromType<Mediums>().valueToKey(medium); }
    void setMedium(const Medium& medium, const bool& value) { m_Mediums.setFlag(medium,value); }
    void mediumEnable(const Medium& medium) { setMedium(medium,true); }
    void mediumDisable(const Medium& medium) { setMedium(medium,false); }

    // status
    Statuses statuses() const { return m_Statuses; }
    QString statusesString() const { return statusesString(m_Statuses); }
    static QString statusesString(const Statuses& statuses) { return QMetaEnum::fromType<Statuses>().valueToKeys(statuses); }
    void setStatuses(const Statuses& statuses) { m_Statuses = Statuses(statuses); }
    void setStatuses(const int& statuses) { m_Statuses = Statuses(statuses); }
    bool hasStatus(const Status& status) const { return m_Statuses.testFlag(status); }
    static QString statusString(const Status& status) { return QMetaEnum::fromType<Statuses>().valueToKey(status); }
    void setStatus(const Status& status, const bool& value) { m_Statuses.setFlag(status,value); }
    void statusEnable(const Status& status) { setStatus(status, true); }
    void statusDisable(const Status& status) { setStatus(status, false); }

    // address
    QKnxAddress address() const { return m_Address; }
    void setAddress(const QKnxAddress& address) { m_Address = address; }
    void setAddress(const quint16& address) { m_Address.setAddress(address); }
    void setAddress(const QString& address) { m_Address.setAddress(address); }

    // project installation
    quint16 projectInstallation() const { return m_ProjectInstallation; }
    void setProjectInstallation(const quint16& project_installation) { m_ProjectInstallation = project_installation; }

    // device serial
    QByteArray serial() const { return m_Serial; }
    QString serialString() const { return QString::fromLatin1(m_Serial.toHex()); }
    void setSerial(const QByteArray& serial);
    void setSerial(const QString& serial) { setSerial(QByteArray::fromHex(serial.toLatin1())); }
    void setSerial(const char* serial) { setSerial(QString::fromLatin1(serial)); }

    // multicast address
    QHostAddress multicastAddress() const { return m_MulticastAddress; }
    void setMulticastAddress(const QHostAddress& multicast_address) { m_MulticastAddress = multicast_address; }

    // mac address
    QKnxMacAddress macAddress() const { return m_MacAddress; }
    void setMacAddress(const QKnxMacAddress& mac_address) { m_MacAddress = mac_address; }
    void setMacAddress(const QString& mac_address) { m_MacAddress.setAddress(mac_address); }
    void setMacAddress(const QByteArray& mac_address) { m_MacAddress.setAddress(mac_address); }
    void setMacAddress(const char* mac_address) { m_MacAddress.setAddress(mac_address); }

    // device friendly name
    QByteArray friendlyName() const { return m_FriendlyName; }
    QString friendlyNameString() const { return QString::fromUtf8(m_FriendlyName); }
    void setFriendlyName(const QByteArray& friendly_name);
    void setFriendlyName(const QString& friendly_name) { setFriendlyName(friendly_name.toUtf8()); }

private:
    int m_ReadDibType;
    Mediums m_Mediums;
    Statuses m_Statuses;
    QKnxAddress m_Address;
    quint16 m_ProjectInstallation;
    QByteArray m_Serial;
    QHostAddress m_MulticastAddress;
    QKnxMacAddress m_MacAddress;
    QByteArray m_FriendlyName;

    friend QKnxStream& operator>>(QKnxStream& fs, QKnxDeviceInfo& data);
    friend QKnxStream& operator<<(QKnxStream& fs, const QKnxDeviceInfo& data);
};

QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QKnxDeviceInfo& data);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const QKnxDeviceInfo& data);

#endif // QKNXDEVICEINFO_H
