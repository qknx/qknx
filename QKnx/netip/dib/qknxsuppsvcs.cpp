#include "qknxsuppsvcs.h"

QKnxSuppSvcs::QKnxSuppSvcs()
{
    m_ReadDibType = -1;
}

QKnxSuppSvcs::QKnxSuppSvcs(const QByteArray &data)
{
    m_ReadDibType = -1;
    setSuppSvcs(data);
}

QKnxSuppSvcs::~QKnxSuppSvcs()
{

}

bool QKnxSuppSvcs::setSuppSvcs(const QByteArray &data)
{
    QKnxStream fs(data);
    fs >> *this;
    return isValid();
}

QByteArray QKnxSuppSvcs::toByteArray() const
{
    QByteArray ret_value;
    QKnxStream fs(&ret_value,32);
    fs << *this;
    fs.finishWrite();
    return ret_value;
}

bool QKnxSuppSvcs::isValid() const
{
    if (m_ReadDibType != -1 && m_ReadDibType != dibType()) {
        return false;
    }

    if (m_SuppSvcs.count() == 0) {
        return false;
    }

    return true;
}

QKnxStream &operator>>(QKnxStream &fs, QKnxSuppSvcs &data)
{
    quint8 len, dib_type;
    fs >> len;
    fs >> dib_type;

    // on error return with invalid dib type
    data.m_ReadDibType = -1;
    if (dib_type != QKnxSuppSvcs::dibType()) {
        fs.setPosition(fs.position()-2);
        return fs;
    }

    int to_read = len-2;
    data.m_SuppSvcs.clear();
    quint8 tmp_svc, tmp_version;
    while (to_read > 0) {
        fs >> tmp_svc;
        fs >> tmp_version;
        data.m_SuppSvcs.insert((QKnxSuppSvcs::SuppSvc)tmp_svc,tmp_version);
        to_read -= 2;
    }

    // on success set dib type
    if (!fs.error()) {
        data.m_ReadDibType = dib_type;
    }

    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const QKnxSuppSvcs &data)
{
    // write length and type, remember length pos for overwriting
    int start_pos = fs.position();
    fs << (quint8)0x00;
    fs << (quint8)QKnxSuppSvcs::dibType();

    foreach (QKnxSuppSvcs::SuppSvc svc, data.m_SuppSvcs.keys()) {
        fs << (quint8)svc;
        fs << data.m_SuppSvcs.value(svc,0x00);
    }

    // add length
    quint8 len = fs.position()-start_pos;
    fs.setByte(start_pos,len);

    return fs;
}
