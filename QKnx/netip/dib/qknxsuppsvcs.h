#ifndef QKNXSUPPSVCS_H
#define QKNXSUPPSVCS_H

#include "netip/qknxnetip.h"

#include <QMutex>

class QKNX_SHARED_EXPORT QKnxSuppSvcs : public QKnxNetIp
{
    Q_GADGET
public:
    explicit QKnxSuppSvcs();
    explicit QKnxSuppSvcs(const QByteArray& data);
    virtual ~QKnxSuppSvcs();

    static DibType dibType() { return DIB_SUPP_SVC; }

    // import/export
    bool setSuppSvcs(const QByteArray& data);
    static QKnxSuppSvcs fromByteArray(const QByteArray& data) { return QKnxSuppSvcs(data); }
    QByteArray toByteArray() const;

    bool isReadOk() const { return (m_ReadDibType == dibType()); }
    bool isValid() const;

    // services & service map
    SuppSvcMap suppSvcs() const { return m_SuppSvcs; }
    void setSuppSvcs(const SuppSvcMap& supp_svcs) { m_SuppSvcs = supp_svcs; }
    bool hasSuppSvc(const SuppSvc& supp_svc) { return m_SuppSvcs.contains(supp_svc); }
    static QString suppSvcString(const SuppSvc& supp_svc) { return QMetaEnum::fromType<SuppSvc>().valueToKey(supp_svc); }
    quint8 suppSvcVersion(const SuppSvc& supp_svc) { return m_SuppSvcs.value(supp_svc,0); }
    void suppSvcEnable(const SuppSvc& supp_svc, quint8 version = 1) { m_SuppSvcs.insert(supp_svc,version); }
    void suppSvcDisable(const SuppSvc& supp_svc) { m_SuppSvcs.remove(supp_svc); }
    void suppSvcsClear() { m_SuppSvcs.clear(); }

private:
    int m_ReadDibType;
    SuppSvcMap m_SuppSvcs;

    friend QKnxStream& operator>>(QKnxStream& fs, QKnxSuppSvcs& data);
    friend QKnxStream& operator<<(QKnxStream& fs, const QKnxSuppSvcs& data);
};

QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QKnxSuppSvcs& data);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const QKnxSuppSvcs& data);

#endif // QKNXSUPPSVCS_H
