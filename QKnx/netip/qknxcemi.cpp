#include "qknxcemi.h"

#include <QMutex>

// error descriptions
static QKnxCemi::MessageCodeDescriptionMap message_code_descriptions;
static QMutex message_code_descriptions_mutex;

QKnxCemi::QKnxCemi()
{
    messageCode = MESSAGE_NONE;
}

QKnxCemi::QKnxCemi(const QKnxCemi::MessageCode &msg_code)
{
    messageCode = msg_code;

    // M_PropRead
    interfaceObjectType = 0x0000;
    objectInstance = 0x00;
    propertyIdentifier = 0x00;
    numberOfElements = 0x00;
    startIndex = 0x0000;
    errorCode = 0x00;
}

QKnxCemi::QKnxCemi(const QByteArray &data)
{
    messageCode = MESSAGE_NONE;

    // M_PropRead
    interfaceObjectType = 0x0000;
    objectInstance = 0x00;
    propertyIdentifier = 0x00;
    numberOfElements = 0x00;
    startIndex = 0x0000;
    errorCode = 0x00;

    // set data
    setCemi(data);
}

QKnxCemi::~QKnxCemi()
{

}

QKnxCemi::MessageCodeDescriptionMap QKnxCemi::allMessageCodeDescriptions()
{
    QMutexLocker lock(&message_code_descriptions_mutex);
    MessageCodeDescriptionMap& descs = message_code_descriptions;
    if (descs.isEmpty()) {
        descs.insert(L_RAW_REQ, "L_Raw.req");
        descs.insert(L_DATA_REQ, "L_Data.req");
        descs.insert(L_POLL_DATA_REQ, "L_Poll_Data.req");
        descs.insert(L_POLL_DATA_CON, "L_Poll_Data.con");
        descs.insert(L_DATA_IND, "L_Data.ind");
        descs.insert(L_BUSMON_IND, "L_Busmon.ind");
        descs.insert(L_RAW_IND, "L_Raw.ind");
        descs.insert(L_DATA_CON, "L_Data.con");
        descs.insert(L_RAW_CON, "L_Raw.con");
        descs.insert(T_DATA_CONNEC_REQ, "T_Data_Connected.req");
        descs.insert(T_DATA_INDV_REQ, "T_Data_Individual.req");
        descs.insert(T_DATA_CONNEC_IND, "T_Data_Connected.ind");
        descs.insert(T_DATA_INDV_IND, "T_Data_Individual.ind");
        descs.insert(M_RESET_IND, "M_Reset.ind");
        descs.insert(M_RESET_REQ, "M_Reset.req");
        descs.insert(M_PROPWRITE_CON, "M_PropWrite.con");
        descs.insert(M_PROPWRITE_REQ, "M_PropWrite.req");
        descs.insert(M_PROPINFO_IND, "M_PropInfo.ind");
        descs.insert(M_FUNCPROPCOM_REQ, "M_FuncPropCommand.req");
        descs.insert(M_FUNCPROPSTATREAD_REQ, "M_FuncPropStateRead.req");
        descs.insert(M_FUNCPROPCOM_CON, "M_FuncPropCommand/StateRead.con");
        descs.insert(M_PROPREAD_CON, "M_PropRead.con");
        descs.insert(M_PROPREAD_REQ, "M_PropRead.req");
    }
    return descs;
}

bool QKnxCemi::setCemi(const QByteArray &data)
{
    QKnxStream fs(data);
    fs >> *this;
    return isValid();
}

QByteArray QKnxCemi::toByteArray() const
{
    QByteArray ret_value;
    QKnxStream fs(&ret_value,512);
    fs << *this;
    fs.finishWrite();
    return ret_value;
}

bool QKnxCemi::isValid() const
{
    if (messageCode == MESSAGE_NONE) {
        return false;
    }

    // TODO: More validity checking

    return true;
}

QKnxStream &operator>>(QKnxStream &fs, QKnxCemi &data)
{
    quint8 msg_code_tmp;
    fs >> msg_code_tmp;
    data.messageCode = (QKnxCemi::MessageCode)msg_code_tmp;

    // L_Data
    bool L_Data = false;
    if (data.messageCode == QKnxCemi::L_DATA_REQ || data.messageCode == QKnxCemi::L_DATA_IND || data.messageCode == QKnxCemi::L_DATA_CON) {
        L_Data = true;
    }

    // M_PropRead
    bool M_PropRead = false;
    if (data.messageCode == QKnxCemi::M_PROPREAD_REQ || data.messageCode == QKnxCemi::M_PROPREAD_CON) {
        M_PropRead = true;
    }

    if (L_Data) {
        // additional info
        quint8 add_info_len;
        fs >> add_info_len;
        data.additionalInformation.clear();
        if (add_info_len)
        {
            data.additionalInformation.resize(add_info_len);
            fs >> data.additionalInformation;
        }
    }

    if (L_Data) {
        // kxn telegram
        data.telegram.setCemiIO(true);
        fs >> data.telegram;
    }

    if (M_PropRead) {
        fs >> data.interfaceObjectType;
        fs >> data.objectInstance;
        fs >> data.propertyIdentifier;

        quint16 tmp;
        fs >> tmp;
        data.numberOfElements = (quint8)((tmp >> 12) & 0x0F);
        data.startIndex = tmp & 0x0FFF;

        if (data.messageCode == QKnxCemi::M_PROPREAD_CON) {
            fs >> data.errorCode;
        }
    }

    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const QKnxCemi &data)
{
    fs << (quint8)data.messageCode;

    // L_Data
    bool L_Data = false;
    if (data.messageCode == QKnxCemi::L_DATA_REQ || data.messageCode == QKnxCemi::L_DATA_IND || data.messageCode == QKnxCemi::L_DATA_CON) {
        L_Data = true;
    }

    // M_PropRead
    bool M_PropRead = false;
    if (data.messageCode == QKnxCemi::M_PROPREAD_REQ || data.messageCode == QKnxCemi::M_PROPREAD_CON) {
        M_PropRead = true;
    }

    if (L_Data) {
        fs << (quint8)data.additionalInformation.length();
        if (!data.additionalInformation.isEmpty()) {
            fs << data.additionalInformation;
        }
    }

    if (L_Data) {
        fs << data.telegram;
    }

    if (M_PropRead) {
        fs << data.interfaceObjectType;
        fs << data.objectInstance;
        fs << data.propertyIdentifier;

        quint16 tmp = ((quint16)data.numberOfElements & 0x0F) << 12;
        tmp |= data.startIndex & 0x0FFF;
        fs << tmp;

        if (data.messageCode == QKnxCemi::M_PROPREAD_CON) {
            fs << data.errorCode;
        }
    }

    return fs;
}
