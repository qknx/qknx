#ifndef QKNXCEMI_H
#define QKNXCEMI_H

#include "qknxstream.h"
#include "qknxtelegram.h"

class QKNX_SHARED_EXPORT QKnxCemi
{
    Q_GADGET
public:
    enum MessageCode {
        MESSAGE_NONE = 0x00,
        L_RAW_REQ = 0x10,
        L_DATA_REQ = 0x11,
        L_POLL_DATA_REQ = 0x13,
        L_POLL_DATA_CON = 0x25,
        L_DATA_IND = 0x29,
        L_BUSMON_IND = 0x2B,
        L_RAW_IND = 0x2D,
        L_DATA_CON = 0x2E,
        L_RAW_CON = 0x2F,
        T_DATA_CONNEC_REQ = 0x41,
        T_DATA_INDV_REQ = 0x4A,
        T_DATA_CONNEC_IND = 0x89,
        T_DATA_INDV_IND = 0x94,
        M_RESET_IND = 0xF0,
        M_RESET_REQ = 0xF1,
        M_PROPWRITE_CON = 0xF5,
        M_PROPWRITE_REQ = 0xF6,
        M_PROPINFO_IND = 0xF7,
        M_FUNCPROPCOM_REQ = 0xF8,
        M_FUNCPROPSTATREAD_REQ = 0xF9,
        M_FUNCPROPCOM_CON = 0xFA,
        M_PROPREAD_CON = 0xFB,
        M_PROPREAD_REQ = 0xFC
    };
    Q_ENUM(MessageCode)
    typedef QMap<MessageCode, QString> MessageCodeDescriptionMap;

    explicit QKnxCemi();
    explicit QKnxCemi(const MessageCode& msg_code);
    explicit QKnxCemi(const QByteArray& data);
    virtual ~QKnxCemi();

    static MessageCodeDescriptionMap allMessageCodeDescriptions();
    static QString messageCodeDescription(const MessageCode& message_code) { return allMessageCodeDescriptions().value(message_code); }
    static QString messageCodeString(const MessageCode& message_code) { return QMetaEnum::fromType<MessageCode>().valueToKey(message_code); }

    // import/export
    bool setCemi(const QByteArray& data);
    static QKnxCemi fromByteArray(const QByteArray& data) { return QKnxCemi(data); }
    QByteArray toByteArray() const;

    bool isReadOk() const { return (messageCode != MESSAGE_NONE); }
    bool isValid() const;

    // data
    MessageCode messageCode;
    QByteArray additionalInformation;

    // L_Data
    QKnxTelegram telegram;

    // M_PropRead
    quint16 interfaceObjectType;
    quint8 objectInstance;
    quint8 propertyIdentifier;
    quint8 numberOfElements;
    quint16 startIndex;
    quint8 errorCode;

private:
    friend QKnxStream& operator>>(QKnxStream& fs, QKnxCemi& data);
    friend QKnxStream& operator<<(QKnxStream& fs, const QKnxCemi& data);
};

QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QKnxCemi& data);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const QKnxCemi& data);

#endif // QKNXCEMI_H
