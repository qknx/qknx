#include "qknxclient.h"

#include <QNetworkInterface>

/*
QKnxNetIpAgent::QKnxNetIpAgent(QObject *parent) : QObject(parent)
{
    QKNX_REGISTER_TYPE(QKnxNetIpAgent*);

    m_Protocol = QAbstractSocket::IPv4Protocol;

    m_NumTry = 1;

    // discovery interval timer
    m_Timer.setInterval(2000);
    m_Timer.setSingleShot(false);
    connect(&m_Timer,SIGNAL(timeout()),this,SLOT(discover()));

    // discovery timeout timer
    m_TimeOut.setInterval(5000);
    m_TimeOut.setSingleShot(true);
    connect(&m_TimeOut,SIGNAL(timeout()),this,SLOT(discoveryStop()));
}

QKnxNetIpAgent::~QKnxNetIpAgent()
{

}

void QKnxNetIpAgent::discoveryStart(int num_try)
{
    qDebug() << "";

    m_NumTry = num_try;

    emit(discoveryStarted());

    discover();
}

void QKnxNetIpAgent::discoveryStop()
{
    qDebug() << "";

    m_Timer.stop();
    m_TimeOut.stop();

    qDeleteAll(m_Sockets);
    m_Sockets.clear();

    emit(discoveryStopped());
}

void QKnxNetIpAgent::discover()
{
    qDebug() << "";

    if (m_NumTry == 0) {
        return;
    }

    qDebug() << "discovery.......";

    m_Timer.stop();
    m_TimeOut.stop();

    if (m_Sockets.isEmpty()) {
        quint16 port = 0;
        QList<QNetworkInterface> ifaces = QNetworkInterface::allInterfaces();
        foreach (QNetworkInterface iface, ifaces) {
            if (!iface.isValid() || !iface.flags().testFlag(QNetworkInterface::IsUp)) {
                continue;
            }
            if (iface.flags().testFlag(QNetworkInterface::IsLoopBack)) {
                continue;
            }
            foreach (QNetworkAddressEntry e, iface.addressEntries()) {
                QHostAddress addr = e.ip();
                if (protocol() == QAbstractSocket::IPv4Protocol && addr.protocol() != QAbstractSocket::IPv4Protocol) {
                    continue;
                }
                if (protocol() == QAbstractSocket::IPv6Protocol && addr.protocol() != QAbstractSocket::IPv6Protocol) {
                    continue;
                }

                QUdpSocket* sock = new QUdpSocket(this);
                bool ret = sock->bind(e.ip(),port,QUdpSocket::DontShareAddress);
                if (!ret) {
                    delete sock;
                    continue;
                }
                if (!port) {
                    port = sock->localPort();
                }
                // setup socket connections
                //sock->setSocketOption(QUdpSocket::MulticastLoopbackOption,false);
                //connect(&m_Socket,SIGNAL(stateChanged(QAbstractSocket::SocketState)),this,SLOT(onStateChanged(QAbstractSocket::SocketState)));
                connect(sock,SIGNAL(readyRead()),this,SLOT(processPendingDatagrams()));
                m_Sockets.append(sock);

                //qDebug() << "added socket" << sock->localAddress() << ":" << sock->localPort() << "@" << sock->localAddress().protocol();
            }
        }
    }

    QKnxNetIpSearchRequest req;
    foreach (QUdpSocket* sock, m_Sockets) {
        if (!sock) {
            continue;
        }

        if (sock->state() == QUdpSocket::BoundState) {
            QKnxNetIpEndpoint ep = req.endpoint();
            if (sock->localAddress().protocol() == QAbstractSocket::IPv4Protocol) {
                ep.setHostProtocolCode(QKnxNetIpEndpoint::IPV4_UDP);
            } else if (sock->localAddress().protocol() == QAbstractSocket::IPv6Protocol) {
                ep.setHostProtocolCode(QKnxNetIpEndpoint::IPV6_UDP);
            } else {
                continue;
            }
            ep.setHostAddress(sock->localAddress());
            ep.setPort(sock->localPort());
            req.setEndpoint(ep);

            sock->writeDatagram(req.toByteArray(),QKnxNetIpEndpoint::defaultMulticastIPv4(),QKnxNetIpEndpoint::defaultPort());
        }
    }

    if (m_NumTry < 0) {
        m_Timer.start();
    } else {
        m_NumTry--;
        if (m_NumTry == 0) {
            m_TimeOut.start();
        } else {
            m_Timer.start();
        }
    }
}

void QKnxNetIpAgent::processPendingDatagrams()
{
    QUdpSocket* socket = qobject_cast<QUdpSocket*>(sender());
    if (!socket) {
        return;
    }

    qDebug() << "processing datagram";

    while (socket->hasPendingDatagrams()) {
        QByteArray datagram;
        QHostAddress sender;
        quint16 senderPort;

        datagram.resize(socket->pendingDatagramSize());
        socket->readDatagram(datagram.data(), datagram.size(), &sender, &senderPort);

        // additional filter for looped packages
        if (QNetworkInterface::allAddresses().contains(sender) && socket->localPort() == senderPort) {
            qDebug() << "FILTERED";
            continue;
        }

        QKnxNetIpService* svc = QKnxNetIpService::fromByteArray(datagram);
        if (!svc) {
            continue;
        }

        QKnxNetIpSearchResponse* res = svc->dataPtr<QKnxNetIpSearchResponse>();
        if (!res) {
            continue;
        }

        qDebug() << "got packet at local address:" << socket->localAddress();
        res->setLocalAddress(socket->localAddress());
        emit(discoveryResponse(*res));
    }
}
*/
