#ifndef QKNXNETIPAGENT_H
#define QKNXNETIPAGENT_H

#include "qknxglobal.h"

//#include "service/qknxnetipsearchresponse.h"

#include <QUdpSocket>
#include <QTimer>

/*

class QKNX_SHARED_EXPORT QKnxNetIpAgent : public QObject
{
    Q_OBJECT
public:
    explicit QKnxNetIpAgent(QObject* parent = Q_NULLPTR);
    virtual ~QKnxNetIpAgent();

    QAbstractSocket::NetworkLayerProtocol protocol() const { return m_Protocol; }
    void setProtocol(const QAbstractSocket::NetworkLayerProtocol& protocol) { m_Protocol = protocol; }

signals:
    void discoveryStarted();
    void discoveryStopped();
    //void discoveryResponse(QKnxNetIpSearchResponse response);

public slots:
    void discoveryStart(int num_try = 1);
    void discoveryStop();

private:
    QList<QUdpSocket*> m_Sockets;
    QAbstractSocket::NetworkLayerProtocol m_Protocol;
    QTimer m_Timer;
    QTimer m_TimeOut;
    int m_NumTry; // -1: infinie, 0: none, >0: n times

private slots:
    void discover();
    void processPendingDatagrams();

};
*/

#endif // QKNXNETIPAGENT_H
