#include "qknxconnectinfo.h"

QKnxConnectInfo::QKnxConnectInfo() : infoType(InfoNone), connectionType(QKnxNetIp::CONNECTION_NONE), reserved(0)
{

}

QKnxConnectInfo::~QKnxConnectInfo()
{

}

quint8 QKnxConnectInfo::structureLength() const
{
    if (infoType != InfoRequest && infoType != InfoResponse) {
        return 0;
    }
    if (connectionType == QKnxNetIp::TUNNEL_CONNECTION) {
        return 4;
    }
    return 2;
}

QKnxStream &operator>>(QKnxStream &fs, QKnxConnectInfo &data)
{
    if (data.infoType == QKnxConnectInfo::InfoNone) {
        qDebug() << "Warning: Connect infoType not set.";
        return fs;
    }

    // length
    quint8 structure_length;
    fs >> structure_length;

    // connection type
    quint8 connection_type;
    fs >> connection_type;
    data.connectionType = (QKnxNetIp::ConnectionType)connection_type;

    if (data.infoType == QKnxConnectInfo::InfoRequest) {
        if (data.connectionType == QKnxNetIp::TUNNEL_CONNECTION) {
            // knx layers
            quint8 layers;
            fs >> layers;
            data.knxLayers = (QKnxNetIp::KnxLayers)layers;

            // reserved
            fs >> data.reserved;
        }
    } else if (data.infoType == QKnxConnectInfo::InfoResponse) {
        if (data.connectionType == QKnxNetIp::TUNNEL_CONNECTION) {
            fs >> data.address;
        }
    }

    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const QKnxConnectInfo &data)
{
    if (data.infoType == QKnxConnectInfo::InfoNone) {
        qDebug() << "Warning: Connect infoType not set.";
        return fs;
    }

    // info length
    fs << data.structureLength();

    // connection type
    fs << (quint8)data.connectionType;

    if (data.infoType == QKnxConnectInfo::InfoRequest) {
        if (data.connectionType == QKnxNetIp::TUNNEL_CONNECTION) {
            // knx layers
            fs << (quint8)data.knxLayers;

            // reserved
            fs << data.reserved;
        }
    } else if (data.infoType == QKnxConnectInfo::InfoResponse) {
        if (data.connectionType == QKnxNetIp::TUNNEL_CONNECTION) {
            fs << data.address;
        }
    }

    return fs;
}
