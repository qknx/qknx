#ifndef QKNXCONNECTINFO_H
#define QKNXCONNECTINFO_H

#include "netip/qknxnetip.h"
#include "qknxaddress.h"

class QKNX_SHARED_EXPORT QKnxConnectInfo
{
    Q_GADGET
public:
    enum InfoType {
        InfoNone = 0x00,
        InfoRequest = 0x01,
        InfoResponse = 0x02
    };
    Q_ENUM(InfoType)

    explicit QKnxConnectInfo();
    virtual ~QKnxConnectInfo();

    InfoType infoType;
    QKnxNetIp::ConnectionType connectionType;
    QKnxNetIp::KnxLayers knxLayers;
    quint8 reserved;
    QKnxAddress address;

    quint8 structureLength() const;

private:
    friend QKnxStream& operator>>(QKnxStream& fs, QKnxConnectInfo& data);
    friend QKnxStream& operator<<(QKnxStream& fs, const QKnxConnectInfo& data);
};

QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QKnxConnectInfo& data);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const QKnxConnectInfo& data);

#endif // QKNXCONNECTINFO_H
