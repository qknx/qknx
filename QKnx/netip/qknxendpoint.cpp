#include "qknxendpoint.h"

#include <QAbstractSocket>
#include <QDataStream>
#include <QIPv6Address>
#include <QMutex>

QKnxEndpoint::QKnxEndpoint()
{
    m_HostProtocolCode = IPV4_UDP;
    m_Port = 0;
}

QKnxEndpoint::QKnxEndpoint(const QByteArray &hpai)
{
    m_HostProtocolCode = IPV4_UDP;
    m_Port = 0;
    setEndpoint(hpai);
}

QKnxEndpoint::~QKnxEndpoint()
{

}

bool QKnxEndpoint::operator==(QKnxEndpoint &other_endpoint) const
{
    if (m_HostProtocolCode != other_endpoint.m_HostProtocolCode) {
        return false;
    }
    if (m_HostAddress != other_endpoint.m_HostAddress) {
        return false;
    }
    if (m_Port != other_endpoint.m_Port) {
        return false;
    }
    return true;
}

bool QKnxEndpoint::setEndpoint(const QByteArray &hpai)
{
    QKnxStream fs(hpai);
    fs >> *this;
    return isValid();
}

QByteArray QKnxEndpoint::toByteArray() const
{
    QByteArray ret_value;
    QKnxStream fs(&ret_value,32);
    fs << *this;
    fs.finishWrite();
    return ret_value;
}

QHostAddress QKnxEndpoint::defaultMulticastIPv4() {
    static QMutex mutex;
    static QHostAddress default_multicast_addr4;
    mutex.lock();
    if (default_multicast_addr4.isNull()) {
        default_multicast_addr4 = QHostAddress(QKNX_UDP_DEFAULT_MULTICAST_IPV4);
    }
    QHostAddress addr = default_multicast_addr4;
    mutex.unlock();
    return addr;
}

QHostAddress QKnxEndpoint::defaultMulticastIPv6()
{
    static QMutex mutex;
    static QHostAddress default_multicast_addr6;
    mutex.lock();
    if (default_multicast_addr6.isNull()) {
        default_multicast_addr6 = QHostAddress(QKNX_UDP_DEFAULT_MULTICAST_IPV6);
    }
    QHostAddress addr = default_multicast_addr6;
    mutex.unlock();
    return addr;
}

bool QKnxEndpoint::isValid() const
{
    if (m_HostProtocolCode == NONE_HPC) {
        return false;
    }

    if (m_HostAddress.protocol() == QAbstractSocket::IPv4Protocol) {
        if (m_HostProtocolCode == IPV4_UDP) {
            return true;
        }
        if (m_HostProtocolCode == IPV4_TCP) {
            return true;
        }
    }

    if (m_HostAddress.protocol() == QAbstractSocket::IPv6Protocol) {
        if (m_HostProtocolCode == IPV6_UDP) {
                return true;
        }
        if (m_HostProtocolCode == IPV6_TCP) {
                return true;
        }
    }

    return false;
}


QKnxStream &operator>>(QKnxStream &fs, QKnxEndpoint &ep)
{
    // length
    quint8 len;
    fs >> len;

    // host protocol code
    quint8 hpc;
    fs >> hpc;

    ep.m_HostProtocolCode = (QKnxEndpoint::HostProtocolCode)hpc;

    if (ep.m_HostProtocolCode == QKnxEndpoint::IPV4_UDP || ep.m_HostProtocolCode == QKnxEndpoint::IPV4_TCP ) {
        quint32 addr_;
        fs >> addr_;
        ep.m_HostAddress.setAddress(addr_);
    } else if (ep.m_HostProtocolCode == QKnxEndpoint::IPV6_UDP || ep.m_HostProtocolCode == QKnxEndpoint::IPV6_TCP) {
        QIPv6Address addr_;
        for (int x = 0; x < 16; x++) {
            fs >> addr_[x];
        }
        ep.m_HostAddress.setAddress(addr_);
    } else {
        return fs;
    }

    // port
    fs >> ep.m_Port;

    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const QKnxEndpoint &ep)
{
    // remember start position for overwriting length
    int start_pos = fs.position();

    fs << (quint8)0x00; // length
    fs << (quint8)ep.m_HostProtocolCode;

    if (ep.m_HostAddress.protocol() == QAbstractSocket::IPv4Protocol) {
        fs << ep.m_HostAddress.toIPv4Address();
    } else if (ep.m_HostAddress.protocol() == QAbstractSocket::IPv6Protocol) {
        QIPv6Address addr_ = ep.m_HostAddress.toIPv6Address();
        for (int x = 0; x < 16; x++) {
            fs << addr_[x];
        }
    }

    fs << ep.m_Port;

    // add length
    quint8 len = fs.position()-start_pos;
    fs.setByte(start_pos,len);

    return fs;
}
