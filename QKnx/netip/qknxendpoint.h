#ifndef QKNXNETIPENDPOINT_H
#define QKNXNETIPENDPOINT_H

#include "qknxstream.h"

#include <QHostAddress>

class QKNX_SHARED_EXPORT QKnxEndpoint
{
    Q_GADGET
public:
    enum HostProtocolCode {
        NONE_HPC = 0x00,
        IPV4_UDP = 0x01,
        IPV4_TCP = 0x02,
        IPV6_UDP = 0x03, // TODO: Is KNX IPv6 compatible?
        IPV6_TCP = 0x04
    };
    Q_ENUM(HostProtocolCode)

    explicit QKnxEndpoint();
    explicit QKnxEndpoint(const QByteArray& hpai);
    virtual ~QKnxEndpoint();

    // comparison operators
    bool operator== (QKnxEndpoint &other_endpoint) const;
    bool operator!= (QKnxEndpoint &other_endpoint) const { return !(*this == other_endpoint); }

    // import/export
    bool setEndpoint(const QByteArray& hpai);
    static QKnxEndpoint fromByteArray(const QByteArray& hpai) { return QKnxEndpoint(hpai); }
    QByteArray toByteArray() const;

    // host protocol code
    HostProtocolCode hostProtocolCode() const { return m_HostProtocolCode; }
    QString hostProtocolCodeString() const { return QMetaEnum::fromType<HostProtocolCode>().valueToKey(m_HostProtocolCode); }
    static QString hostProtocolCodeString(const HostProtocolCode& hpc) { return QMetaEnum::fromType<HostProtocolCode>().valueToKey(hpc); }
    void setHostProtocolCode(const HostProtocolCode& hpc) { m_HostProtocolCode = hpc; }

    // host address
    QHostAddress hostAddress() const { return m_HostAddress; }
    void setHostAddress(const QHostAddress& address) { m_HostAddress = address; }
    void setHostAddress(const QString& address) { m_HostAddress = QHostAddress(address); }
    void setHostAddress(const char* address) { setHostAddress(QString(address)); }
    void setHostAddress(const quint32& address) { m_HostAddress.setAddress(address); }

    // port
    quint16 port() const { return m_Port; }
    void setPort(const quint16& port) { m_Port = port; }

    // default mcast address and port
    static QHostAddress defaultMulticastIPv4();
    static QHostAddress defaultMulticastIPv6();
    static quint16 defaultPort() { return QKNX_UDP_DEFAULT_PORT; }

    // validity check
    virtual bool isValid() const;

private:
    HostProtocolCode m_HostProtocolCode;
    QHostAddress m_HostAddress;
    quint16 m_Port;

    friend QKnxStream& operator>>(QKnxStream& fs, QKnxEndpoint& ep);
    friend QKnxStream& operator<<(QKnxStream& fs, const QKnxEndpoint& ep);
};

QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QKnxEndpoint& ep);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const QKnxEndpoint& ep);


#endif // QKNXNETIPENDPOINT_H
