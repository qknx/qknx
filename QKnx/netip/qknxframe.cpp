#include "qknxframe.h"

QKnxFrame::QKnxFrame() : QKnxNetIp(), headerLength(HeaderLengthDefault), protocolVersion(ProtocolVersionDefault), serviceType(0),
                                    structureLength(0), channelId(0), sequenceCounter(0), errorCode(0),
                                        discoveryEndpoint(controlEndpoint), remotePort(0), m_ReadOk(-1)
{

}

QKnxFrame::QKnxFrame(const QByteArray &data) : QKnxNetIp(), headerLength(HeaderLengthDefault), protocolVersion(ProtocolVersionDefault), serviceType(0),
                                                            structureLength(0), channelId(0), sequenceCounter(0), errorCode(0),
                                                                discoveryEndpoint(controlEndpoint), remotePort(0), m_ReadOk(-1)
{
    setFrameData(data);
}

QKnxFrame::~QKnxFrame()
{

}

void QKnxFrame::reset()
{

}

QKnxNetIp::Fields QKnxFrame::fieldsByServiceType(const QKnxNetIp::ServiceType &st)
{
    Fields f = FIELD_NONE;
    switch (st) {
    case SERVICE_NONE: break;
    case SEARCH_REQUEST: f = FIELD_DISCOVERY_ENDPOINT; break;
    case SEARCH_RESPONSE: f = FIELD_CONTROL_ENDPOINT|FIELD_DIB_DEVICE_INFO|FIELD_DIB_SUPP_SVC; break;
    case DESCRIPTION_REQUEST: f = FIELD_CONTROL_ENDPOINT; break;
    case DESCRIPTION_RESPONSE: f = FIELD_DIB_DEVICE_INFO|FIELD_DIB_SUPP_SVC; break;
    case CONNECT_REQUEST: f = FIELD_DISCOVERY_ENDPOINT|FIELD_DATA_ENDPOINT|FIELD_CRI; break;
    case CONNECT_RESPONSE: f= FIELD_CHANNEL_ID|FIELD_ERROR_CODE|FIELD_DATA_ENDPOINT|FIELD_CRDB; break;
    case CONNECTIONSTATE_REQUEST: f = FIELD_CHANNEL_ID|FIELD_ERROR_CODE|FIELD_CONTROL_ENDPOINT; break;
    case CONNECTIONSTATE_RESPONSE: f = FIELD_CHANNEL_ID|FIELD_ERROR_CODE; break;
    case DISCONNECT_REQUEST: f = FIELD_CHANNEL_ID|FIELD_ERROR_CODE|FIELD_CONTROL_ENDPOINT; break;
    case DISCONNECT_RESPONSE: f = FIELD_CHANNEL_ID|FIELD_ERROR_CODE; break;
    case DEVICE_CONFIGURATION_REQUEST: f = FIELD_STRUCTURE_LENGTH|FIELD_CHANNEL_ID|FIELD_SEQ_COUNTER|FIELD_ERROR_CODE|FIELD_CEMI; break;
    case DEVICE_CONFIGURATION_ACK: f = FIELD_STRUCTURE_LENGTH|FIELD_CHANNEL_ID|FIELD_SEQ_COUNTER|FIELD_ERROR_CODE; break;
    case TUNNELLING_REQUEST: f = FIELD_STRUCTURE_LENGTH|FIELD_CHANNEL_ID|FIELD_SEQ_COUNTER|FIELD_ERROR_CODE|FIELD_CEMI; break;
    case TUNNELLING_ACK:  f = FIELD_STRUCTURE_LENGTH|FIELD_CHANNEL_ID|FIELD_SEQ_COUNTER|FIELD_ERROR_CODE; break;
    case ROUTING_INDICATION: f = FIELD_CEMI; break;
    case ROUTING_LOST: break;
    case ROUTING_BUSY: break;
    case REMOTE_DIAG_REQUEST: break;
    case REMOTE_DIAG_RESPONSE: break;
    case REMOTE_BASIC_CONF_REQUEST: break;
    case REMOTE_RESPONSEET_REQUEST: break;
    default: break;
    }
    return f;
}

bool QKnxFrame::setFrameData(const QByteArray &data)
{
    QKnxStream fs(data);
    fs >> *this;
    return isValid();
}

QByteArray QKnxFrame::toByteArray() const
{
    QByteArray ret_value;
    QKnxStream fs(&ret_value,384);
    fs << *this;
    fs.finishWrite();
    return ret_value;
}

bool QKnxFrame::isValid() const
{
    if (m_ReadOk == 0) {
        return false;
    }
    // TODO: Add more validity checks

    return true;
}

// input
QKnxStream &operator>>(QKnxStream &fs, QKnxFrame &data)
{
    // read header
    quint16 total_len;
    fs >> data.headerLength >> data.protocolVersion >> data.serviceType >> total_len;
    if (total_len != fs.length()) {
        // size mismatch
        data.m_ReadOk = 0;
        return fs;
    }

    // get required fields
    QKnxFrame::Fields fields = data.fields();
    if (fields == QKnxFrame::FIELD_NONE) {
        // no defined fields
        data.m_ReadOk = 0;
        return fs;
    }

    // structure length
    if (fields & QKnxFrame::FIELD_STRUCTURE_LENGTH) {
        fs >> data.structureLength;
    }

    // channel id
    if (fields & QKnxFrame::FIELD_CHANNEL_ID) {
        fs >> data.channelId;
    }

    // sequence counter
    if (fields & QKnxFrame::FIELD_SEQ_COUNTER) {
        fs >> data.sequenceCounter;
    }

    // error code
    if (fields & QKnxFrame::FIELD_ERROR_CODE) {
        fs >> data.errorCode;
    }

    // control/discovery endpoint
    if (fields & QKnxFrame::FIELD_CONTROL_ENDPOINT || fields & QKnxFrame::FIELD_DISCOVERY_ENDPOINT) {
        fs >> data.controlEndpoint;
    }

    // data endpoint
    if (fields & QKnxFrame::FIELD_DATA_ENDPOINT) {
        fs >> data.dataEndpoint;
    }

    // dibs, cemi, cri, crdb
    bool read_success = true;
    while (read_success && !fs.atEnd() && !fs.error()) {
        read_success = false;

        quint8 len, dib_type;
        fs >> len;
        fs >> dib_type;
        fs.setPosition(fs.position()-2);

        // CRI/CRDB
        if (fields & QKnxFrame::FIELD_CRI || fields & QKnxFrame::FIELD_CRDB) {
            data.connectInfo.infoType = (QKnxFrame::FIELD_CRI) ? QKnxConnectInfo::InfoRequest : QKnxConnectInfo::InfoResponse;
            fs >> data.connectInfo;
            read_success = true;
        }

        // DIB: DeviceInfo
        if (fields & QKnxFrame::FIELD_DIB_DEVICE_INFO && dib_type == data.deviceInfo.dibType()) {
            fs >> data.deviceInfo;
            read_success = data.deviceInfo.isReadOk();
        }

        // DIB: SuppSvcs
        if (fields & QKnxFrame::FIELD_DIB_DEVICE_INFO && dib_type == data.suppSvcs.dibType()) {
            fs >> data.suppSvcs;
            read_success = data.suppSvcs.isReadOk();
        }

        // CEMI
        if (fields & QKnxFrame::FIELD_CEMI) {
            fs >> data.cEMI;
            read_success = data.cEMI.isReadOk();
        }
    }

    data.m_ReadOk = 1;
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const QKnxFrame &data)
{
    // write header
    int start_pos = fs.position();
    fs << data.headerLength << data.protocolVersion << data.serviceType;
    int len_pos = fs.position();
    fs << (quint16)0x0000;

    // get required fields
    QKnxFrame::Fields fields = data.fields();
    if (fields == QKnxFrame::FIELD_NONE) {
        // no defined fields
        return fs;
    }

    // structure length
    if (fields & QKnxFrame::FIELD_STRUCTURE_LENGTH) {
        fs << data.structureLength;
    }

    // channel id
    if (fields & QKnxFrame::FIELD_CHANNEL_ID) {
        fs << data.channelId;
    }

    // sequence counter
    if (fields & QKnxFrame::FIELD_SEQ_COUNTER) {
        fs << data.sequenceCounter;
    }

    // error code
    if (fields & QKnxFrame::FIELD_ERROR_CODE) {
        fs << data.errorCode;
    }

    // control/discovery endpoint
    if (fields & QKnxFrame::FIELD_CONTROL_ENDPOINT || fields & QKnxFrame::FIELD_DISCOVERY_ENDPOINT) {
        fs << data.controlEndpoint;
    }

    // data endpoint
    if (fields & QKnxFrame::FIELD_DATA_ENDPOINT) {
        fs << data.dataEndpoint;
    }

    // CRI/CRDB
    if (fields & QKnxFrame::FIELD_CRI || fields & QKnxFrame::FIELD_CRDB) {
        fs << data.connectInfo;
    }

    // DIB: DeviceInfo
    if (fields & QKnxFrame::FIELD_DIB_DEVICE_INFO) {
        fs << data.deviceInfo;
    }

    // DIB: SuppSvcs
    if (fields & QKnxFrame::FIELD_DIB_DEVICE_INFO) {
        fs << data.suppSvcs;
    }

    // CEMI
    if (fields & QKnxFrame::FIELD_CEMI) {
        fs << data.cEMI;
    }

    // insert frame length
    int end_pos = fs.position();
    quint16 len = end_pos-start_pos;
    fs.setPosition(len_pos);
    fs << len;
    fs.setPosition(end_pos);

    return fs;
}
