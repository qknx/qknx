#ifndef QKNXFRAME_H
#define QKNXFRAME_H

#include "qknxnetip.h"
#include "qknxstream.h"
#include "netip/qknxendpoint.h"

// cri/crdb
#include "netip/qknxconnectinfo.h"

// dibs
#include "netip/dib/qknxdeviceinfo.h"
#include "netip/dib/qknxsuppsvcs.h"

// cemi
#include "netip/qknxcemi.h"

class QKNX_SHARED_EXPORT QKnxFrame : public QKnxNetIp
{
    Q_GADGET
public:
    explicit QKnxFrame();
    explicit QKnxFrame(const QByteArray& data);
    virtual ~QKnxFrame();

    void reset();
    Fields fields() const { return fieldsByServiceType((ServiceType)serviceType); }
    static Fields fieldsByServiceType(const ServiceType& st);

    // import/export
    bool setFrameData(const QByteArray& data);
    static QKnxFrame fromByteArray(const QByteArray& data) { return QKnxFrame(data); }
    QByteArray toByteArray() const;

    bool isReadOkay() const { return (m_ReadOk == 1); }
    bool isValid() const;

    // header
    quint8 headerLength;
    quint8 protocolVersion;
    quint16 serviceType;

    // body
    quint8 structureLength;
    quint8 channelId;
    quint8 sequenceCounter;
    quint8 errorCode;

    // endpoints
    QKnxEndpoint controlEndpoint;
    QKnxEndpoint& discoveryEndpoint; // refs control endpoint
    QKnxEndpoint dataEndpoint;

    // cri/crdb
    QKnxConnectInfo connectInfo;

    // dibs
    QKnxDeviceInfo deviceInfo;
    QKnxSuppSvcs suppSvcs;

    // cemi
    QKnxCemi cEMI;

    // remote host info
    QHostAddress remoteHost;
    quint16 remotePort;

private:
    int m_ReadOk;

    friend QKnxStream& operator>>(QKnxStream& fs, QKnxFrame& data);
    friend QKnxStream& operator<<(QKnxStream& fs, const QKnxFrame& data);

};

QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QKnxFrame& data);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const QKnxFrame& data);


#endif // QKNXFRAME_H
