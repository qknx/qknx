#include "qknxmacaddress.h"

QKnxMacAddress::QKnxMacAddress()
{

}

QKnxMacAddress::QKnxMacAddress(const QByteArray &mac_address)
{
    setAddress(mac_address);
}

QKnxMacAddress::QKnxMacAddress(const QString &mac_address)
{
    setAddress(mac_address);
}

QKnxMacAddress::QKnxMacAddress(const char *mac_address)
{
    setAddress(QString::fromLatin1(mac_address));
}

QKnxMacAddress::~QKnxMacAddress()
{

}

bool QKnxMacAddress::operator==(QKnxMacAddress &other_mac) const
{
    return (m_MacAddress == other_mac.m_MacAddress);
}

bool QKnxMacAddress::setAddress(const QByteArray &mac_address)
{
    if (mac_address.size() != QKNX_MAC_ADDRESS_REQUIRED_LENGTH) {
        return false;
    }
    m_MacAddress = mac_address;
    return true;
}

bool QKnxMacAddress::setAddress(const QString &mac_address)
{
    QByteArray tmp = QByteArray::fromHex(mac_address.trimmed().remove(":").toLatin1());
    if (tmp.count() != QKNX_MAC_ADDRESS_REQUIRED_LENGTH) {
        return false;
    }
    m_MacAddress = tmp;
    return true;
}

QKnxMacAddress QKnxMacAddress::fromByteArray(QByteArray &mac_address, bool *ok)
{
    QKnxMacAddress mac;
    bool ret = mac.setAddress(mac_address);
    if (ok) *ok = ret;
    return mac;
}

QKnxMacAddress QKnxMacAddress::fromString(QString &mac_address, bool *ok)
{
    QKnxMacAddress mac;
    bool ret = mac.setAddress(mac_address);
    if (ok) *ok = ret;
    return mac;
}

QString QKnxMacAddress::toString() const
{
    if (!isValid()) {
        return QString();
    }
    char delim = ':';
    QByteArray tmp = m_MacAddress.toHex();
    tmp.insert(10,delim);
    tmp.insert(8,delim);
    tmp.insert(6,delim);
    tmp.insert(4,delim);
    tmp.insert(2,delim);
    return QString::fromLatin1(tmp);
}

QKnxStream &operator<<(QKnxStream &fs, const QKnxMacAddress &mac)
{
    fs << mac.m_MacAddress;
    return fs;
}

QKnxStream &operator>>(QKnxStream &fs, QKnxMacAddress &mac)
{
    if (mac.m_MacAddress.length() != QKNX_MAC_ADDRESS_REQUIRED_LENGTH) mac.m_MacAddress.resize(QKNX_MAC_ADDRESS_REQUIRED_LENGTH);
    fs >> mac.m_MacAddress;
    return fs;
}

