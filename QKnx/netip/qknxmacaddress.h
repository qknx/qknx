#ifndef QKNXNETIPMACADDRESS_H
#define QKNXNETIPMACADDRESS_H

#include "qknxstream.h"

#define QKNX_MAC_ADDRESS_REQUIRED_LENGTH 6

class QKNX_SHARED_EXPORT QKnxMacAddress
{
public:
    explicit QKnxMacAddress();
    explicit QKnxMacAddress(const QByteArray& mac_address);
    explicit QKnxMacAddress(const QString& mac_address);
    explicit QKnxMacAddress(const char* mac_address);
    virtual ~QKnxMacAddress();

    // comparison operators
    bool operator== (QKnxMacAddress &other_mac) const;
    bool operator!= (QKnxMacAddress &other_mac) const { return !(*this == other_mac); }

    // setter function
    bool setAddress(const QKnxMacAddress& mac_address) { return setAddress(mac_address.m_MacAddress); }
    bool setAddress(const QByteArray& mac_address);
    bool setAddress(const QString& mac_address);
    bool setAddress(const char* mac_address) { return setAddress(QString::fromLatin1(mac_address)); }

    // import functions
    static QKnxMacAddress fromByteArray(QByteArray& mac_address, bool* ok = Q_NULLPTR);
    static QKnxMacAddress fromString(QString& mac_address, bool* ok = Q_NULLPTR);

    // export functions
    QByteArray toByteArray() const { return m_MacAddress; }
    QString toString() const;

    // validity check
    static quint8 requiredLength() { return QKNX_MAC_ADDRESS_REQUIRED_LENGTH; }
    bool isValid() const { return (m_MacAddress.size() == QKNX_MAC_ADDRESS_REQUIRED_LENGTH); }

private:
    QByteArray m_MacAddress;

    // streams
    friend QKnxStream &operator<<(QKnxStream& fs, const QKnxMacAddress& mac);
    friend QKnxStream &operator>>(QKnxStream& fs, QKnxMacAddress& mac);
};

// stream operators
QKNX_SHARED_EXPORT QKnxStream &operator<<(QKnxStream& fs, const QKnxMacAddress& mac);
QKNX_SHARED_EXPORT QKnxStream &operator>>(QKnxStream& fs, QKnxMacAddress& mac);

#endif // QKNXNETIPMACADDRESS_H
