#include "qknxnetip.h"

#include <QMutex>
#include <QMutexLocker>

// error descriptions
static QKnxNetIp::ErrorDescriptionMap error_descriptions;
static QMutex error_descriptions_mutex;

// suppsvc descriptions
static QKnxNetIp::SuppSvcDescriptionsMap suppsvc_descriptions;
static QMutex suppsvc_descriptions_mutex;

QKnxNetIp::ErrorDescriptionMap QKnxNetIp::allErrorDescriptions()
{
    QMutexLocker lock(&error_descriptions_mutex);
    ErrorDescriptionMap& d = error_descriptions;
    if (d.isEmpty()) {
        d.insert(E_NO_ERROR, "No error occured");
        d.insert(E_CONNECTION_ID, "The KNXnet/IP server device could not find an active data connection with the specified ID");
        d.insert(E_CONNECTION_TYPE, "The KNXnet/IP server device does not support the requested connection type");
        d.insert(E_CONNECTION_OPTION, "The KNXnet/IP server device does not support one or more requested connection options");
        d.insert(E_NO_MORE_CONNECTIONS, "The KNXnet/IP server device could not accept the new data connection (busy)");
        d.insert(E_DATA_CONNECTION, "The KNXnet/IP server device detected an error concerning the data connection with the specified ID");
        d.insert(E_KNX_CONNECTION, "The KNXnet/IP server device detected an error concerning the EIB bus / KNX subsystem connection with the specified ID");
        d.insert(E_TUNNELLING_LAYER, "The KNXnet/IP server device does not support the requested tunnelling layer");
    }
    return d;
}

QKnxNetIp::SuppSvcDescriptionsMap QKnxNetIp::allSuppSvcDescriptions()
{
    QMutexLocker lock(&suppsvc_descriptions_mutex);
    SuppSvcDescriptionsMap& d = suppsvc_descriptions;
    if (d.isEmpty()) {
        d.insert(SVC_CORE,"KNXnet/IP Core");
        d.insert(SVC_DEVICE_MANAGEMENT,"KNXnet/IP Device Management");
        d.insert(SVC_TUNNELING,"KNXnet/IP Tunneling");
        d.insert(SVC_ROUTING,"KNXnet/IP Routing");
        d.insert(SVC_REMOTE_LOGGING,"KNXnet/IP Remote Logging");
        d.insert(SVC_REMOTE_CONFIG,"KNXnet/IP Remote Configuration and Diagnosis");
        d.insert(SVC_OBJECT_SERVER,"KNXnet/IP Object Server");
    }
    return d;
}

