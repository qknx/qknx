#ifndef QKNXNETIP_H
#define QKNXNETIP_H

#include "qknxstream.h"
#include "qknxaddress.h"

#include <QHostAddress>

class QKNX_SHARED_EXPORT QKnxNetIp
{
    Q_GADGET
public:
    enum HeaderLength {
        HeaderLengthNone = 0x00,
        HeaderLengthDefault = 0x06
    };
    Q_ENUM(HeaderLength)

    enum ProtocolVersion {
        ProtocolVersionNone = 0x00,
        ProtocolVersionDefault = 0x10,
        ProtocolVersionSecure = 0x13
    };
    Q_ENUM(ProtocolVersion)

    enum ServiceType {
        SERVICE_NONE = 0x0000,
        SEARCH_REQUEST = 0x0201,
        SEARCH_RESPONSE = 0x0202,
        DESCRIPTION_REQUEST = 0x0203,
        DESCRIPTION_RESPONSE = 0x0204,
        CONNECT_REQUEST = 0x0205,
        CONNECT_RESPONSE = 0x0206,
        CONNECTIONSTATE_REQUEST = 0x0207,
        CONNECTIONSTATE_RESPONSE = 0x0208,
        DISCONNECT_REQUEST = 0x0209,
        DISCONNECT_RESPONSE = 0x020A,
        DEVICE_CONFIGURATION_REQUEST = 0x0310,
        DEVICE_CONFIGURATION_ACK = 0x0311,
        TUNNELLING_REQUEST = 0x0420,
        TUNNELLING_ACK = 0x0421,
        ROUTING_INDICATION = 0x0530,
        ROUTING_LOST = 0x0531,
        ROUTING_BUSY = 0x0532,
        REMOTE_DIAG_REQUEST = 0x0740,
        REMOTE_DIAG_RESPONSE = 0x0741,
        REMOTE_BASIC_CONF_REQUEST = 0x0742,
        REMOTE_RESPONSEET_REQUEST = 0x0743
    };
    Q_ENUM(ServiceType)

    enum ConnectionType {
        CONNECTION_NONE = 0x00,
        DEVICE_MGMT_CONNECTION = 0x03,
        TUNNEL_CONNECTION = 0x04,
        REMLOG_CONNECTION = 0x06,
        REMCONF_CONNECTION = 0x07,
        OBJSVR_CONNECTION = 0x08
    };
    Q_ENUM(ConnectionType)

    enum KnxLayer {
        TUNNEL_NONE = 0x00,
        TUNNEL_LINKLAYER = 0x02,
        TUNNEL_RAW = 0x04,
        TUNNEL_BUSMONITOR = 0x80
    };
    Q_DECLARE_FLAGS(KnxLayers,KnxLayer)
    Q_FLAG(KnxLayers)

    enum ErrorCode {
        E_NO_ERROR = 0x00,
        E_CONNECTION_ID = 0x21,
        E_CONNECTION_TYPE = 0x22,
        E_CONNECTION_OPTION = 0x23,
        E_NO_MORE_CONNECTIONS = 0x24,
        E_DATA_CONNECTION = 0x26,
        E_KNX_CONNECTION = 0x27,
        E_TUNNELLING_LAYER = 0x29
    };
    Q_ENUM(ErrorCode)
    typedef QMap<ErrorCode, QString> ErrorDescriptionMap;

    // device information block
    enum DibType {
        DIB_TYPE_NONE = 0x00,
        DIB_DEVICE_INFO = 0x01,
        DIB_SUPP_SVC = 0x02,
        DIB_IP_CONF = 0x03,
        DIB_IP_CURRENT = 0x04,
        DIB_KNX_ADDRESS = 0x05,
        DIB_MFR_DATA = 0xFE
    };
    Q_ENUM(DibType)

    // medium
    enum Medium {
        KNX_NONE = 0x00,
        KNX_TP0 = 0x01,
        KNX_TP1 = 0x02,
        KNX_PL110 = 0x04,
        KNX_PL132 = 0x08,
        KNX_RF = 0x10,
        KNX_IP = 0x20
    };
    Q_DECLARE_FLAGS(Mediums,Medium)
    Q_FLAG(Mediums)

    // device status
    enum Status {
        STATUS_NONE = 0x00,
        STATUS_PROGMODE = 0x01
    };
    Q_DECLARE_FLAGS(Statuses,Status)
    Q_FLAG(Statuses)

    // supp svcs
    enum SuppSvc {
        SVC_NONE = 0x00,
        SVC_RESERVED1 = 0x01,
        SVC_CORE = 0x02,
        SVC_DEVICE_MANAGEMENT = 0x03,
        SVC_TUNNELING = 0x04,
        SVC_ROUTING = 0x05,
        SVC_REMOTE_LOGGING = 0x06,
        SVC_REMOTE_CONFIG = 0x07,
        SVC_OBJECT_SERVER = 0x08
    };
    Q_ENUM(SuppSvc)
    // Service maps typedefs
    typedef QMap<SuppSvc, quint8> SuppSvcMap;
    typedef QMap<SuppSvc, QString> SuppSvcDescriptionsMap;

    // possible fields in knx frame
    enum Field {
        FIELD_NONE = 0,
        FIELD_STRUCTURE_LENGTH = 1 << 0,
        FIELD_CHANNEL_ID = 1 << 1,
        FIELD_SEQ_COUNTER = 1 << 2,
        FIELD_ERROR_CODE = 1 << 3,
        FIELD_CONTROL_ENDPOINT = 1 << 4,
        FIELD_DISCOVERY_ENDPOINT = 1 << 5,
        FIELD_DATA_ENDPOINT = 1 << 6,
        FIELD_DIB_DEVICE_INFO = 1 << 7,
        FIELD_DIB_SUPP_SVC = 1 << 8,
        FIELD_DIB_IP_CONF = 1 << 9,
        FIELD_DIB_IP_CURRENT = 1 << 10,
        FIELD_DIB_KNX_ADDRESS = 1 << 11,
        FIELD_DIB_MFR_DATA = 1 << 12,
        FIELD_CRI = 1 << 13,
        FIELD_CRDB = 1 << 14,
        FIELD_CEMI = 1 << 15
    };
    Q_DECLARE_FLAGS(Fields,Field)
    Q_FLAG(Fields)

    explicit QKnxNetIp() {}
    virtual ~QKnxNetIp() {}

    static ErrorDescriptionMap allErrorDescriptions();
    static QString errorCodeDescription(const ErrorCode& error_code) { return allErrorDescriptions().value(error_code); }
    static QString errorCodeString(const ErrorCode& error_code) { return QMetaEnum::fromType<ErrorCode>().valueToKey(error_code); }

    static SuppSvcDescriptionsMap allSuppSvcDescriptions();
    static QString suppSvcDescription(const SuppSvc& supp_svc) { return allSuppSvcDescriptions().value(supp_svc); }
    static QString suppSvcString(const SuppSvc& supp_svc) { return QMetaEnum::fromType<SuppSvc>().valueToKey(supp_svc); }
};
Q_DECLARE_OPERATORS_FOR_FLAGS(QKnxNetIp::KnxLayers)
Q_DECLARE_OPERATORS_FOR_FLAGS(QKnxNetIp::Mediums)
Q_DECLARE_OPERATORS_FOR_FLAGS(QKnxNetIp::Statuses)
Q_DECLARE_OPERATORS_FOR_FLAGS(QKnxNetIp::Fields)

#endif
