#include "qknxnetwork.h"

#include "qknxnetworkworker.h"

#include <QThread>
#include <QMutexLocker>

// init statics
static QMutex network_worker_mutex;
static QKnxNetworkWorker* network_worker = Q_NULLPTR;
static QThread* network_thread = Q_NULLPTR;
static int network_instance_count = 0;
static int dummyQNetworkInterface = qRegisterMetaType<QNetworkInterface>();

QKnxNetwork::QKnxNetwork(QObject *parent) : QObject(parent)
{
    QMutexLocker lock(&mutex());
    network_instance_count++;

    // worker control connections
    connect(this, SIGNAL(stopped()), worker(), SLOT(stop()));

    // worker data connections
    //connect(m_Worker, SIGNAL(error(QString)), this, SLOT(errorString(QString)));
    connect(worker(), SIGNAL(interfaceAdded(QNetworkInterface)), this, SIGNAL(interfaceAdded(QNetworkInterface)));
    connect(worker(), SIGNAL(interfaceChanged(QNetworkInterface)), this, SIGNAL(interfaceChanged(QNetworkInterface)));
    connect(worker(), SIGNAL(interfaceRemoved(QNetworkInterface)), this, SIGNAL(interfaceRemoved(QNetworkInterface)));
    connect(worker(), SIGNAL(primaryInterfaceChanged(QNetworkInterface)), this, SIGNAL(primaryInterfaceChanged(QNetworkInterface)));
    connect(worker(), SIGNAL(interfacesModified()), this, SIGNAL(interfacesModified()));

    if (network_instance_count == 1) {
        worker()->moveToThread(workerThread());
        connect(workerThread(), SIGNAL(started()), worker(), SLOT(start()));
        connect(worker(), SIGNAL(finished()), workerThread(), SLOT(quit()));
        connect(worker(), SIGNAL(finished()), worker(), SLOT(deleteLater()));
        connect(workerThread(), SIGNAL(finished()), workerThread(), SLOT(deleteLater()));
        workerThread()->start();
    }
}

QKnxNetwork::~QKnxNetwork()
{
    QMutexLocker lock(&mutex());
    network_instance_count--;
    if (!network_instance_count) {
        emit(stopped());
        worker(true);
        workerThread(true);
    }
}

QList<QNetworkInterface> QKnxNetwork::interfaces()
{
    QMutexLocker lock(&mutex());
    return worker()->interfaces();
}

QMutex &QKnxNetwork::mutex()
{
    static QMutex mutex_;
    return mutex_;
}

QKnxNetworkWorker *QKnxNetwork::worker(bool set_null)
{
    if (!set_null && network_worker) {
        return network_worker;
    }

    QMutexLocker lock(&network_worker_mutex);
    if (set_null) {
        network_worker = Q_NULLPTR;
    } else {
        network_worker = new QKnxNetworkWorker();
    }
    return network_worker;
}

QThread *QKnxNetwork::workerThread(bool set_null)
{
    if (!set_null && network_thread) {
        return network_thread;
    }

    QMutexLocker lock(&network_worker_mutex);
    if (set_null) {
        network_thread = Q_NULLPTR;
    } else {
        network_thread = new QThread();
    }
    return network_thread;
}
