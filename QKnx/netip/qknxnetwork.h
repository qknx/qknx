#ifndef QKNXNETWORK_H
#define QKNXNETWORK_H

#include "qknxglobal.h"

#include <QMutex>
#include <QNetworkInterface>

class QKnxNetworkWorker;
class QThread;

class QKNX_SHARED_EXPORT QKnxNetwork : public QObject
{
    Q_OBJECT
public:
    explicit QKnxNetwork(QObject* parent = Q_NULLPTR);
    virtual ~QKnxNetwork();

    QList<QNetworkInterface> interfaces();

signals:
    void stopped();

    void interfaceAdded(const QNetworkInterface& iface);
    void interfaceChanged(const QNetworkInterface& iface);
    void interfaceRemoved(const QNetworkInterface& iface);
    void primaryInterfaceChanged(const QNetworkInterface& iface);
    void interfacesModified();

public slots:

private:
    static QMutex& mutex();
    static QKnxNetworkWorker* worker(bool set_null=false);
    static QThread* workerThread(bool set_null=false);

};

#endif // QKNXNETWORK_H
