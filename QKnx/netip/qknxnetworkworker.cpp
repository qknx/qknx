#include "qknxnetworkworker.h"

#include <QNetworkConfigurationManager>
#include <QElapsedTimer>

QKnxNetworkWorker::QKnxNetworkWorker()
{
    m_Initialized = false;

    m_IfaceCache.setMaxCost(100);

    m_Timer = new QTimer(this);
    m_Timer->setSingleShot(false);
    m_Timer->setInterval(QKNX_NETWORK_TIMER*1000);
    connect(m_Timer,SIGNAL(timeout()),this,SLOT(updateInterfaces()));

}

QKnxNetworkWorker::~QKnxNetworkWorker()
{
    //qDebug() << "destroying worker";
}

bool QKnxNetworkWorker::isInitialized()
{
    m_InitializedMutex.lock();
    bool ret = m_Initialized;
    m_InitializedMutex.unlock();
    return ret;
}

QList<QNetworkInterface> QKnxNetworkWorker::interfaces()
{
    if (!isInitialized()) {
        updateInterfaces();
    }
    QMutexLocker lock(&m_DataMutex);
    return m_IfaceList;
}

void QKnxNetworkWorker::start()
{
    if (!isInitialized()) {
        updateInterfaces();
    }
    m_Timer->start();
}

void QKnxNetworkWorker::stop()
{
    m_Timer->stop();
    emit(finished());
}

void QKnxNetworkWorker::updateInterfaces()
{
    bool ifaces_modified = false;
    QList<QNetworkInterface> all_interfaces(QNetworkInterface::allInterfaces());

    QMutexLocker lock(&m_DataMutex);
    QList<int> removed_indexes = m_IfaceCache.keys();
    m_IfaceList.clear();
    foreach(QNetworkInterface iface, all_interfaces)
    {
        if (!iface.isValid()) continue;
        QNetworkInterface::InterfaceFlags flags = iface.flags();
        if (flags.testFlag(QNetworkInterface::IsLoopBack)) continue;
        //if (!flags.testFlag(QNetworkInterface::CanBroadcast) || !flags.testFlag(QNetworkInterface::CanMulticast)) continue;
        if (!flags.testFlag(QNetworkInterface::IsUp) || !flags.testFlag(QNetworkInterface::IsRunning)) continue;

        m_IfaceList.append(iface);

        // test if iface is new
        int index = iface.index();
        QNetworkInterface* old_iface = m_IfaceCache.object(index);
        if (!old_iface)
        {
            m_IfaceCache.insert(index,new QNetworkInterface(iface));
            ifaces_modified = true;
            emit(interfaceAdded(iface));
            continue;
        }

        // test if iface changed
        bool changed = false;
        removed_indexes.removeOne(index);
        if (iface.hardwareAddress() != old_iface->hardwareAddress()) changed = true;
        if (iface.addressEntries() != old_iface->addressEntries()) changed = true;
        if (flags != old_iface->flags()) changed = true;
        if (changed)
        {
            m_IfaceCache.insert(index,new QNetworkInterface(iface));
            ifaces_modified = true;
            emit(interfaceChanged(iface));
            continue;
        }
    }

    // check for removed interfaces
    if (removed_indexes.count() > 0) {
        foreach (int i, removed_indexes) {
            QNetworkInterface* old_iface = m_IfaceCache.object(i);
            if (old_iface)
            {
                ifaces_modified = true;
                emit(interfaceRemoved(*old_iface));
            }
            m_IfaceCache.remove(i);
        }

    }

    // signal modified ifaces
    if (ifaces_modified) {
        // determine primary interface
        QUdpSocket s;
        s.connectToHost(QKNX_UDP_IFACE_TEST_DNS_HOST,QKNX_UDP_IFACE_TEST_DNS_PORT,QIODevice::ReadWrite,QAbstractSocket::IPv4Protocol);
        QHostAddress tmp_host = s.localAddress();
        s.close();

        int smallest_index = -1;
        bool success = false;
        QNetworkInterface found_iface;
        QList<int> indexes = m_IfaceCache.keys();
        foreach (int i, indexes)
        {
            QNetworkInterface* iface = m_IfaceCache.object(i);
            if (!iface) continue;

            foreach (QNetworkAddressEntry e, iface->addressEntries()) {
                if (e.ip() == tmp_host) {
                    found_iface = *iface;
                    success = true;
                    break;
                }
                if (success) break;
            }
            if (success) break;

            if (smallest_index == -1 || i < smallest_index) {
                smallest_index = i;
            }
        }

        if (!success) {
            found_iface = QNetworkInterface::interfaceFromIndex(smallest_index);
        }

        // test if primary interface was modified
        bool changed = false;
        if (found_iface.index() != m_PrimaryInterface.index()) changed = true;
        if (found_iface.hardwareAddress() != m_PrimaryInterface.hardwareAddress()) changed = true;
        if (found_iface.addressEntries() != m_PrimaryInterface.addressEntries()) changed = true;
        if (found_iface.flags() != m_PrimaryInterface.flags()) changed = true;
        if (changed)
        {
            m_PrimaryInterface = found_iface;
            emit(primaryInterfaceChanged(m_PrimaryInterface));
        }

        // signal that interfaces have been changed
        emit(interfacesModified());
    }

    if (!isInitialized()) {
        m_InitializedMutex.lock();
        m_Initialized = true;
        m_InitializedMutex.unlock();
    }
}

