#ifndef QKNXNETWORKWORKER_H
#define QKNXNETWORKWORKER_H

#include "qknxglobal.h"

#include <QCache>
#include <QNetworkInterface>
#include <QTimer>
#include <QUdpSocket>
#include <QMutex>

#define QKNX_NETWORK_TIMER 10

class QKNX_SHARED_EXPORT QKnxNetworkWorker : public QObject
{
    Q_OBJECT
public:
    explicit QKnxNetworkWorker();
    virtual ~QKnxNetworkWorker();

    bool isInitialized();
    QList<QNetworkInterface> interfaces();

signals:
    void finished();

    void interfaceAdded(const QNetworkInterface& iface);
    void interfaceChanged(const QNetworkInterface& iface);
    void interfaceRemoved(const QNetworkInterface& iface);
    void primaryInterfaceChanged(const QNetworkInterface& iface);
    void interfacesModified();

public slots:
    void start();
    void stop();

private:
    bool m_Initialized;
    QMutex m_InitializedMutex;

    QMutex m_DataMutex;
    QCache<int, QNetworkInterface> m_IfaceCache;
    QList<QNetworkInterface> m_IfaceList;
    QNetworkInterface m_PrimaryInterface;
    QTimer* m_Timer;

private slots:
    void updateInterfaces();

};

#endif // QKNXNETWORKWORKER_H
