#include "qknxserver.h"

#include <QNetworkInterface>
#include <QElapsedTimer>
#include <QThread>

#include <climits>

QKnxServer::QKnxServer(QObject *parent) : QObject(parent)
{
    QKNX_REGISTER_TYPE(QKnxServer*);
    QKNX_REGISTER_TYPE(QKnxTelegram);

    m_Started = false;
    m_TelegramCount = 0;

    // port
    m_Port = QKnxEndpoint::defaultPort();

    // setup protocol
    m_Protocol = QAbstractSocket::IPv4Protocol;
    m_MulticastAddressIPv4 = QKnxEndpoint::defaultMulticastIPv4();
    m_MulticastAddressIPv6 = QKnxEndpoint::defaultMulticastIPv6();

    // cache sizes
    m_LocalAddressCache.setMaxCost(100);
    m_LocalMacAddressCache.setMaxCost(100);
    m_SearchResponseCache.setMaxCost(100);
    m_DescriptionResponseCache.setMaxCost(100);

    // device info
    m_DeviceInfo.setAddress("0.0.1");
    m_DeviceInfo.setFriendlyName(QKnxGlobal::libName());
    m_DeviceInfo.setMedium(QKnxNetIp::KNX_IP,true);
    m_DeviceInfo.setProjectInstallation(0x0000);
    m_DeviceInfo.setSerial("000000001337");

    // setup tunneling
    m_TunnelClientTimeout = QKNX_UDP_TUNNELING_TIMEOUT * 1000;
    m_TunnelNextId = 0;
    m_TunnelClients.setMaxCost(256);
    m_TunnelPoolSize = 0;
    m_TunnelNextAddress = 0;
    m_TunnelAddressPool.setMaxCost(256);

    m_TunnelAckTimer.setInterval(100);
    m_TunnelAckTimer.setSingleShot(false);
    m_TunnelAckTimer.start();
    connect(&m_TunnelAckTimer,SIGNAL(timeout()),this,SLOT(tunnelAckCheck()));

    // update services
    updateSuppSvcs();

    // connect qkxnnetwork for cache resets if interfaces change
    connect(&m_Network, SIGNAL(interfacesModified()), this, SLOT(resetCache()));

    // connect udp signals
    connect(&m_Udp4,SIGNAL(framePending()),this,SLOT(processPendingFrames()));
    connect(&m_Udp6,SIGNAL(framePending()),this,SLOT(processPendingFrames()));
}

QKnxServer::~QKnxServer()
{

}

void QKnxServer::setDeviceInfo(const QKnxDeviceInfo &dev_info)
{
    if (m_Started) {
        return;
    }
    m_DeviceInfo = dev_info;

    quint16 base_address = m_DeviceInfo.address().address();
    quint32 limit_test = (quint32)base_address + m_TunnelPoolSize;
    if (limit_test > std::numeric_limits<quint16>::max()) {
        qDebug() << "Warning: pool size exceeding address limit!";
        m_TunnelPoolSize = 0;
        return;
    }
    if (m_TunnelPoolSize > 0) {
        m_TunnelNextAddress = base_address + 1;
    } else {
        m_TunnelNextAddress = base_address;
    }
}

void QKnxServer::setTunnelPoolSize(const quint8 &pool_size)
{
    if (m_Started) {
        return;
    }
    quint16 base_address = m_DeviceInfo.address().address();
    quint32 limit_test = (quint32)base_address + pool_size;
    if (limit_test > std::numeric_limits<quint16>::max()) {
        qDebug() << "Warning: pool size exceeding address limit!";
        m_TunnelPoolSize = 0;
        return;
    }
    m_TunnelPoolSize = pool_size;
    if (m_TunnelPoolSize > 0) {
        m_TunnelNextAddress = base_address + 1;
    } else {
        m_TunnelNextAddress = base_address;
    }
}

void QKnxServer::serverStart()
{
    if (m_Started) {
        // server already running
        return;
    }
    m_Started = true;

    if (m_Modes == ModeNone) {
        qDebug() << "Warning: No mode set to start server with.";
        return;
    }

    if (m_Protocol == QAbstractSocket::AnyIPProtocol || m_Protocol == QAbstractSocket::IPv4Protocol) {
        m_Udp4.listen(QHostAddress::AnyIPv4,QKnxEndpoint::defaultPort(),QKnxEndpoint::defaultMulticastIPv4());
    }

    if (m_Protocol == QAbstractSocket::AnyIPProtocol || m_Protocol == QAbstractSocket::IPv6Protocol) {
        m_Udp6.listen(QHostAddress::AnyIPv6,QKnxEndpoint::defaultPort(),QKnxEndpoint::defaultMulticastIPv6());
    }

    emit(serverStarted());
}

void QKnxServer::serverStop()
{
    if (!m_Started) {
        // server not running
        return;
    }
    m_Started = false;

    m_Udp4.close();
    m_Udp6.close();

    emit(serverStopped());
}

void QKnxServer::writeTelegram(const QKnxTelegram &telegram)
{
    QKnxFrame frame;
    frame.serviceType = QKnxNetIp::ROUTING_INDICATION;
    frame.cEMI.messageCode = QKnxCemi::L_DATA_IND;
    frame.cEMI.telegram = telegram;
    frame.cEMI.telegram.setCemiIO(true);

    writeMulticast(frame);

    frame.serviceType = QKnxNetIp::TUNNELLING_REQUEST;
    frame.structureLength = 4;

    writeTunnel(frame,-1);
}

QHostAddress *QKnxServer::cachedLocalAddress(const QHostAddress &remote_address)
{
    QHostAddress* local_address = m_LocalAddressCache.object(remote_address);
    if (local_address) {
        return local_address;
    }

    // connect external dns to find out local iface
    m_SocketCacheLookup.connectToHost(QKNX_UDP_IFACE_TEST_DNS_HOST,QKNX_UDP_IFACE_TEST_DNS_PORT,QAbstractSocket::ReadWrite,QAbstractSocket::IPv4Protocol);
    local_address = new QHostAddress(m_SocketCacheLookup.localAddress());
    m_SocketCacheLookup.close();
    m_LocalAddressCache.insert(remote_address,local_address);

    return local_address;
}

QKnxMacAddress *QKnxServer::cachedLocalMacAddress(const QHostAddress &local_address)
{
    QKnxMacAddress* mac_result = m_LocalMacAddressCache.object(local_address);
    if (mac_result) {
        return mac_result;
    }

    // dont have mac in cache, search for it
    foreach (QNetworkInterface iface, m_Network.interfaces())
    {
        foreach (QNetworkAddressEntry e, iface.addressEntries()) {
            QHostAddress tmp_addr(e.ip());
            // remember all macs at once
            if (!m_LocalMacAddressCache.contains(tmp_addr)) {
                m_LocalMacAddressCache.insert(tmp_addr,new QKnxMacAddress(iface.hardwareAddress()));
                qDebug() << iface.humanReadableName() << tmp_addr;
            }
            // grab my result
            if (tmp_addr == local_address) {
                mac_result = new QKnxMacAddress(iface.hardwareAddress());
            }
        }
    }

    // ensure not returning null ptr, but invalid mac
    if (!mac_result) {
        mac_result = new QKnxMacAddress();
    }

    // remember mac address
    m_LocalMacAddressCache.insert(local_address,mac_result);

    return mac_result;
}

QKnxFrame *QKnxServer::cachedSearchResponse(const QHostAddress &local_address)
{
    QKnxFrame* response = m_SearchResponseCache.object(local_address);
    if (response) {
        return response;
    }
    createCachedResponse(local_address);
    return m_SearchResponseCache.object(local_address);
}

QKnxFrame *QKnxServer::cachedDescriptionResponse(const QHostAddress &local_address)
{
    QKnxFrame* response = m_DescriptionResponseCache.object(local_address);
    if (response) {
        return response;
    }
    createCachedResponse(local_address);
    return m_DescriptionResponseCache.object(local_address);
}

void QKnxServer::createCachedResponse(const QHostAddress &local_address)
{
    // create response frame data
    QKnxFrame* frame = new QKnxFrame;
    QKnxFrame& fd = *frame;

    // create description response first
    fd.serviceType = QKnxNetIp::DESCRIPTION_RESPONSE;

    // device info
    QAbstractSocket::NetworkLayerProtocol proto = local_address.protocol();
    fd.deviceInfo = m_DeviceInfo;
    fd.deviceInfo.setMacAddress(*cachedLocalMacAddress(local_address));
    if (proto == QAbstractSocket::IPv4Protocol) {
        fd.deviceInfo.setMulticastAddress(m_MulticastAddressIPv4);
    } else if (proto == QAbstractSocket::IPv6Protocol) {
        fd.deviceInfo.setMulticastAddress(m_MulticastAddressIPv6);
    }

    // supported services - description
    fd.suppSvcs = m_SuppSvcsDescription;

    // save description response
    m_DescriptionResponseCache.insert(local_address,new QKnxFrame(fd));

    // create search request - this only requires setting type and adding endpoint
    fd.serviceType = QKnxNetIp::SEARCH_RESPONSE;

    // supported services - search
    fd.suppSvcs = m_SuppSvcsSearch;

    // local endpoint
    if (proto == QAbstractSocket::IPv4Protocol) {
        fd.controlEndpoint.setHostProtocolCode(QKnxEndpoint::IPV4_UDP);
    } else if (proto == QAbstractSocket::IPv6Protocol) {
        fd.controlEndpoint.setHostProtocolCode(QKnxEndpoint::IPV6_UDP);
    } else {
        fd.controlEndpoint.setHostProtocolCode(QKnxEndpoint::NONE_HPC);
    }
    fd.controlEndpoint.setHostAddress(local_address);
    fd.controlEndpoint.setPort(m_Port);

    // save search response
    m_SearchResponseCache.insert(local_address, frame);
}

void QKnxServer::updateSuppSvcs()
{
    // supported services - search response
    m_SuppSvcsSearch.suppSvcsClear();
    if (m_Modes & ModeServer) {
        m_SuppSvcsSearch.suppSvcEnable(QKnxNetIp::SVC_CORE, 1);
    }
    if (m_Modes & ModeRouting) {
        m_SuppSvcsSearch.suppSvcEnable(QKnxNetIp::SVC_ROUTING, 1);
    }
    if (m_Modes & ModeTunneling) {
        m_SuppSvcsSearch.suppSvcEnable(QKnxNetIp::SVC_TUNNELING, 1);
    }

    // supported services - description response
    m_SuppSvcsDescription.setSuppSvcs(m_SuppSvcsSearch.suppSvcs());
    if (m_Modes & ModeServer) {
        //m_SuppSvcsDescription.suppSvcEnable(QKnxNetIp::SVC_DEVICE_MANAGEMENT, 1);
    }
}

void QKnxServer::handleSearchRequest(QKnxFrame &frame, QKnxUdp *udp)
{
    qDebug() << "";

    // create response
    QHostAddress* local_address = cachedLocalAddress(frame.discoveryEndpoint.hostAddress());
    QKnxFrame* resp = cachedSearchResponse(*local_address);

    // get copy of cached response and add target host/port
    QKnxFrame* resp_copy = new QKnxFrame(*resp);
    resp_copy->remoteHost = frame.discoveryEndpoint.hostAddress();
    resp_copy->remotePort = frame.discoveryEndpoint.port();

    // write response
    udp->writeFrame(resp_copy);
}

void QKnxServer::handleSearchResponse(QKnxFrame &frame, QKnxUdp *udp)
{
    Q_UNUSED(frame); Q_UNUSED(udp);
}

void QKnxServer::handleDescriptionRequest(QKnxFrame &frame, QKnxUdp *udp)
{
    // create response
    QHostAddress host_ep(frame.controlEndpoint.hostAddress());
    bool use_endpoint_address = false;
    QHostAddress* local_address = Q_NULLPTR;
    if (!host_ep.isNull() && host_ep != QHostAddress::AnyIPv4 && host_ep != QHostAddress::AnyIPv6) {
        use_endpoint_address = true;
        local_address = cachedLocalAddress(host_ep);
    } else {
        local_address = cachedLocalAddress(frame.remoteHost);
    }

    QKnxFrame* resp = cachedDescriptionResponse(*local_address);
    QKnxFrame* resp_copy = new QKnxFrame(*resp);

    if (use_endpoint_address) {
        resp_copy->remoteHost = frame.controlEndpoint.hostAddress();
        resp_copy->remotePort = frame.controlEndpoint.port();
    } else {
        resp_copy->remoteHost = frame.remoteHost;
        resp_copy->remotePort = frame.remotePort;
    }

    // write response
    udp->writeFrame(resp_copy);
}

void QKnxServer::handleDescriptionResponse(QKnxFrame &frame, QKnxUdp *udp)
{
    Q_UNUSED(frame); Q_UNUSED(udp);
}

void QKnxServer::handleConnectRequest(QKnxFrame &frame, QKnxUdp *udp)
{
    if (!m_Modes.testFlag(ModeTunneling)) {
        // server does not accept tunneling, ignore
        // TODO: tell client tunneling is not supported
        return;
    }

    if (frame.connectInfo.infoType != QKnxConnectInfo::InfoRequest) {
        return;
    }

    bool fail = false;
    qint16 next_id = tunnelNextId();
    if (next_id < 0) {
        qDebug() << "Warning: cache out of connection ids";
        fail = true;
    }

    qint32 next_address = -1;
    if (frame.connectInfo.connectionType == QKnxNetIp::TUNNEL_CONNECTION) {
        next_address = tunnelNextAddress();
        if (next_address < 0) {
            qDebug() << "Warning: out of knx addresses.";
            fail = true;
        }
    }

    if (fail) {
        // TODO: answer with error code
        // failed to get connection id or knx address - send negative answer
        return;
    }

    QKnxTunnel* c = new QKnxTunnel(this,udp,(quint8)next_id);
    if (frame.connectInfo.connectionType == QKnxNetIp::TUNNEL_CONNECTION) {
        c->address.setAddress((quint16)next_address);
    }
    c->connectRequest(frame);

    // store client and address
    m_TunnelClients.insert(c->channelId,c);
    if (frame.connectInfo.connectionType == QKnxNetIp::TUNNEL_CONNECTION) {
        m_TunnelAddressPool.insert(c->address.address(),new quint8(c->channelId));
    }
}

void QKnxServer::handleConnectResponse(QKnxFrame &frame, QKnxUdp *udp)
{
    Q_UNUSED(frame); Q_UNUSED(udp);
}

void QKnxServer::handleConnectionStateRequest(QKnxFrame &frame, QKnxUdp *udp)
{
    if (!m_Modes.testFlag(ModeTunneling)) {
        // server does not accept tunneling, ignore
        // TODO: tell client tunneling is not supported
        return;
    }

    QKnxTunnel* c = getTunnelClient(frame.channelId);
    if (!c || (frame.controlEndpoint != c->discoveryEndpoint && frame.controlEndpoint != c->dataEndpoint)) {
        //qDebug() << "Client has wrong endpoint for channel id" << fd.channelId;
        QKnxFrame* error_frame = new QKnxFrame;
        QKnxFrame& ef = *error_frame;
        ef.serviceType = QKnxNetIp::CONNECTIONSTATE_RESPONSE;
        ef.channelId = frame.channelId;
        ef.errorCode = QKnxNetIp::E_CONNECTION_ID;

        // add remote host data
        ef.remoteHost = frame.controlEndpoint.hostAddress();
        ef.remotePort = frame.controlEndpoint.port();

        // write response
        udp->writeFrame(error_frame);

        return;
    }

    c->connectionStateRequest(frame);
}

void QKnxServer::handleConnectionStateResponse(QKnxFrame &frame, QKnxUdp *udp)
{
    Q_UNUSED(frame); Q_UNUSED(udp);
}

void QKnxServer::handleDisconnectRequest(QKnxFrame &frame, QKnxUdp *udp)
{
    if (!m_Modes.testFlag(ModeTunneling)) {
        // server does not accept tunneling, ignore
        // TODO: tell client tunneling is not supported
        return;
    }

    QKnxTunnel* c = getTunnelClient(frame.channelId);
    if (!c || c->udp != udp || (frame.controlEndpoint != c->discoveryEndpoint && frame.controlEndpoint != c->dataEndpoint)) {
        //qDebug() << "Client has wrong endpoint for channel id" << fd.channelId;
        return;
    }

    c->disconnectRequest(frame);

    removeTunnelClient(c);
}

void QKnxServer::handleDisconnectResponse(QKnxFrame &frame, QKnxUdp *udp)
{
    Q_UNUSED(frame); Q_UNUSED(udp);
}

void QKnxServer::handleDeviceConfigurationRequest(QKnxFrame &frame, QKnxUdp *udp)
{
    if (!m_Modes.testFlag(ModeTunneling)) {
        // server does not accept tunneling, ignore
        // TODO: tell client tunneling is not supported
        return;
    }

    QKnxTunnel* c = getTunnelClient(frame.channelId);
    if (!c || c->udp != udp) {
        //qDebug() << "Client has invalid channel id" << fd.channelId;
        return;
    }

    c->deviceConfigurationRequest(frame);
}

void QKnxServer::handleDeviceConfigurationAck(QKnxFrame &frame, QKnxUdp *udp)
{
    if (!m_Modes.testFlag(ModeTunneling)) {
        // server does not accept tunneling, ignore
        // TODO: tell client tunneling is not supported
        return;
    }

    QKnxTunnel* c = getTunnelClient(frame.channelId);
    if (!c || c->udp != udp) {
        //qDebug() << "Client has invalid channel id" << fd.channelId;
        return;
    }

    c->deviceConfigurationAck(frame);
}

void QKnxServer::handleTunnelingRequest(QKnxFrame &frame, QKnxUdp *udp)
{
    if (!m_Modes.testFlag(ModeTunneling)) {
        // server does not accept tunneling, ignore
        // TODO: tell client tunneling is not supported
        return;
    }

    QKnxTunnel* c = getTunnelClient(frame.channelId);
    if (!c || c->udp != udp) {
        //qDebug() << "Client has invalid channel id" << fd.channelId;
        return;
    }

    c->tunnelingRequest(frame);
}

void QKnxServer::handleTunnelingAck(QKnxFrame &frame, QKnxUdp *udp)
{
    if (!m_Modes.testFlag(ModeTunneling)) {
        // server does not accept tunneling, ignore
        // TODO: tell client tunneling is not supported
        return;
    }

    QKnxTunnel* c = getTunnelClient(frame.channelId);
    if (!c || c->udp != udp) {
        //qDebug() << "Client has invalid channel id" << fd.channelId;
        return;
    }

    c->tunnelingAck(frame);
}

void QKnxServer::handleRoutingIndication(QKnxFrame &frame, QKnxUdp *udp)
{
    Q_UNUSED(udp);

    if (frame.cEMI.messageCode == QKnxCemi::L_DATA_IND) {
        // forward to tunneling clients
        frame.serviceType = QKnxNetIp::TUNNELLING_REQUEST;
        frame.structureLength = 4;
        writeTunnel(frame,-1);

        // forward to server/user
        telegramReceived(frame.cEMI.telegram);
    }
}

void QKnxServer::handleRoutingLost(QKnxFrame &frame, QKnxUdp *udp)
{
    Q_UNUSED(frame); Q_UNUSED(udp);
}

void QKnxServer::handleRoutingBusy(QKnxFrame &frame, QKnxUdp *udp)
{
    Q_UNUSED(frame); Q_UNUSED(udp);
}

void QKnxServer::handleRemoteDiagRequest(QKnxFrame &frame, QKnxUdp *udp)
{
    Q_UNUSED(frame); Q_UNUSED(udp);
}

void QKnxServer::handleRemoteDiagResponse(QKnxFrame &frame, QKnxUdp *udp)
{
    Q_UNUSED(frame); Q_UNUSED(udp);
}

void QKnxServer::handleRemoteBasicConfRequest(QKnxFrame &frame, QKnxUdp *udp)
{
    Q_UNUSED(frame); Q_UNUSED(udp);
}

void QKnxServer::handleRemoteResponseEtRequest(QKnxFrame &frame, QKnxUdp *udp)
{
    Q_UNUSED(frame); Q_UNUSED(udp);
}

void QKnxServer::handleDefault(QKnxFrame &frame, QKnxUdp *udp)
{
    Q_UNUSED(frame); Q_UNUSED(udp);
    // Do nothing with unknown service type frame
}

QKnxTunnel *QKnxServer::getTunnelClient(quint8 channel_id)
{
    QKnxTunnel* c = m_TunnelClients.object(channel_id);
    if (!c) {
        return Q_NULLPTR;
    }
    if (c->updateTimer.elapsed() > m_TunnelClientTimeout) {
        removeTunnelClient(c);
        return Q_NULLPTR;
    }
    return c;
}

void QKnxServer::removeTunnelClient(QKnxTunnel *client)
{
    if (!client) {
        return;
    }

    // destroy client
    quint8 channel_id = client->channelId;
    if (client->connectionType == QKnxNetIp::TUNNEL_CONNECTION) {
        m_TunnelAddressPool.remove(client->address.address());
    }
    m_TunnelClients.remove(channel_id);
}

qint16 QKnxServer::tunnelNextId()
{
    quint8 start_nextid = m_TunnelNextId;
    while (getTunnelClient(m_TunnelNextId)) {
        m_TunnelNextId++;
        if (m_TunnelNextId == start_nextid) {
            return -1;
        }
    }
    return m_TunnelNextId++;
}

qint32 QKnxServer::tunnelNextAddress()
{
    quint16 base_address = m_DeviceInfo.address().address();
    if (m_TunnelPoolSize == 0) {
        return base_address;
    }

    if (base_address == 0) {
        qDebug() << "Warning: Base address is 0.0.0";
        return -1;
    }

    quint32 limit_test = (quint32)base_address + m_TunnelPoolSize;
    if (limit_test > std::numeric_limits<quint16>::max()) {
        qDebug() << "Warning: pool size exceeding address limit!";
        return -1;
    }

    quint16 max_address = base_address + m_TunnelPoolSize;
    if (m_TunnelNextAddress > max_address) {
        m_TunnelNextAddress = base_address + 1;
    }
    quint16 start_nextaddress = m_TunnelNextAddress;
    while (m_TunnelAddressPool.contains(m_TunnelNextAddress)) {
        m_TunnelNextAddress++;
        if (m_TunnelNextAddress > max_address) {
            m_TunnelNextAddress = base_address + 1;
        }
        if (m_TunnelNextAddress == start_nextaddress) {
            return -1;
        }
    }
    return m_TunnelNextAddress++;
}

void QKnxServer::writeTunnel(QKnxFrame &frame, qint16 from_client)
{
    if (!m_Modes.testFlag(QKnxServer::ModeTunneling)) {
        return;
    }

    // write frame to all connected clients
    foreach (quint8 c_to, m_TunnelClients.keys()) {
        if (c_to == from_client) {
            continue;
        }
        QKnxTunnel* c_to_ptr = getTunnelClient(c_to);
        if (!c_to_ptr) {
            continue;
        }
        QKnxFrame* frame_forward = new QKnxFrame(frame);
        frame_forward->channelId = c_to;

        // set remote host/port
        frame_forward->remoteHost = c_to_ptr->dataEndpoint.hostAddress();
        frame_forward->remotePort = c_to_ptr->dataEndpoint.port();

        c_to_ptr->writeFrame(frame_forward);
    }
}

void QKnxServer::writeMulticast(const QKnxFrame &frame)
{
    if (!m_Modes.testFlag(ModeRouting)) {
        return;
    }

    if (m_Protocol == QAbstractSocket::AnyIPProtocol || m_Protocol == QAbstractSocket::IPv4Protocol) {
        QKnxFrame* mcast_frame = new QKnxFrame(frame);
        mcast_frame->remoteHost = m_MulticastAddressIPv4;
        mcast_frame->remotePort = m_Port;
        m_Udp4.writeFrame(mcast_frame);
    }

    if (m_Protocol == QAbstractSocket::AnyIPProtocol || m_Protocol == QAbstractSocket::IPv6Protocol) {
        QKnxFrame* mcast_frame = new QKnxFrame(frame);
        mcast_frame->remoteHost = m_MulticastAddressIPv6;
        mcast_frame->remotePort = m_Port;
        m_Udp6.writeFrame(mcast_frame);
    }
}

void QKnxServer::processPendingFrames()
{
    QKnxUdp* udp = qobject_cast<QKnxUdp*>(sender());
    if (!udp) {
        return;
    }

    QKnxFrame* frame = udp->readFrame();
    while (frame) {
        switch (frame->serviceType)
        {
        case QKnxNetIp::SERVICE_NONE: handleDefault(*frame, udp); break;
        case QKnxNetIp::SEARCH_REQUEST: handleSearchRequest(*frame, udp); break;
        case QKnxNetIp::SEARCH_RESPONSE: handleSearchResponse(*frame, udp); break;
        case QKnxNetIp::DESCRIPTION_REQUEST: handleDescriptionRequest(*frame, udp); break;
        case QKnxNetIp::DESCRIPTION_RESPONSE: handleDescriptionResponse(*frame, udp); break;
        case QKnxNetIp::CONNECT_REQUEST: handleConnectRequest(*frame, udp); break;
        case QKnxNetIp::CONNECT_RESPONSE: handleConnectResponse(*frame, udp); break;
        case QKnxNetIp::CONNECTIONSTATE_REQUEST: handleConnectionStateRequest(*frame, udp); break;
        case QKnxNetIp::CONNECTIONSTATE_RESPONSE: handleConnectionStateResponse(*frame, udp); break;
        case QKnxNetIp::DISCONNECT_REQUEST: handleDisconnectRequest(*frame, udp); break;
        case QKnxNetIp::DISCONNECT_RESPONSE: handleDisconnectResponse(*frame, udp); break;
        case QKnxNetIp::DEVICE_CONFIGURATION_REQUEST: handleDeviceConfigurationRequest(*frame, udp); break;
        case QKnxNetIp::DEVICE_CONFIGURATION_ACK: handleDeviceConfigurationAck(*frame, udp); break;
        case QKnxNetIp::TUNNELLING_REQUEST: handleTunnelingRequest(*frame, udp); break;
        case QKnxNetIp::TUNNELLING_ACK: handleTunnelingAck(*frame, udp); break;
        case QKnxNetIp::ROUTING_INDICATION: handleRoutingIndication(*frame, udp); break;
        case QKnxNetIp::ROUTING_LOST: handleRoutingLost(*frame, udp); break;
        case QKnxNetIp::ROUTING_BUSY: handleRoutingBusy(*frame, udp); break;
        case QKnxNetIp::REMOTE_DIAG_REQUEST: handleRemoteDiagRequest(*frame, udp); break;
        case QKnxNetIp::REMOTE_DIAG_RESPONSE: handleRemoteDiagResponse(*frame, udp); break;
        case QKnxNetIp::REMOTE_BASIC_CONF_REQUEST: handleRemoteBasicConfRequest(*frame, udp); break;
        case QKnxNetIp::REMOTE_RESPONSEET_REQUEST: handleRemoteResponseEtRequest(*frame, udp); break;
        default: handleDefault(*frame, udp); break;
        }

        // delete frame after handling
        delete frame;

        // read next frame
        frame = udp->readFrame();
    }
}

void QKnxServer::resetCache()
{
    m_LocalAddressCache.clear();
    m_LocalMacAddressCache.clear();
    m_SearchResponseCache.clear();
    m_DescriptionResponseCache.clear();
}

void QKnxServer::tunnelAckCheck()
{
    QList<quint8> list = m_TunnelClients.keys();
    QKnxTunnel* c = Q_NULLPTR;
    foreach (quint8 k, list) {
        c = m_TunnelClients.object(k);
        if (!c) {
            continue;
        }
        if (!c->ackCheck()) {
            qDebug() << "client" << c->channelId << "failed to get acc, destroying connection";
            removeTunnelClient(c);
        }
    }
}

