#ifndef QKNXNETIPSERVER_H
#define QKNXNETIPSERVER_H

#include "qknxglobal.h"

#include "netip/qknxnetwork.h"
#include "netip/qknxtunnel.h"
#include "netip/qknxframe.h"
#include "io/qknxudp.h"

#include <QUdpSocket>
#include <QTimer>
#include <QDateTime>
#include <QNetworkInterface>
#include <QCache>

class QKNX_SHARED_EXPORT QKnxServer : public QObject
{
    Q_OBJECT
public:
    enum Mode {
        ModeNone = 0,
        ModeServer = 1 << 0,
        ModeRouting = 1 << 1,
        ModeTunneling = 1 << 2
    };
    Q_DECLARE_FLAGS(Modes,Mode)
    Q_FLAG(Modes)

    explicit QKnxServer(QObject* parent = Q_NULLPTR);
    virtual ~QKnxServer();

    // modes
    Modes modes() const { return m_Modes; }
    QString modesString() const { return modesString(m_Modes); }
    static QString modesString(const Modes& modes) { return QMetaEnum::fromType<Modes>().valueToKeys(modes); }
    void setModes(const Modes& modes) { if(!m_Started) m_Modes = modes; updateSuppSvcs(); }
    void setModes(const int& modes) { if(!m_Started) m_Modes = Modes(modes); updateSuppSvcs(); }
    bool hasMode(const Mode& mode) const { return m_Modes.testFlag(mode); }
    static QString modeString(const Mode& mode) { return QMetaEnum::fromType<Modes>().valueToKey(mode); }
    void setMode(const Mode& mode, const bool& value) { if(!m_Started) m_Modes.setFlag(mode,value); updateSuppSvcs(); }
    void modeEnable(const Mode& mode) { setMode(mode,true); updateSuppSvcs(); }
    void modeDisable(const Mode& mode) { setMode(mode,false); updateSuppSvcs(); }

    // server config
    bool modeServer() const { return m_Modes.testFlag(ModeServer); }
    void setModeServer(bool start = true) { setMode(ModeServer,start); }
    bool modeRouting() const { return m_Modes.testFlag(ModeRouting); }
    void setModeRouting(bool start = true) { setMode(ModeRouting,start); }
    bool modeTunneling() const { return m_Modes.testFlag(ModeTunneling); }
    void setModeTunneling(bool start = true) { setMode(ModeTunneling,start); }

    QKnxDeviceInfo deviceInfo() const { return m_DeviceInfo; }
    void setDeviceInfo(const QKnxDeviceInfo& dev_info);

    // tunneling address poolsize
    quint8 tunnelPoolSize() const { return m_TunnelPoolSize; }
    void setTunnelPoolSize(const quint8& pool_size);

    quint16 port() const { return m_Port; }
    void setPort(const quint16& port) { if(!m_Started) m_Port = port; }

    QAbstractSocket::NetworkLayerProtocol protocol() const { return m_Protocol; }
    void setProtocol(const QAbstractSocket::NetworkLayerProtocol& protocol) { if(!m_Started) m_Protocol = protocol; }

    QHostAddress multicastIPv4() const { return m_MulticastAddressIPv4; }
    void setMulticastIPv4(const QHostAddress& mcast_address) { if(!m_Started) m_MulticastAddressIPv4 = mcast_address; }
    void useDefaultMulticastIPv4() { if(!m_Started) m_MulticastAddressIPv4 = QKnxEndpoint::defaultMulticastIPv4(); }

    QHostAddress multicastIPv6() const { return m_MulticastAddressIPv6; }
    void setMulticastIPv6(const QHostAddress& mcast_address) { if(!m_Started) m_MulticastAddressIPv6 = mcast_address; }
    void useDefaultMulticastIPv6() { if(!m_Started) m_MulticastAddressIPv6 = QKnxEndpoint::defaultMulticastIPv6(); }

signals:
    void serverStarted();
    void serverStopped();
    void telegramReceived(const QKnxTelegram& telegram);

public slots:
    void serverStart();
    void serverStop();
    void writeTelegram(const QKnxTelegram& telegram);

private:
    bool m_Started;
    quint64 m_TelegramCount;

    // udp connections
    quint16 m_Port;
    QKnxUdp m_Udp4;
    QKnxUdp m_Udp6;

    // device setup
    Modes m_Modes;
    QAbstractSocket::NetworkLayerProtocol m_Protocol;
    QUdpSocket m_SocketCacheLookup;
    QHostAddress m_MulticastAddressIPv4;
    QHostAddress m_MulticastAddressIPv6;

    QKnxDeviceInfo m_DeviceInfo;
    QKnxSuppSvcs m_SuppSvcsSearch;
    QKnxSuppSvcs m_SuppSvcsDescription;

    // interfaces
    QKnxNetwork m_Network;

    // tunneling clients
    qint64 m_TunnelClientTimeout;
    quint8 m_TunnelNextId;
    QCache<quint8, QKnxTunnel> m_TunnelClients;
    quint8 m_TunnelPoolSize;
    quint16 m_TunnelNextAddress;
    QCache<quint16, quint8> m_TunnelAddressPool;
    QTimer m_TunnelAckTimer;

    // caches
    QCache<QHostAddress, QHostAddress> m_LocalAddressCache;
    QCache<QHostAddress, QKnxMacAddress> m_LocalMacAddressCache;
    QCache<QHostAddress, QKnxFrame> m_SearchResponseCache;
    QCache<QHostAddress, QKnxFrame> m_DescriptionResponseCache;

    // cache access
    QHostAddress* cachedLocalAddress(const QHostAddress& remote_address);
    QKnxMacAddress* cachedLocalMacAddress(const QHostAddress& local_address);
    QKnxFrame* cachedSearchResponse(const QHostAddress& local_address);
    QKnxFrame* cachedDescriptionResponse(const QHostAddress& local_address);
    void createCachedResponse(const QHostAddress& local_address);

    // supported services update
    void updateSuppSvcs();

    // request handling
    void handleSearchRequest(QKnxFrame& frame, QKnxUdp* udp);
    void handleSearchResponse(QKnxFrame& frame, QKnxUdp* udp);
    void handleDescriptionRequest(QKnxFrame& frame, QKnxUdp* udp);
    void handleDescriptionResponse(QKnxFrame& frame, QKnxUdp* udp);
    void handleConnectRequest(QKnxFrame& frame, QKnxUdp* udp);
    void handleConnectResponse(QKnxFrame& frame, QKnxUdp* udp);
    void handleConnectionStateRequest(QKnxFrame& frame, QKnxUdp* udp);
    void handleConnectionStateResponse(QKnxFrame& frame, QKnxUdp* udp);
    void handleDisconnectRequest(QKnxFrame& frame, QKnxUdp* udp);
    void handleDisconnectResponse(QKnxFrame& frame, QKnxUdp* udp);
    void handleDeviceConfigurationRequest(QKnxFrame& frame, QKnxUdp* udp);
    void handleDeviceConfigurationAck(QKnxFrame& frame, QKnxUdp* udp);
    void handleTunnelingRequest(QKnxFrame& frame, QKnxUdp* udp);
    void handleTunnelingAck(QKnxFrame& frame, QKnxUdp* udp);
    void handleRoutingIndication(QKnxFrame& frame, QKnxUdp* udp);
    void handleRoutingLost(QKnxFrame& frame, QKnxUdp* udp);
    void handleRoutingBusy(QKnxFrame& frame, QKnxUdp* udp);
    void handleRemoteDiagRequest(QKnxFrame& frame, QKnxUdp* udp);
    void handleRemoteDiagResponse(QKnxFrame& frame, QKnxUdp* udp);
    void handleRemoteBasicConfRequest(QKnxFrame& frame, QKnxUdp* udp);
    void handleRemoteResponseEtRequest(QKnxFrame& frame, QKnxUdp* udp);
    void handleDefault(QKnxFrame& frame, QKnxUdp* udp);

    // tunneling functions
    QKnxTunnel* getTunnelClient(quint8 channel_id);
    void removeTunnelClient(quint8 channel_id) { removeTunnelClient(m_TunnelClients.object(channel_id)); }
    void removeTunnelClient(QKnxTunnel* client);
    qint16 tunnelNextId();
    qint32 tunnelNextAddress();

    // writing functions
    //void writeTelegram(QKnxTelegram &telegram, qint16 from_client = -1);
    void writeTunnel(QKnxFrame &frame, qint16 from_client = -1);
    void writeMulticast(const QKnxFrame &frame);

    // tunnel class functions as friend
    friend void QKnxTunnel::connectRequest(QKnxFrame& frame);
    friend void QKnxTunnel::connectionStateRequest(QKnxFrame& frame);
    friend void QKnxTunnel::disconnectRequest(QKnxFrame& frame);
    friend void QKnxTunnel::tunnelingRequest(QKnxFrame& frame);
    friend void QKnxTunnel::tunnelingAck(QKnxFrame& frame);

    friend void QKnxTunnel::deviceConfigurationRequest(QKnxFrame& frame);
    friend void QKnxTunnel::deviceConfigurationAck(QKnxFrame& frame);

private slots:
    void processPendingFrames();
    void resetCache();
    void tunnelAckCheck();

};
Q_DECLARE_OPERATORS_FOR_FLAGS(QKnxServer::Modes)

#endif // QKNXNETIPSERVER_H
