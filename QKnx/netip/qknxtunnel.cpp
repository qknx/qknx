#include "qknxtunnel.h"


#include "netip/qknxserver.h"

#include <climits>

QKnxTunnel::QKnxTunnel(QKnxServer *server, QKnxUdp *udp, quint8 channel_id) : QObject(), server(server), udp(udp), channelId(channel_id), sequenceReceive(0), sequenceSend(0),
                                                                                  connectionType(QKnxNetIp::CONNECTION_NONE), reserved(0), nextAck(Q_NULLPTR), nextAckResent(false)
{
    //sequenceCounter = std::numeric_limits<quint8>::max();
    address.setPhysicalAddress();
}

QKnxTunnel::~QKnxTunnel()
{
    qDeleteAll(outputQueue);
    delete nextAck;
}

void QKnxTunnel::connectRequest(QKnxFrame &frame)
{
    if (!server) {
        return;
    }

    discoveryEndpoint = frame.discoveryEndpoint;
    dataEndpoint = frame.dataEndpoint;
    connectionType = frame.connectInfo.connectionType;
    knxLayers = frame.connectInfo.knxLayers;
    reserved = frame.connectInfo.reserved;
    updateTimer.start();

    // create response message
    QKnxFrame* result = new QKnxFrame;
    QKnxFrame& res = *result;
    res.serviceType = QKnxNetIp::CONNECT_RESPONSE;
    res.channelId = channelId;
    res.errorCode = QKnxNetIp::E_NO_ERROR;

    // local endpoint
    QHostAddress* local_address = server->cachedLocalAddress(frame.discoveryEndpoint.hostAddress());
    res.dataEndpoint.setHostProtocolCode(discoveryEndpoint.hostProtocolCode());
    res.dataEndpoint.setHostAddress(*local_address);
    res.dataEndpoint.setPort(server->m_Port);

    // connection info
    res.connectInfo.infoType = QKnxConnectInfo::InfoResponse;
    res.connectInfo.connectionType = connectionType;
    res.connectInfo.address = address;

    // add remote host data
    res.remoteHost = frame.discoveryEndpoint.hostAddress();
    res.remotePort = frame.discoveryEndpoint.port();

    // write response
    writeFrame(result);
}

void QKnxTunnel::connectionStateRequest(QKnxFrame &frame)
{
    // reset timer
    updateTimer.start();

    // create response
    QKnxFrame* result = new QKnxFrame;
    QKnxFrame& res = *result;
    res.serviceType = QKnxNetIp::CONNECTIONSTATE_RESPONSE;
    res.channelId = channelId;
    res.errorCode = QKnxNetIp::E_NO_ERROR;

    // add remote host data
    res.remoteHost = frame.controlEndpoint.hostAddress();
    res.remotePort = frame.controlEndpoint.port();

    // write response
    writeFrame(result);
}

void QKnxTunnel::disconnectRequest(QKnxFrame &frame)
{
    // create response
    QKnxFrame* result = new QKnxFrame;
    QKnxFrame& res = *result;
    res.serviceType = QKnxNetIp::DISCONNECT_RESPONSE;
    quint8 channel_id = channelId;
    res.channelId = channel_id;
    res.errorCode = QKnxNetIp::E_NO_ERROR;

    // add remote host data
    res.remoteHost = frame.controlEndpoint.hostAddress();
    res.remotePort = frame.controlEndpoint.port();

    // write response
    writeFrame(result);
}

void QKnxTunnel::tunnelingRequest(QKnxFrame &frame)
{
    if (!server) {
        return;
    }

    quint8 last_seq = sequenceReceive;
    last_seq--;
    if (frame.sequenceCounter != last_seq && frame.sequenceCounter != sequenceReceive)
    {
        qDebug() << "Got frame with invalid sequence. frame:" << frame.sequenceCounter << "internal:" << sequenceReceive;
        return;
    }

    // send ack
    QKnxFrame* frame_ack = new QKnxFrame;
    QKnxFrame& f_ack = *frame_ack;
    f_ack.serviceType = QKnxNetIp::TUNNELLING_ACK;
    f_ack.structureLength = frame.structureLength;
    f_ack.channelId = frame.channelId;
    f_ack.sequenceCounter = frame.sequenceCounter;
    f_ack.errorCode = QKnxNetIp::E_NO_ERROR;

    f_ack.remoteHost = dataEndpoint.hostAddress();
    f_ack.remotePort = dataEndpoint.port();

    // write ack
    writeFrame(frame_ack);

    if (frame.sequenceCounter != sequenceReceive) {
        return;
    }

    if (frame.cEMI.messageCode == QKnxCemi::L_DATA_REQ) {
        // write to multicast

        // convert to multicast frame, send, convert back to tunneling request
        frame.serviceType = QKnxNetIp::ROUTING_INDICATION;
        frame.cEMI.messageCode = QKnxCemi::L_DATA_IND;
        frame.cEMI.telegram.setSource(address.address());
        frame.cEMI.telegram.routingCountDecrease();

        server->writeMulticast(frame);

        frame.serviceType = QKnxNetIp::TUNNELLING_REQUEST;
        frame.cEMI.telegram.setSource((quint16)0);
        frame.cEMI.telegram.routingCountIncrease();

        // update sequence counter
        //frame.sequenceCounter = frame.sequenceCounter;

        // send confirmation
        QKnxFrame* con = new QKnxFrame(frame);
        con->cEMI.messageCode = QKnxCemi::L_DATA_CON;
        con->remoteHost = dataEndpoint.hostAddress();
        con->remotePort = dataEndpoint.port();

        //udp->writeFrame(con);
        writeFrame(con);

        //relay message to other clients and broadcast
        frame.cEMI.messageCode = QKnxCemi::L_DATA_IND;
        frame.cEMI.telegram.setSource(address.address());
        server->writeTunnel(frame,channelId);

        // relay to server/user
        server->telegramReceived(frame.cEMI.telegram);
    }

    // increase sequence counter
    sequenceReceive++;
}

void QKnxTunnel::tunnelingAck(QKnxFrame &frame)
{
    if (!nextAck) {
        qDebug() << "Got ack while not waiting for one.";
    }

    if (nextAck->sequenceCounter != frame.sequenceCounter) {
        qDebug() << "Got ack with wrong sequence number";
        return;
    }

    delete nextAck;
    nextAck = 0;

    // got ack, handle queue
    if (!outputQueue.isEmpty()) {
        //qDebug() << "sending from queue";
        QKnxFrame* next = outputQueue.dequeue();
        nextAck = new QKnxFrame(*next);
        nextAckTimer.start();
        nextAckResent = false;
        udp->writeFrame(next);
    }
}

void QKnxTunnel::deviceConfigurationRequest(QKnxFrame &frame)
{
    if (!server) {
        return;
    }

    quint8 last_seq = sequenceReceive;
    last_seq--;
    if (frame.sequenceCounter != last_seq && frame.sequenceCounter != sequenceReceive)
    {
        qDebug() << "Got frame with invalid sequence. frame:" << frame.sequenceCounter << "internal:" << sequenceReceive;
        return;
    }

    QKnxFrame* frame_ack = new QKnxFrame;
    QKnxFrame& f_ack = *frame_ack;
    f_ack.serviceType = QKnxNetIp::DEVICE_CONFIGURATION_ACK;
    f_ack.structureLength = frame.structureLength;
    f_ack.channelId = frame.channelId;
    f_ack.sequenceCounter = frame.sequenceCounter;
    f_ack.errorCode = QKnxNetIp::E_NO_ERROR;

    f_ack.remoteHost = dataEndpoint.hostAddress();
    f_ack.remotePort = dataEndpoint.port();

    // write ack
    writeFrame(frame_ack);

    if (frame.sequenceCounter != sequenceReceive) {
        return;
    }

    // got M_PropRead.req
    QKnxFrame* con = new QKnxFrame(frame);
    if (con->cEMI.messageCode == QKnxCemi::M_PROPREAD_REQ)
    {
        // answer with M_PropRead.con
        con->cEMI.messageCode = QKnxCemi::M_PROPREAD_CON;
        con->cEMI.numberOfElements = 0x00;
        con->cEMI.errorCode = 0x00;

        con->remoteHost = dataEndpoint.hostAddress();
        con->remotePort = dataEndpoint.port();

        //udp->writeFrame(con);
        writeFrame(con);
    }

    sequenceReceive++;
}

void QKnxTunnel::deviceConfigurationAck(QKnxFrame &frame)
{
    tunnelingAck(frame);
}

void QKnxTunnel::writeFrame(QKnxFrame *frame)
{
    if (!frame || !udp) {
        qDebug() << "Warning: frame, tunnel or udp is nullptr!";
        return;
    }

    if (frame->serviceType == QKnxNetIp::TUNNELLING_REQUEST || frame->serviceType == QKnxNetIp::DEVICE_CONFIGURATION_REQUEST) {
        frame->sequenceCounter = sequenceSend++;
        if (!nextAck) {
            nextAck = new QKnxFrame(*frame);
            nextAckTimer.start();
            nextAckResent = false;
            udp->writeFrame(frame);
        } else {
            outputQueue.enqueue(frame);
        }
    } else {
        udp->writeFrame(frame);
    }
}

bool QKnxTunnel::ackCheck()
{
    if (!server || !udp) {
        qDebug() << "Warning: tunnel or udp is nullptr!";
        return false;
    }

    qint64 elapsed = nextAckTimer.elapsed();
    if (!nextAck || elapsed < QKNX_UDP_ACK_TIMEOUT_RESEND) {
        return true;
    }

    if (elapsed > QKNX_UDP_ACK_TIMEOUT_FAIL) {
        qDebug() << "Destroying ack timeout fail connection";
        return false;
    }

    if (!nextAckResent && elapsed > QKNX_UDP_ACK_TIMEOUT_RESEND) {
        qDebug() << "resending frame";
        udp->writeFrame(nextAck);
        nextAckResent = true;
    }
    return true;
}
