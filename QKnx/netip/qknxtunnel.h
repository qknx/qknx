#ifndef QKNXTUNNEL_H
#define QKNXTUNNEL_H

#include "qknxglobal.h"

#include "netip/qknxframe.h"
#include "io/qknxudp.h"
#include "netip/qknxendpoint.h"
#include "netip/qknxnetip.h"
#include "qknxaddress.h"

#include <QElapsedTimer>
#include <QQueue>

class QKnxServer;

class QKNX_SHARED_EXPORT QKnxTunnel : public QObject
{
    Q_OBJECT
public:
    explicit QKnxTunnel(QKnxServer* server, QKnxUdp* udp, quint8 channel_id);
    virtual ~QKnxTunnel();

    // connection & transmission functions
    void connectRequest(QKnxFrame& frame);
    void connectionStateRequest(QKnxFrame& frame);
    void disconnectRequest(QKnxFrame& frame);
    void tunnelingRequest(QKnxFrame& frame);
    void tunnelingAck(QKnxFrame& frame);

    // device management
    void deviceConfigurationRequest(QKnxFrame& frame);
    void deviceConfigurationAck(QKnxFrame& frame);

    void writeFrame(QKnxFrame* frame);

    bool ackCheck();

    QKnxServer* server;
    QKnxUdp* udp;
    quint8 channelId;
    quint8 sequenceReceive;
    quint8 sequenceSend;
    QElapsedTimer updateTimer;
    QKnxEndpoint discoveryEndpoint;
    QKnxEndpoint dataEndpoint;
    QKnxAddress address;
    QKnxNetIp::ConnectionType connectionType;
    QKnxNetIp::KnxLayers knxLayers;
    quint8 reserved;
    QQueue<QKnxFrame*> outputQueue;
    QKnxFrame* nextAck;
    bool nextAckResent;
    QElapsedTimer nextAckTimer;

signals:

public slots:

};

#endif // QKNXTUNNEL_H
