#include "qknxaddress.h"

#include <QDataStream>

QKnxAddress::~QKnxAddress()
{

}

bool QKnxAddress::operator==(const QKnxAddress &other_addr) const
{
    if (address() != other_addr.address()) {
        return false;
    }

    if (daf() != other_addr.daf()) {
        return false;
    }

    return true;
}

bool QKnxAddress::setAddress(const QByteArray &address)
{
    if (address.size() != 2) {
        qDebug() << "setAddress(ByteArray) - error: byte array is not 2 bytes long.";
        return false;
    }

    quint16 tmp_uint = 0x0000;
    tmp_uint |= ((quint16)(address.at(0)) << 8);
    tmp_uint |= (quint8)address.at(1);
    setAddress(tmp_uint);
    return true;
}

bool QKnxAddress::setAddress(const QString &address_str)
{
    // physical address
    if (address_str.split(".").count() == 3) {
        QStringList tmp = address_str.split(".");
        QList<uint> tmp_uint;
        for(quint8 i=0; i<3; i++) {
            tmp_uint.append(tmp.at(i).toUInt());
        }
        if (tmp_uint.at(0) > 15 || tmp_uint.at(1) > 15 || tmp_uint.at(2) > 255) {
            qDebug() << "setAddress() - error: exceeding limits.";
            return false;
        }
        m_Address = 0x0000 | ((tmp_uint.at(0) & 0x0F) << 12);
        m_Address |= (tmp_uint.at(1) & 0x0F) << 8;
        m_Address |= tmp_uint.at(2) & 0xFF;
        setFormat(FormatPhysical);
        return true;
    }

    // group address 2 part
    if (address_str.split("/").count() == 2) {
        QStringList tmp = address_str.split("/");
        QList<uint> tmp_uint;
        for(quint8 i=0; i<2; i++) {
            tmp_uint.append(tmp.at(i).toUInt());
        }
        if (tmp_uint.at(0) > 15 || tmp_uint.at(1) > 2047 ) {
            qDebug() << "setAddress() - error: exceeding limits.";
            return false;
        }
        m_Address = 0x0000 | ((tmp_uint.at(0) & 0x0F) << 11);
        m_Address |= tmp_uint.at(1) & 0x07FF;
        setFormat(FormatGroup2Part);
        return true;
    }

    // group address 3 part
    if (address_str.split("/").count() == 3) {
        QStringList tmp = address_str.split("/");
        QList<uint> tmp_uint;
        for(quint8 i=0; i<3; i++) {
            tmp_uint.append(tmp.at(i).toUInt());
        }
        if (tmp_uint.at(0) > 15 || tmp_uint.at(1) > 7 || tmp_uint.at(2) > 255) {
            qDebug() << "setAddress() - error: exceeding limits.";
            return false;
        }
        m_Address = 0x0000 | ((tmp_uint.at(0) & 0x0F) << 11);
        m_Address |= (tmp_uint.at(1) & 0x07) << 8;
        m_Address |= tmp_uint.at(2) & 0xFF;
        setFormat(FormatGroup3Part);
        return true;
    }

    setFormat(FormatUndefined);
    return false;
}

bool QKnxAddress::setAddress(QDataStream &ds)
{
    quint16 addr;
    ds >> addr;
    setAddress(addr);
    return (ds.status() == QDataStream::Ok);
}

int QKnxAddress::mainGroup()
{
    if (!isGroupAddress()) {
        return -1;
    }

    return toString().split("/").first().toInt();
}

int QKnxAddress::middleGroup()
{
    if (!isGroup3PartAddress()) {
        return -1;
    }

    return toString().split("/").at(1).toInt();
}

int QKnxAddress::subGroup()
{
    if (!isGroupAddress()) {
        return -1;
    }

    return toString().split("/").last().toInt();
}

QKnxAddress QKnxAddress::fromString(QString address_str, bool *ok)
{
    QKnxAddress addr;
    bool success = addr.setAddress(address_str);
    if (ok) *ok = success;
    return addr;
}

QString QKnxAddress::toString() const
{
    if (isPhysicalAddress()) {
        QString addr = "%1.%2.%3";
        addr = addr.arg(QString::number((m_Address >> 12) & 0x0F));
        addr = addr.arg(QString::number((m_Address >> 8) & 0x0F));
        addr = addr.arg(QString::number(m_Address & 0xFF));
        return addr;
    }
    if (isGroup3PartAddress()) {
        QString addr = "%1/%2/%3";
        addr = addr.arg(QString::number((m_Address >> 11) & 0x0F));
        addr = addr.arg(QString::number((m_Address >> 8) & 0x07));
        addr = addr.arg(QString::number(m_Address & 0xFF));
        return addr;
    }
    if (isGroup2PartAddress()) {
        QString addr = "%1/%2";
        addr = addr.arg(QString::number((m_Address >> 11) & 0x0F));
        addr = addr.arg(QString::number(m_Address & 0x07FF));
        return addr;
    }
    return "";
}

QByteArray QKnxAddress::toByteArray() const
{
    QByteArray b_arr;
    b_arr.append((char)(m_Address >> 8));
    b_arr.append((char)(m_Address & 0xFF));
    return b_arr;
}

QString QKnxAddress::rangeToString(quint16 start, quint16 end, QKnxAddress::Format format, bool shorten)
{
    QKnxAddress addr_start;
    addr_start.setGroupAddress();
    addr_start.setFormat(format);
    addr_start.setAddress(start);

    QKnxAddress addr_end;
    addr_end.setGroupAddress();
    addr_end.setFormat(format);
    addr_end.setAddress(end);

    QStringList start_ = addr_start.toString().split("/");
    QStringList end_ = addr_end.toString().split("/");

    if (start_.count() != end_.count()) {
        qDebug() << "start and stop group address have different part count";
        return "";
    }

    QStringList res;
    int i = 0;
    while (i < start_.count()) {
        if (start_.at(i) == end_.at(i)) {
            res.append(start_.at(i));
        } else {
            if (shorten) break;
            res.append(QString("x"));
        }
        i++;
    }
    return res.join("/");
}

QKnxAddress::Format &QKnxAddress::defaultFormatGroupPrivate()
{
    static Format default_group_format = FormatGroup3Part;
    return default_group_format;
}

void QKnxAddress::initAddress()
{
    m_Address = 0x0000;
    m_Format = defaultFormat();
    //m_Format = FormatGroup3Part;
}

QKnxStream &operator>>(QKnxStream &fs, QKnxAddress &address)
{
    fs >> address.m_Address;
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const QKnxAddress &address)
{
    fs << address.m_Address;
    return fs;
}
