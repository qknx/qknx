#ifndef QKNXADDRESS_H
#define QKNXADDRESS_H

//#include "dpt/qknxdptinfo.h"
#include "qknxstream.h"

class QKNX_SHARED_EXPORT QKnxAddress
{
    Q_GADGET
public:
    enum Format {
        FormatUndefined = 0,
        FormatPhysical,
        FormatGroup2Part,
        FormatGroup3Part
    };
    Q_ENUM(Format)

    explicit QKnxAddress() { initAddress(); }
    explicit QKnxAddress(const QString addr_str) { initAddress(); setAddress(addr_str); }
    explicit QKnxAddress(const quint16 address, Format format) { initAddress(); setFormat(format); setAddress(address); }
    explicit QKnxAddress(const char* address) { initAddress(); setAddress(address); }
    explicit QKnxAddress(const QByteArray address, Format format) { initAddress(); setFormat(format); setAddress(address); }
    virtual ~QKnxAddress();

    // comparison operators
    bool operator== (const QKnxAddress &other_addr) const;
    bool operator!= (const QKnxAddress &other_addr) const { return !(*this == other_addr); }
    bool operator> (const QKnxAddress &other_addr) const { return (address() > other_addr.address()); }
    bool operator>= (const QKnxAddress &other_addr) const { return (address() >= other_addr.address()); }
    bool operator< (const QKnxAddress &other_addr) const { return (address() < other_addr.address()); }
    bool operator<= (const QKnxAddress &other_addr) const { return (address() <= other_addr.address()); }

    // conversion operators
    operator QString() const { return toString(); }

    // global formatting options
    static Format defaultFormat() { return FormatUndefined; }
    static Format defaultFormatGroup() { return defaultFormatGroupPrivate(); }
    static void setDefaultFormatGroup(const Format format_grp) { if (format_grp == FormatGroup2Part || format_grp == FormatGroup3Part) defaultFormatGroupPrivate() = format_grp; }

    quint16 address() const { return m_Address; }
    void setAddress(const quint16& address) { m_Address = address; }
    bool setAddress(const char* address_str) { return setAddress(QString(address_str)); }
    bool setAddress(const QByteArray& address);
    bool setAddress(const QString& address_str);
    bool setAddress(QDataStream& ds);

    //QKnxDptInfo dptInfo() const { return m_DptInfo; }
    //void setDptInfo(const QKnxDptInfo& dpt_info) { m_DptInfo = dpt_info; }

    QString name() const { return m_Name; }
    void setName(const QString& name) { m_Name = name; }

    QString desc() const { return m_Desc; }
    void setDesc(const QString& desc) { m_Desc = desc; }

    int mainGroup();
    int middleGroup();
    int subGroup();

    static QKnxAddress fromString(QString address_str, bool* ok = Q_NULLPTR);

    // DAF=0 -> physical address, DAF=1 -> group address
    bool daf() const { return isGroupAddress(); }
    void setDAF(const bool daf) { if (daf) {setGroupAddress();} else { setPhysicalAddress();} }
    bool isGroupAddress() const { return (isGroup2PartAddress() || isGroup3PartAddress()); }
    bool isGroup2PartAddress() const { return (format() == FormatGroup2Part); }
    bool isGroup3PartAddress() const { return (format() == FormatGroup3Part); }
    void setGroupAddress() { setFormat(defaultFormatGroup()); }
    bool isPhysicalAddress() const { return (format() == FormatPhysical); }
    void setPhysicalAddress() { setFormat(FormatPhysical); }

    Format format() const { return m_Format; }
    void setFormat(const Format format) { m_Format = format; }

    QString toString() const;
    QByteArray toByteArray() const;

    bool isValid() const { return (m_Format != FormatUndefined); }
    static QString rangeToString(quint16 start, quint16 end, Format format, bool shorten = false);

private:
    quint16 m_Address;
    Format m_Format;

    //QKnxDptInfo m_DptInfo;
    QString m_Name;
    QString m_Desc;

    static Format& defaultFormatGroupPrivate();

    void initAddress();

    friend QKnxStream& operator>>(QKnxStream& fs, QKnxAddress& address);
    friend QKnxStream& operator<<(QKnxStream& fs, const QKnxAddress& address);
};
//Q_DECLARE_METATYPE(QKnxAddress)

QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QKnxAddress& address);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const QKnxAddress& address);

#endif // QKNXADDRESS_H
