#include "qknxglobal.h"

#include "log/qknxlog.h"

void QKnxGlobal::init()
{
    // setup logging system
    QKnxLog::registerLogger();
}

