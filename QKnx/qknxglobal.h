#ifndef QKNX_GLOBAL_H
#define QKNX_GLOBAL_H

#include <QtCore/qglobal.h>

#include <QObject>
#include <QMetaType>
#include <QMetaEnum>
#include <QDebug>

// Library version
#define QKNX_LIBRARY_NAME "qknx"
#define QKNX_LIBRARY_VERSION "0.0.4"

// Helper macros for type registration
#define QKNX_TYPE_TO_STRING(T) #T
#define QKNX_REGISTER_TYPE(T) QKnxGlobal::registerType<T>(#T)

// Udp
#define QKNX_UDP_DEFAULT_ADDRESS_POOL_SIZE 8
#define QKNX_UDP_DEFAULT_MULTICAST_IPV4 "224.0.23.12"
#define QKNX_UDP_DEFAULT_MULTICAST_IPV6 "0:0:0:0:0:ffff:e000:170c"
#define QKNX_UDP_IFACE_TEST_DNS_HOST "8.8.8.8"
#define QKNX_UDP_IFACE_TEST_DNS_PORT 53
#define QKNX_UDP_DEFAULT_PORT 3671
#define QKNX_UDP_TUNNELING_TIMEOUT 125
#define QKNX_UDP_ACK_TIMEOUT_RESEND 1000
#define QKNX_UDP_ACK_TIMEOUT_FAIL 2000

// Shared library defines
#if defined(QKNX_LIBRARY)
#  define QKNX_SHARED_EXPORT Q_DECL_EXPORT
#else
#  define QKNX_SHARED_EXPORT Q_DECL_IMPORT
#endif

class QKNX_SHARED_EXPORT QKnxGlobal {
public:
    static QString libName() { return QKNX_LIBRARY_NAME; }
    static QString libVersion() { return QKNX_LIBRARY_VERSION; }
    static QString libString() { return libName().append("-").append(libVersion()); }

    template<typename T> static void registerType(const char* typeName)
    {
        if (!QMetaType::isRegistered(QMetaType::type(typeName))) {
            qDebug() << "registering meta type for qknx type:" << typeName;
            qRegisterMetaType<T>(typeName);
        }
    }

    static void init();
};

#endif // QKNX_GLOBAL_H
