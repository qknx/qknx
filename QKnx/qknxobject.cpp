#include "qknxobject.h"

QKnxObject::QKnxObject(QObject *parent) : QObject(parent)
{
    setRandomID();
}

bool QKnxObject::setID(QUuid id)
{
    QMutexLocker lock(&idMutex());
    if (ids().contains(id)) {
        return false;
    }

    QUuid old_id = m_ID;
    m_ID = id;
    ids().append(id);
    emit(idChanged(old_id,id));

    //qDebug() << ids() << metaObject()->className();

    return true;
}

QUuid QKnxObject::setRandomID()
{
    QUuid new_id = QUuid::createUuid();
    while (1) {
        if (setID(new_id)) {
            return new_id;
        }
        new_id = QUuid::createUuid();
    }
    return QUuid();
}

QMutex &QKnxObject::idMutex()
{
    static QMutex uuids_mutex;
    return uuids_mutex;
}

QList<QUuid> &QKnxObject::ids()
{
    static QList<QUuid> uuid_list;
    return uuid_list;
}

