#ifndef QKNXOBJECT_H
#define QKNXOBJECT_H

#include "qknxglobal.h"

//#include "qknxworker.h"

#include <QObject>
#include <QUuid>
#include <QDebug>
#include <QMutex>
#include <QMutexLocker>

class QKNX_SHARED_EXPORT QKnxObject : public QObject
{
    Q_OBJECT
public:
    explicit QKnxObject(QObject *parent = 0);
    virtual ~QKnxObject() {}

    QUuid id() { return m_ID; }
    bool setID(QUuid id);
    QUuid setRandomID();

signals:
    void idChanged(QUuid old_id, QUuid new_id);

public slots:

private:
    QUuid m_ID;
    static QMutex& idMutex();
    static QList<QUuid>& ids();
};

#endif // QKNXOBJECT_H
