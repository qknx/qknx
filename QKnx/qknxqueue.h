#ifndef QKNXQUEUE_H
#define QKNXQUEUE_H

#include "qknxglobal.h"

#include <QQueue>
#include <QSemaphore>
#include <QMutex>
#include <QMutexLocker>

template<class T> class QKnxQueue
{
public:
    explicit QKnxQueue(int queue_size = -1) : m_QueueCapacity(queue_size), m_TakeOwnership(true) {}
    ~QKnxQueue() { clear(); }

    bool takeOwnership() const { QMutexLocker lock(&m_Mutex); return m_TakeOwnership; }
    void setTakeOwnership(const bool& take_ownership) { QMutexLocker lock(&m_Mutex); m_TakeOwnership = take_ownership; }


    int enqueue(const T &value)
    {
        m_Mutex.lock();
        if (m_QueueCapacity != -1 && m_QueueCapacity == m_Queue.count()) {
            m_Mutex.unlock();
            return 0;
        }
        m_Queue.enqueue(value);
        int c = m_Queue.count();
        m_Mutex.unlock();
        return c;
    }

    T dequeue()
    {
        QMutexLocker lock(&m_Mutex);
        if (m_Queue.isEmpty()) {
            if (isPtr()) {
                return Q_NULLPTR;
            } else {
                return T();
            }
        }
        return m_Queue.dequeue();
    }

    void clear()
    {
        QMutexLocker lock(&m_Mutex);
        if (isPtr() && m_TakeOwnership) {
            deleteQueue(m_Queue);
        }
        m_Queue.clear();
    }

private:
    QQueue<T> m_Queue;
    QMutex m_Mutex;
    int m_QueueCapacity;
    int m_QueueCount;
    bool m_TakeOwnership;

    // pointer type test & container object deletion
    bool isPtr() { return Ptr<T>::isPtr(); }
    void deleteQueue(QQueue<T>& queue) { Ptr<T>::deleteAll(queue); }

    // specialisation for non-ptr
    template<typename t>
    struct Ptr {
        static bool isPtr() { return false; }
        static void deleteAll(QQueue<T>& queue) { Q_UNUSED(queue); }
    };

    // specialisation for ptr
    template<typename t>
    struct Ptr<t*> {
        static bool isPtr() { return true; }
        static void deleteAll(QQueue<T>& queue) { qDeleteAll(queue); }
    };

};

#endif // QKNXQUEUE_H
