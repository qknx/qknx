#include "qknxstream.h"

QKnxStream::QKnxStream(const QByteArray &data) : m_Ptr(Q_NULLPTR), m_DataPtr(Q_NULLPTR), m_Length(0), m_MaxLength(0), m_Position(0), m_Readable(false), m_Writeable(false), m_Warn(false)
{
    m_Ptr = const_cast<QByteArray*>(&data);
    m_Length = m_Ptr->length();
    m_MaxLength = m_Length;
    m_Readable = true;
    m_DataPtr = m_Ptr->data();

    m_Warn = QKNX_STREAM_WARN;
}

QKnxStream::QKnxStream(QByteArray *data_ptr, int buffer_size) : m_Ptr(data_ptr), m_DataPtr(Q_NULLPTR), m_Length(0), m_MaxLength(0), m_Position(0), m_Readable(false), m_Writeable(false), m_Warn(false)
{
    if (m_Ptr) {
        m_Length = m_Ptr->length();
        m_MaxLength = m_Length;
        m_Readable = true;
        m_Writeable = true;
        m_Position = m_Length;
        if (buffer_size > m_Length) {
            m_Ptr->resize(buffer_size);
            m_MaxLength = buffer_size;
        }
        m_DataPtr = m_Ptr->data();
    }

    m_Warn = QKNX_STREAM_WARN;
}

QKnxStream::~QKnxStream()
{

}

bool QKnxStream::setByte(const int &pos, quint8 value)
{
    if (pos == m_Position) {
        *this << value;
        return true;
    }
    if (pos < m_Position) {
        m_DataPtr[pos] = value;
        return true;
    }
    return false;
}

bool QKnxStream::isLittleEndianSystem()
{
    quint32 number = 0x01;
    char *numPtr = (char*)&number;
    return (numPtr[0] == 1);
}

void QKnxStream::enlarge()
{
    m_Ptr->resize(m_MaxLength+QKnxStream::blockSize());
    m_MaxLength = m_Ptr->size();
    m_DataPtr = m_Ptr->data();
}

// char
QKnxStream &operator>>(QKnxStream &fs, char &output)
{
    if (!fs.m_Readable) {
        fs.warningReadNotAllowed();
        return fs;
    }
    if (fs.m_Position < fs.m_Length) {
        output = fs.m_DataPtr[fs.m_Position++];
    } else {
        output = 0;
    }
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const char &input)
{
    if (!fs.m_Writeable) {
        fs.warningWriteNotAllowed();
        return fs;
    }
    while (fs.m_Position >= fs.m_MaxLength) {
        fs.enlarge();
    }
    fs.m_DataPtr[fs.m_Position++] = input;
    if (fs.m_Position > fs.m_Length) fs.m_Length = fs.m_Position;
    return fs;
}

// 8 bit integer - unsigned
QKnxStream &operator>>(QKnxStream &fs, quint8 &output)
{
    if (!fs.m_Readable) {
        fs.warningReadNotAllowed();
        return fs;
    }
    if (fs.m_Position < fs.m_Length) {
        output = fs.m_DataPtr[fs.m_Position++];
    } else {
        output = 0;
    }
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const quint8 &input)
{
    if (!fs.m_Writeable) {
        fs.warningWriteNotAllowed();
        return fs;
    }
    while (fs.m_Position >= fs.m_MaxLength) {
        fs.enlarge();
    }
    fs.m_DataPtr[fs.m_Position++] = input;
    if (fs.m_Position > fs.m_Length) fs.m_Length = fs.m_Position;
    return fs;
}

// 8 bit integer - signed
QKnxStream &operator>>(QKnxStream &fs, qint8 &output)
{
    if (!fs.m_Readable) {
        fs.warningReadNotAllowed();
        return fs;
    }
    if (fs.m_Position < fs.m_Length) {
        output = fs.m_DataPtr[fs.m_Position++];
    } else {
        output = 0;
    }
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const qint8 &input)
{
    if (!fs.m_Writeable) {
        fs.warningWriteNotAllowed();
        return fs;
    }
    while (fs.m_Position >= fs.m_MaxLength) {
        fs.enlarge();
    }
    fs.m_DataPtr[fs.m_Position++] = input;
    if (fs.m_Position > fs.m_Length) fs.m_Length = fs.m_Position;
    return fs;
}

// 16 bit integer - unsigned
QKnxStream &operator>>(QKnxStream &fs, quint16 &output)
{
    if (!fs.m_Readable) {
        fs.warningReadNotAllowed();
        return fs;
    }
    if (fs.m_Position+1 < fs.m_Length) {
        output = (quint16)(fs.m_DataPtr[fs.m_Position++] << 8) & 0xFF00;
        output |= (quint16)fs.m_DataPtr[fs.m_Position++] & 0xFF;
    } else {
        output = 0;
    }
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const quint16 &input)
{
    if (!fs.m_Writeable) {
        fs.warningWriteNotAllowed();
        return fs;
    }
    while (fs.m_Position+1 >= fs.m_MaxLength) {
        fs.enlarge();
    }
    fs.m_DataPtr[fs.m_Position++] = input >> 8;
    fs.m_DataPtr[fs.m_Position++] = input;
    if (fs.m_Position > fs.m_Length) fs.m_Length = fs.m_Position;
    return fs;
}

// 16 bit integer - signed
QKnxStream &operator>>(QKnxStream &fs, qint16 &output)
{
    if (!fs.m_Readable) {
        fs.warningReadNotAllowed();
        return fs;
    }
    if (fs.m_Position+1 < fs.m_Length) {
        output = (qint16)(fs.m_DataPtr[fs.m_Position++] << 8) & 0xFF00;
        output |= (qint16)fs.m_DataPtr[fs.m_Position++] & 0xFF;
    } else {
        output = 0;
    }
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const qint16 &input)
{
    if (!fs.m_Writeable) {
        fs.warningWriteNotAllowed();
        return fs;
    }
    while (fs.m_Position+1 >= fs.m_MaxLength) {
        fs.enlarge();
    }
    fs.m_DataPtr[fs.m_Position++] = input >> 8;
    fs.m_DataPtr[fs.m_Position++] = input;
    if (fs.m_Position > fs.m_Length) fs.m_Length = fs.m_Position;
    return fs;
}

// 32 bit integer - unsigned
QKnxStream &operator>>(QKnxStream &fs, quint32 &output)
{
    if (!fs.m_Readable) {
        fs.warningReadNotAllowed();
        return fs;
    }
    if (fs.m_Position+3 < fs.m_Length) {
        output = (quint32)(fs.m_DataPtr[fs.m_Position++] << 24) & 0xFF000000;
        output |= (quint32)(fs.m_DataPtr[fs.m_Position++] << 16) & 0xFF0000;
        output |= (quint32)(fs.m_DataPtr[fs.m_Position++] << 8) & 0xFF00;
        output |= (quint32)fs.m_DataPtr[fs.m_Position++] & 0xFF;
    } else {
        output = 0;
    }
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const quint32 &input)
{
    if (!fs.m_Writeable) {
        fs.warningWriteNotAllowed();
        return fs;
    }
    while (fs.m_Position+3 >= fs.m_MaxLength) {
        fs.enlarge();
    }
    fs.m_DataPtr[fs.m_Position++] = input >> 24;
    fs.m_DataPtr[fs.m_Position++] = input >> 16;
    fs.m_DataPtr[fs.m_Position++] = input >> 8;
    fs.m_DataPtr[fs.m_Position++] = input >> 0;
    if (fs.m_Position > fs.m_Length) fs.m_Length = fs.m_Position;
    return fs;
}

// 32 bit integer - signed
QKnxStream &operator>>(QKnxStream &fs, qint32 &output)
{
    if (!fs.m_Readable) {
        fs.warningReadNotAllowed();
        return fs;
    }
    if (fs.m_Position+3 < fs.m_Length) {
        output = (qint32)(fs.m_DataPtr[fs.m_Position++] << 24) & 0xFF000000;
        output |= (qint32)(fs.m_DataPtr[fs.m_Position++] << 16) & 0xFF0000;
        output |= (qint32)(fs.m_DataPtr[fs.m_Position++] << 8) & 0xFF00;
        output |= (qint32)fs.m_DataPtr[fs.m_Position++] & 0xFF;
    } else {
        output = 0;
    }
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const qint32 &input)
{
    if (!fs.m_Writeable) {
        fs.warningWriteNotAllowed();
        return fs;
    }
    while (fs.m_Position+3 >= fs.m_MaxLength) {
        fs.enlarge();
    }
    fs.m_DataPtr[fs.m_Position++] = input >> 24;
    fs.m_DataPtr[fs.m_Position++] = input >> 16;
    fs.m_DataPtr[fs.m_Position++] = input >> 8;
    fs.m_DataPtr[fs.m_Position++] = input >> 0;
    if (fs.m_Position > fs.m_Length) fs.m_Length = fs.m_Position;
    return fs;
}

// 32 bit float
QKnxStream &operator>>(QKnxStream &fs, float &output)
{
    QKnxStream::float_union x;
    if (QKnxStream::isLittleEndianSystem()) {
        fs >> x.c[3] >> x.c[2] >> x.c[1] >> x.c[0];
    } else {
        fs >> x.c[0] >> x.c[1] >> x.c[2] >> x.c[3];
    }
    output = x.f;
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const float &input)
{
    QKnxStream::float_union x;
    x.f = input;
    if (QKnxStream::isLittleEndianSystem()) {
        fs << x.c[3] << x.c[2] << x.c[1] << x.c[0];
    } else {
        fs << x.c[0] << x.c[1] << x.c[2] << x.c[3];
    }
    return fs;
}

// bytearray
QKnxStream &operator>>(QKnxStream &fs, QByteArray &output)
{
    if (!fs.m_Readable) {
        fs.warningReadNotAllowed();
        return fs;
    }
    int write_bytes = output.length();
    int current_byte = 0;
    char* out = output.data();
    while (current_byte < write_bytes) {
        if (fs.m_Position < fs.m_Length) {
            out[current_byte++] = fs.m_DataPtr[fs.m_Position++];
        } else {
            out[current_byte++] = 0x00;
        }
    }
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const QByteArray &input)
{
    if (!fs.m_Writeable) {
        fs.warningWriteNotAllowed();
        return fs;
    }
    int read_bytes = input.length();
    while (fs.m_Position+read_bytes >= fs.m_MaxLength) {
        fs.enlarge();
    }

    int current_byte = 0;
    const char* in = input.data();
    while (current_byte < read_bytes) {
        fs.m_DataPtr[fs.m_Position++] = in[current_byte++];
    }
    if (fs.m_Position > fs.m_Length) fs.m_Length = fs.m_Position;
    return fs;
}
