#ifndef QKNXFRAMESTREAM_H
#define QKNXFRAMESTREAM_H

#include "qknxglobal.h"

#define QKNX_STREAM_BLOCK_SIZE 256
#define QKNX_STREAM_WARN false;

class QKNX_SHARED_EXPORT QKnxStream
{
public:
    explicit QKnxStream(const QByteArray& data);
    explicit QKnxStream(QByteArray* data_ptr, int buffer_size = QKNX_STREAM_BLOCK_SIZE);
    ~QKnxStream();

    int length() { return m_Length; }
    bool atEnd() { return (m_Position == m_Length); }
    bool error() { return (m_Position > m_Length); }

    int position() { return m_Position; }
    bool setPosition(const int& pos) { m_Position = pos; return (m_Position <= m_Length); }

    bool setByte(const int& pos, quint8 value);
    bool setByte(const int& pos, qint8 value) { return setByte(pos,(quint8)value); }

    QByteArray* dataPtr() { return m_Ptr; }
    QByteArray* finishWrite() { m_Ptr->resize(m_Length); return m_Ptr; }

    // endianess check
    static bool isLittleEndianSystem();

private:
    QByteArray* m_Ptr;
    char* m_DataPtr;
    int m_Length;
    int m_MaxLength;
    int m_Position;

    bool m_Readable;
    bool m_Writeable;
    bool m_Warn;

    union float_union {
        float f;
        char c[4];
    };

    static int blockSize() { return QKNX_STREAM_BLOCK_SIZE; }
    void enlarge();

    // warning if reading/writing not allowed
    void warningReadNotAllowed() { if (m_Warn) qDebug() << "Warning: reading not allowed!"; }
    void warningWriteNotAllowed() { if (m_Warn) qDebug() << "Warning: writing not allowed!"; }

    // char
    friend QKnxStream& operator>>(QKnxStream& fs, char& output);
    friend QKnxStream& operator<<(QKnxStream& fs, const char& input);

    // 8 bit integer - unsigned
    friend QKnxStream& operator>>(QKnxStream& fs, quint8& output);
    friend QKnxStream& operator<<(QKnxStream& fs, const quint8& input);

    // 8 bit integer - signed
    friend QKnxStream& operator>>(QKnxStream& fs, qint8& output);
    friend QKnxStream& operator<<(QKnxStream& fs, const qint8& input);

    // 16 bit integer - unsigned
    friend QKnxStream& operator>>(QKnxStream& fs, quint16& output);
    friend QKnxStream& operator<<(QKnxStream& fs, const quint16& input);

    // 16 bit integer - signed
    friend QKnxStream& operator>>(QKnxStream& fs, qint16& output);
    friend QKnxStream& operator<<(QKnxStream& fs, const qint16& input);

    // 32 bit integer - unsigned
    friend QKnxStream& operator>>(QKnxStream& fs, quint32& output);
    friend QKnxStream& operator<<(QKnxStream& fs, const quint32& input);

    // 32 bit integer - signed
    friend QKnxStream& operator>>(QKnxStream& fs, qint32& output);
    friend QKnxStream& operator<<(QKnxStream& fs, const qint32& input);

    // 32 bit float
    friend QKnxStream& operator>>(QKnxStream& fs, float& output);
    friend QKnxStream& operator<<(QKnxStream& fs, const float& input);

    // byte array
    friend QKnxStream& operator>>(QKnxStream& fs, QByteArray& output);
    friend QKnxStream& operator<<(QKnxStream& fs, const QByteArray& input);

};

// char
QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, char& output);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const char& input);

// 8 bit integer - unsigned
QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, quint8& output);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const quint8& input);

// 8 bit integer - signed
QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, qint8& output);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const qint8& input);

// 16 bit integer - unsigned
QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, quint16& output);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const quint16& input);

// 16 bit integer - signed
QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, qint16& output);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const qint16& input);

// 32 bit integer - unsigned
QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, quint32& output);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const quint32& input);

// 32 bit integer - signed
QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, qint32& output);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const qint32& input);

// 32 bit float
QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, float& output);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const float& input);

// byte array
QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QByteArray& output);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const QByteArray& input);

#endif // QKNXFRAMESTREAM_H
