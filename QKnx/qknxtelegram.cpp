#include "qknxtelegram.h"

#include <QDataStream>

QKnxTelegram::QKnxTelegram() : m_CemiIO(false), m_FrameType(FrameTypeDefault), m_Repeated(RepeatedFalse), m_Broadcast(BroadcastNormal), m_Priority(PriorityLow),
                                    m_AckRequest(false), m_Error(false), m_RoutingCount(RoutingDefault), m_LTExt(LTExtOff), m_Command(CommandRead)
{
    // set address type
    m_Source.setFormat(QKnxAddress::FormatPhysical);
}

QKnxTelegram::QKnxTelegram(const QByteArray &data, bool cemi_io) : m_CemiIO(cemi_io), m_FrameType(FrameTypeDefault), m_Repeated(RepeatedFalse), m_Broadcast(BroadcastNormal), m_Priority(PriorityLow),
    m_AckRequest(false), m_Error(false), m_RoutingCount(RoutingDefault), m_LTExt(LTExtOff), m_Command(CommandRead)
{
    // set address type
    m_Source.setFormat(QKnxAddress::FormatPhysical);

    // set data
    setTelegram(data);
}

QKnxTelegram::~QKnxTelegram()
{

}

bool QKnxTelegram::setTelegram(const QByteArray &data, bool cemi_io)
{
    m_CemiIO = cemi_io;
    QKnxStream fs(data);
    fs >> *this;
    return isValid();
}

QByteArray QKnxTelegram::toByteArray(bool cemi_io)
{
    m_CemiIO = cemi_io;
    QByteArray ret_value;
    QKnxStream fs(&ret_value,300);
    fs << *this;
    fs.finishWrite();
    return ret_value;
}

/*
bool QKnxTelegram::setTelegram(const QByteArray &tg, bool is_cemi)
{
    QDataStream ds(tg);
    ds.setByteOrder(QDataStream::BigEndian);
    return setTelegram(ds,is_cemi);
}

bool QKnxTelegram::setTelegram(QDataStream &ds, bool is_cemi)
{
    // Control byte
    quint8 byte_ctrl;
    ds >> byte_ctrl;
    m_FrameType = (FrameType)((byte_ctrl & 0b10000000) >> 7);
    m_Repeated = (Repeated)((byte_ctrl & 0b00100000) >> 5);
    m_Broadcast = (Broadcast)((byte_ctrl & 0b00010000) >> 4);
    m_Priority = (Priority)((byte_ctrl & 0b00001100) >> 2);
    m_AckRequest = (bool)((byte_ctrl & 0b00000010) >> 1);
    m_Error = (bool)((byte_ctrl & 0b00000001) >> 0);

    // Extended Control byte
    if (isFrameTypeExtended() || is_cemi) {
        quint8 byte_ctrl2;
        ds >> byte_ctrl2;
        m_Target.setDAF((bool)((byte_ctrl2 & 0b10000000) >> 7));
        m_RoutingCount = (RoutingCount)((byte_ctrl2 & 0b01110000) >> 4);
        m_LTExt = (LTExt)((byte_ctrl2 & 0b00001111) >> 0);
    } else {
        m_LTExt = LTExtOff;
    }

    // Source address
    m_Source.setPhysicalAddress();
    m_Source.setAddress(ds);
    //current_byte += 2;

    // Target address
    m_Target.setAddress(ds);
    //current_byte += 2;

    // DRL byte
    quint8 content_len, byte_drl;
    ds >> byte_drl;
    if (isFrameTypeDefault() && !is_cemi) {
        m_Target.setDAF((bool)((byte_drl & 0b10000000) >> 7));
        m_RoutingCount = (RoutingCount)((byte_drl & 0b01110000) >> 4);
        content_len = byte_drl & 0b00001111;
    } else {
        content_len = byte_drl;
    }

    // TPCI/APCI bytes
    m_Content.clear();
    quint16 byte_tpci_apci;
    ds >> byte_tpci_apci;
    m_Command = (Command)((byte_tpci_apci & 0b0000001111000000) >> 6);
    if (content_len == 1) {
        m_Content.append((quint8)(byte_tpci_apci & 0b00111111));
    }

    if (isLTExt()) {
        // TODO: LTE-HEE addressing
    } else {
        if (content_len > 1) {
            m_Content.resize(content_len-1);
            ds.readRawData(m_Content.data(),m_Content.length());
            m_Content.prepend((char)0x00);
        }
    }

    if (ds.status() != QDataStream::Ok || !ds.atEnd()) {
        return false;
    }
    return true;
}

QByteArray QKnxTelegram::toByteArray(bool is_cemi) const
{
    QByteArray data;
    QDataStream ds(&data,QIODevice::ReadWrite);
    ds.setByteOrder(QDataStream::BigEndian);

    if (!toDataStream(ds,is_cemi)) {
        return QByteArray();
    }
    return data;
}

bool QKnxTelegram::toDataStream(QDataStream &ds, bool is_cemi) const
{
    if (!isValid()) {
        return false;
    }

    quint8 content_len = contentLength();

    // Control byte
    quint8 byte_ctrl = 0;
        byte_ctrl |= (((quint8)m_FrameType) << 7) & 0b10000000;
        byte_ctrl |= (((quint8)m_Repeated) << 5) & 0b00100000;
        byte_ctrl |= (((quint8)m_Broadcast) << 4) & 0b00010000;
        byte_ctrl |= (((quint8)m_Priority) << 2) & 0b00001100;
        // only extended/udp??
        byte_ctrl |= (((quint8)m_AckRequest) << 1) & 0b00000010;
        byte_ctrl |= (((quint8)m_Error) << 0) & 0b00000001;
    ds << byte_ctrl;

    // Extended control byte
    if (isFrameTypeExtended() || is_cemi) {
        quint8 byte_ctrl2 = 0;
            byte_ctrl2 |= (((quint8)m_Target.daf()) << 7) & 0b10000000;
            byte_ctrl2 |= (((quint8)m_RoutingCount) << 4) & 0b01110000;
            byte_ctrl2 |= (((quint8)m_LTExt) << 0) & 0b00001111;
        ds << byte_ctrl2;
    }

    // Source and destination address
    ds << m_Source.address();
    ds << m_Target.address();

    // DRL byte
    quint8 byte_drl = 0;
    if (isFrameTypeExtended() || is_cemi) {
        byte_drl |= content_len;
    } else {
        byte_drl |= (((quint8)m_Target.daf()) << 7) & 0b10000000;
        byte_drl |= (((quint8)m_RoutingCount) << 4) & 0b01110000;
        if (content_len > 15) {
            qDebug() << "Content length" << content_len << "too large for default frame type!";
        }
        byte_drl |= content_len & 0b00001111;
    }
    ds << byte_drl;

    // TPCI/APCI bytes
    quint16 byte_tpci_apci = 0;
    byte_tpci_apci |= (((quint16)m_Command) << 6) & 0b0000001111000000;
    if (content_len == 1) {
        byte_tpci_apci |= (quint16)m_Content.at(0) & 0b00111111 ;
    }
    ds << byte_tpci_apci;

    if (isLTExt()) {
        // TODO: LTE-HEE addressing
    } else {
        if (content_len > 1) {
            ds.writeRawData(m_Content.right(content_len-1),content_len-1);
        }
    }

    if (ds.status() != QDataStream::Ok) {
        return false;
    }
    return true;
}
*/

bool QKnxTelegram::isValid() const
{
    if (!m_Source.isValid() || !m_Source.isPhysicalAddress() || !m_Target.isValid())
    {
        return false;
    }
    if (m_Content.isEmpty())
    {
        return false;
    }
    //if (m_MessageCode == L_Data_auto) ret = false;
    return true;
}

quint8 QKnxTelegram::calculateChecksum(const QByteArray& data)
{
    quint8 bcc = 0xFF;
    for(int i=0; i < data.size(); i++) {
        bcc ^= data.at(i);
    }
    return bcc;
}


QKnxStream &operator>>(QKnxStream &fs, QKnxTelegram &data)
{
    // Control byte
    quint8 byte_ctrl;
    fs >> byte_ctrl;
    data.m_FrameType = (QKnxTelegram::FrameType)((byte_ctrl & 0b10000000) >> 7);
    data.m_Repeated = (QKnxTelegram::Repeated)((byte_ctrl & 0b00100000) >> 5);
    data.m_Broadcast = (QKnxTelegram::Broadcast)((byte_ctrl & 0b00010000) >> 4);
    data.m_Priority = (QKnxTelegram::Priority)((byte_ctrl & 0b00001100) >> 2);
    data.m_AckRequest = (bool)((byte_ctrl & 0b00000010) >> 1);
    data.m_Error = (bool)((byte_ctrl & 0b00000001) >> 0);

    // Extended Control byte
    if (data.isFrameTypeExtended() || data.m_CemiIO) {
        quint8 byte_ctrl2;
        fs >> byte_ctrl2;
        data.m_Target.setDAF((bool)((byte_ctrl2 & 0b10000000) >> 7));
        data.m_RoutingCount = (byte_ctrl2 & 0b01110000) >> 4;
        data.m_LTExt = (QKnxTelegram::LTExt)((byte_ctrl2 & 0b00001111) >> 0);
    } else {
        data.m_LTExt = QKnxTelegram::LTExtOff;
    }

    // Source address
    data.m_Source.setPhysicalAddress();
    fs >> data.m_Source;
    //current_byte += 2;

    // Target address
    fs >> data.m_Target;
    //current_byte += 2;

    // DRL byte
    quint8 content_len, byte_drl;
    fs >> byte_drl;
    if (data.isFrameTypeDefault() && !data.m_CemiIO) {
        data.m_Target.setDAF((bool)((byte_drl & 0b10000000) >> 7));
        data.m_RoutingCount = (byte_drl & 0b01110000) >> 4;
        content_len = byte_drl & 0b00001111;
    } else {
        content_len = byte_drl;
    }

    // TPCI/APCI bytes
    data.m_Content.clear();
    quint16 byte_tpci_apci;
    fs >> byte_tpci_apci;
    data.m_Command = (QKnxTelegram::Command)((byte_tpci_apci & 0b0000001111000000) >> 6);

    if (data.isLTExt()) {
        // TODO: LTE-HEE addressing
    } else {
        if (content_len) {
            data.m_Content.resize(content_len);
            if (content_len == 1) {
                data.m_Content[0] = ((quint8)(byte_tpci_apci & 0b00111111));
            } else {
                fs.setPosition(fs.position()-1);
                fs >> data.m_Content;
                data.m_Content[0] = 0x00;
            }
        }
    }

    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const QKnxTelegram &data)
{
    if (!data.isValid()) {
        return fs;
    }

    quint8 content_len = data.contentLength();

    // Control byte
    quint8 byte_ctrl = 0;
        byte_ctrl |= (((quint8)data.m_FrameType) << 7) & 0b10000000;
        byte_ctrl |= (((quint8)data.m_Repeated) << 5) & 0b00100000;
        byte_ctrl |= (((quint8)data.m_Broadcast) << 4) & 0b00010000;
        byte_ctrl |= (((quint8)data.m_Priority) << 2) & 0b00001100;
        // only extended/udp??
        byte_ctrl |= (((quint8)data.m_AckRequest) << 1) & 0b00000010;
        byte_ctrl |= (((quint8)data.m_Error) << 0) & 0b00000001;
    fs << byte_ctrl;

    // Extended control byte
    if (data.isFrameTypeExtended() || data.m_CemiIO) {
        quint8 byte_ctrl2 = 0;
            byte_ctrl2 |= (((quint8)data.m_Target.daf()) << 7) & 0b10000000;
            byte_ctrl2 |= (data.m_RoutingCount << 4) & 0b01110000;
            byte_ctrl2 |= (((quint8)data.m_LTExt) << 0) & 0b00001111;
        fs << byte_ctrl2;
    }

    // Source and destination address
    fs << data.m_Source;
    fs << data.m_Target;

    // DRL byte
    quint8 byte_drl = 0;
    if (data.isFrameTypeExtended() || data.m_CemiIO) {
        byte_drl |= content_len;
    } else {
        byte_drl |= (((quint8)data.m_Target.daf()) << 7) & 0b10000000;
        byte_drl |= (data.m_RoutingCount << 4) & 0b01110000;
        if (content_len > 15) {
            qDebug() << "Content length" << content_len << "too large for default frame type!";
        }
        byte_drl |= content_len & 0b00001111;
    }
    fs << byte_drl;

    // TPCI/APCI bytes
    quint16 byte_tpci_apci = 0;
    byte_tpci_apci |= (((quint16)data.m_Command) << 6) & 0b0000001111000000;
    if (content_len == 1) {
        byte_tpci_apci |= (quint16)data.m_Content[0] & 0b00111111 ;
    }
    fs << byte_tpci_apci;

    if (data.isLTExt()) {
        // TODO: LTE-HEE addressing
    } else {
        if (content_len > 1) {
            // apci will be overwritten, recover it after streaming
            int apci_pos = fs.position()-1;
            fs.setPosition(apci_pos);
            fs << data.m_Content;
            fs.setByte(apci_pos,(quint8)byte_tpci_apci);
        }
    }

    return fs;
}
