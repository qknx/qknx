#ifndef QKNXTELEGRAM_H
#define QKNXTELEGRAM_H

#include "qknxglobal.h"
#include "qknxaddress.h"
#include "qknxstream.h"
#include "dpt/qknxdpt.h"

#include <QObject>
#include <QDebug>
#include <QMetaEnum>

class QKNX_SHARED_EXPORT QKnxTelegram
{
    Q_GADGET
public:
    enum FrameType {
        FrameTypeExtended = 0b00,
        FrameTypeDefault = 0b01
    };
    Q_ENUM(FrameType)

    enum Repeated {
        RepeatedTrue = 0b00,
        RepeatedFalse = 0b01
    };
    Q_ENUM(Repeated)

    enum Broadcast {
        BroadcastSystem = 0b00,
        BroadcastNormal = 0b01   // for KNXnetIP always 1
    };
    Q_ENUM(Broadcast)

    enum Priority {
        PrioritySystem = 0b00,
        PriorityAlarm = 0b10,
        PriorityHigh = 0b01,
        PriorityLow = 0b11
    };
    Q_ENUM(Priority)

    enum RoutingCount {
        RoutingDrop = 0b000,
        RoutingDefault = 0b110,
        RoutingInfinite = 0b111
    };
    Q_ENUM(RoutingCount)

    enum LTExt {
        LTExtOff = 0b0000,
        LTExtLevel0To63 = 0b0100,
        LTExtLevel64To128 = 0b0101,
        LTExtAppSpecificZone = 0b0110,
        LTExtPeripheralZones = 0b0111
    };
    Q_ENUM(LTExt)

    enum Command {
        CommandRead = 0b0000,
        CommandWrite = 0b0010,
        CommandResponse = 0b0001,
        CommandIndividualWrite = 0b0011,
        CommandIndividualRead = 0b0100,
        CommandIndividualResponse = 0b0101,
        CommandMaskVersionRead = 0b1100,
        CommandMaskVersionResponse = 0b1101,
        CommandRestart = 0b1110,
        CommandEscape = 0b1111
    };
    Q_ENUM(Command)

    enum Confirm {
        ConfirmACK =  0b11001100,
        ConfirmBUSY = 0b11000000,
        ConfirmNACK = 0b00001100
    };
    Q_ENUM(Confirm)

    explicit QKnxTelegram();
    explicit QKnxTelegram(const QByteArray& data, bool cemi_io);
    virtual ~QKnxTelegram();

    // import/export
    bool setTelegram(const QByteArray& data) { return setTelegram(data,m_CemiIO); }
    bool setTelegram(const QByteArray &data, bool cemi_io);
    static QKnxTelegram fromByteArray(const QByteArray& data, bool cemi_io) { return QKnxTelegram(data,cemi_io); }
    QByteArray toByteArray() { return toByteArray(m_CemiIO); }
    QByteArray toByteArray(bool cemi_io);

    // set cemi input/output
    bool cemiIO() const { return m_CemiIO; }
    void setCemiIO(const bool& cemi_io) { m_CemiIO = cemi_io; }

    QKnxAddress source() const { return m_Source; }
    void setSource(const QKnxAddress &src) { m_Source = src; }
    void setSource(const QString &src) { m_Source = QKnxAddress::fromString(src); }
    void setSource(const char* src) { setSource(QString(src)); }
    void setSource(const quint16& src) { m_Source.setPhysicalAddress(); m_Source.setAddress(src); }

    QKnxAddress target() const { return m_Target; }
    void setTarget(const QKnxAddress &target) { m_Target = target; }
    void setTarget(const QString &target) { m_Target = QKnxAddress::fromString(target); }
    void setTarget(const char* target) { setTarget(QString(target)); }
    void setTarget(const quint16& target) { m_Target.setGroupAddress(); m_Target.setAddress(target); }

    FrameType frameType() const { return m_FrameType; }
    QString frameTypeString() const { return frameTypeString(m_FrameType); }
    static QString frameTypeString(const FrameType& frame_type) { return QMetaEnum::fromType<FrameType>().valueToKey(frame_type); }
    bool isFrameTypeDefault() const { return (m_FrameType == FrameTypeDefault); }
    bool isFrameTypeExtended() const { return (m_FrameType == FrameTypeExtended); }
    void setFrameType(const FrameType &frametype) { m_FrameType = frametype; }

    Repeated repeated() const { return m_Repeated; }
    QString repeatedString() const { return repeatedString(m_Repeated); }
    static QString repeatedString(const Repeated& repeated ) { return QMetaEnum::fromType<Repeated>().valueToKey(repeated); }
    bool isFirstTransmit() const { return (m_Repeated == RepeatedFalse); }
    bool isRepeated() { return (m_Repeated == RepeatedTrue); }
    void setRepeated(const Repeated &repeated) { m_Repeated = repeated; }

    Broadcast broadcast() const { return m_Broadcast; }
    QString broadcastString() const { return broadcastString(m_Broadcast);  }
    static QString broadcastString(const Broadcast& broadcast) { return QMetaEnum::fromType<Broadcast>().valueToKey(broadcast); }
    bool isBroadcastSystem() const { return (m_Broadcast == BroadcastSystem); }
    bool isBroadcastNormal() const { return (m_Broadcast == BroadcastNormal); }
    void setBroadcast(const Broadcast &broadcast) { m_Broadcast = broadcast; }

    Priority priority() const { return m_Priority; }
    QString priorityString() const { return priorityString(m_Priority); }
    static QString priorityString(const Priority& prio) { return QMetaEnum::fromType<Priority>().valueToKey(prio); }
    void setPriority(const Priority &priority) { m_Priority = priority; }

    bool ackRequest() const { return m_AckRequest; }
    void setAckRequest(const bool &ackrequest) { m_AckRequest = ackrequest; }

    bool error() const { return m_Error; }
    void setError(const bool& error) { m_Error = error; }

    quint8 routingCount() const { return m_RoutingCount; }
    bool isRoutingValid() const { return (m_RoutingCount > RoutingDrop && m_RoutingCount <= RoutingInfinite ); }
    bool isRoutingDrop() const { return (m_RoutingCount == RoutingDrop); }
    bool isRoutingInfinite() const { return (m_RoutingCount == RoutingInfinite); }
    void setRoutingCount(const RoutingCount &routingcount) { m_RoutingCount = routingcount; }
    void setRoutingCount(const quint8 &routingcount) { m_RoutingCount = routingcount; }
    quint8 routingCountIncrease() { if (m_RoutingCount < RoutingInfinite) m_RoutingCount++; return m_RoutingCount; }
    quint8 routingCountDecrease() { if (m_RoutingCount > RoutingDrop) m_RoutingCount--; return m_RoutingCount; }

    LTExt ltext() const { return m_LTExt; }
    QString ltextString() const { return ltextString(m_LTExt); }
    static QString ltextString(const LTExt& ltext) { return QMetaEnum::fromType<LTExt>().valueToKey(ltext); }
    bool isLTExt() const { return !(m_LTExt == LTExtOff); }
    void setLTExt(const LTExt &ltext) { m_LTExt = ltext; }

    Command command() const { return m_Command; }
    QString commandString() { return commandString(m_Command); }
    static QString commandString(const Command& cmd) { return QMetaEnum::fromType<Command>().valueToKey(cmd); }
    bool isCommandRead() const { return (m_Command == CommandRead); }
    bool isCommandResponse() const { return (m_Command == CommandResponse); }
    bool isCommandWrite() const { return (m_Command == CommandWrite); }
    void setCommand(const Command &cmd) { m_Command = cmd; }

    virtual QByteArray content() const { return m_Content; }
    virtual void setContent(const QByteArray &content) { m_Content = content; }
    virtual quint8 contentLength() const { return m_Content.length(); }
    virtual bool isValid() const;

    static quint8 calculateChecksum(const QByteArray &data);

private:
    // set cemi (=true) or default frame (=false)
    bool m_CemiIO;

    QKnxAddress m_Source;
    QKnxAddress m_Target;

    // Control Byte
    FrameType m_FrameType;
    Repeated m_Repeated;
    Broadcast m_Broadcast;
    Priority m_Priority;
    bool m_AckRequest;
    bool m_Error;

    // Additional headers
    quint8 m_RoutingCount;
    //RoutingCount m_RoutingCount;
    LTExt m_LTExt;
    Command m_Command;

    // Content
    QByteArray m_Content;

    friend QKnxStream& operator>>(QKnxStream& fs, QKnxTelegram& data);
    friend QKnxStream& operator<<(QKnxStream& fs, const QKnxTelegram& data);
};

QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QKnxTelegram& data);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const QKnxTelegram& data);

#endif // QKNXTELEGRAM_H
