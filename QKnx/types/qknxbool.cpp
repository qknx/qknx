#include "qknxbool.h"

#include "qknxstream.h"

static bool register_knxtype = QKnxDpt::registerType<QKnxBool>(&register_knxtype);

template<> QKnxDpt::Type QKnxDpt::type<QKnxBool>() { return QKnxDpt::TypeBool; }

QKnxStream &operator>>(QKnxStream &fs, QKnxBool &data)
{
    quint8 in;
    fs >> in;
    data.m_Bool = (bool)(in & 0b01);
    data.m_Control = (in & 0b10) ? QKnxBool::ControlTrue : QKnxBool::ControlFalse;
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const QKnxBool &data)
{
    quint8 out = (quint8)data.m_Bool;
    if (data.m_Control == QKnxBool::ControlTrue) {
        out |= 0b10;
    }
    fs << out;
    return fs;
}

QDebug operator<<(QDebug dbg, const QKnxBool &data)
{
    dbg.nospace() << data.m_Bool << ", " << data.m_Control;
    return dbg;
}
