#ifndef QKNXBOOL_H
#define QKNXBOOL_H

#include "dpt/qknxdpt.h"

class QKNX_SHARED_EXPORT QKnxBool : public QKnxDpt::ValueTypeCRTP<QKnxBool>
{
    Q_GADGET
public:
    enum Control {
        ControlFalse = 0,
        ControlTrue = 1
    };
    Q_ENUM(Control)

    QKnxBool(bool value = false, Control control = ControlFalse) : QKnxDpt::ValueTypeCRTP<QKnxBool>(), m_Bool(value), m_Control(control) {}
    virtual ~QKnxBool() {}

    operator QVariant() const { return QVariant::fromValue<QKnxBool>(*this); }

    bool boolValue() const { return m_Bool; }
    void setBoolValue(const bool& value) { m_Bool = value; }
    operator bool() const { return m_Bool; }
    QKnxBool& operator =(const bool& value) { m_Bool = value; return *this; }

    Control control() const { return m_Control; }
    void setControl(const Control& control) { m_Control = control; }
    operator Control() const { return m_Control; }
    QKnxBool& operator =(const Control& control) { m_Control = control; return *this; }

    static bool minBool() { return false; }
    static bool maxBool() { return true; }

    static Control minControl() { return ControlFalse; }
    static Control maxControl() { return ControlTrue; }

    static QKnxBool min() { return QKnxBool(false, ControlFalse); }
    static QKnxBool max() { return QKnxBool(true, ControlTrue); }

    bool isValid() const { return (m_Control >= ControlFalse && m_Control <= ControlTrue); }

private:
    bool m_Bool;
    Control m_Control;

    friend QKnxStream& operator>>(QKnxStream& fs, QKnxBool& data);
    friend QKnxStream& operator<<(QKnxStream& fs, const QKnxBool& data);

    friend QDebug operator<<(QDebug dbg, const QKnxBool &data);
};

template <> QKNX_SHARED_EXPORT QKnxDpt::Type QKnxDpt::type<QKnxBool>();

QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QKnxBool& data);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const QKnxBool& data);

QKNX_SHARED_EXPORT QDebug operator<<(QDebug dbg, const QKnxBool &data);

#endif // QKNXBOOL_H
