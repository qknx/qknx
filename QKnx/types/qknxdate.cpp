#include "qknxdate.h"

#include "qknxstream.h"

static bool register_knxtype = QKnxDpt::registerType<QKnxDate>(&register_knxtype);

template<> QKnxDpt::Type QKnxDpt::type<QKnxDate>() { return QKnxDpt::Type3ByteDate; }

int QKnxDate::knxYearToYear(int knx_year)
{
    if (knx_year > 99) {
         qDebug() << "value exceeds limits";
         return -1;
     }
     if (knx_year >= 90) {
         knx_year += 1900;
     } else {
         knx_year += 2000;
     }
     return knx_year;
}

int QKnxDate::yearToKnxYear(int year)
{
    if (year >= 1990 && year <= 2089) {
        if (year < 2000) {
            year -= 1900;
        } else {
            year -= 2000;
        }
    } else {
        return -1;
    }
    return year;
}

QKnxStream &operator>>(QKnxStream &fs, QKnxDate &data)
{
    quint8 day, month, year;
    fs >> day >> month >> year;
    data.m_Date.setDate(data.knxYearToYear(year & 0b01111111), month & 0b00001111, day & 0b00011111);
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const QKnxDate &data)
{
    quint8 day, month, year;
    day = (quint8)data.m_Date.day() & 0b00011111;
    month = (quint8)data.m_Date.month() & 0b00001111;
    year = (quint8)data.yearToKnxYear(data.m_Date.year()) & 0b01111111;
    fs << day << month << year;
    return fs;
}

QDebug operator<<(QDebug dbg, const QKnxDate &data)
{
    dbg.nospace() << data.m_Date;
    return dbg;
}

