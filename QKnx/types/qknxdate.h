#ifndef QKNXDATE_H
#define QKNXDATE_H

#include "dpt/qknxdpt.h"

#include <QDate>

class QKNX_SHARED_EXPORT QKnxDate : public QKnxDpt::ValueTypeCRTP<QKnxDate>
{
    Q_GADGET
public:
    QKnxDate() : QKnxDpt::ValueTypeCRTP<QKnxDate>(), m_Date() {}
    QKnxDate(const QDate& date) : QKnxDpt::ValueTypeCRTP<QKnxDate>(), m_Date(date) {}
    virtual ~QKnxDate() {}

    operator QVariant() const { return QVariant::fromValue<QKnxDate>(*this); }

    QDate date() const { return m_Date; }
    void setDate(const QDate& date) { m_Date = date; }
    operator QDate() const { return m_Date; }
    QKnxDate& operator =(const QDate& date) { m_Date = date; return *this; }

    static QDate minDate() { return QDate(1990,1,1); }
    static QDate maxDate() { return QDate(2089,12,31); }

    static QKnxDate min() { return QKnxDate(minDate()); }
    static QKnxDate max() { return QKnxDate(maxDate()); }

    bool isValid() const { return (m_Date.isValid() && m_Date >= minDate() && m_Date <= maxDate()); }

private:
    QDate m_Date;

    static int knxYearToYear(int knx_year);
    static int yearToKnxYear(int year);

    friend QKnxStream& operator>>(QKnxStream& fs, QKnxDate& data);
    friend QKnxStream& operator<<(QKnxStream& fs, const QKnxDate& data);

    friend QDebug operator<<(QDebug dbg, const QKnxDate &data);
};

template <> QKNX_SHARED_EXPORT QKnxDpt::Type QKnxDpt::type<QKnxDate>();

QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QKnxDate& data);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const QKnxDate& data);

QKNX_SHARED_EXPORT QDebug operator<<(QDebug dbg, const QKnxDate &data);

#endif // QKNXDATE_H
