#include "qknxdatetime.h"

#include "qknxstream.h"

static bool register_knxtype = QKnxDpt::registerType<QKnxDateTime>(&register_knxtype);

bool QKnxDateTime::isValid() const
{
    if (!m_DateTime.isValid()) {
        return false;
    }
    if (m_DateTime < minDateTime() || m_DateTime > maxDateTime()) {
        return false;
    }
    if (m_DayOfWeek <= minDayOfWeek() || m_DayOfWeek >= maxDayOfWeek()) {
        return false;
    }
    return true;
}

template<> QKnxDpt::Type QKnxDpt::type<QKnxDateTime>() { return QKnxDpt::Type8ByteDateTime; }

QKnxStream &operator>>(QKnxStream &fs, QKnxDateTime &data)
{
    quint8 year, month, day, hour, minute, second;
    quint16 flags;
    fs >> year >> month >> day >> hour >> minute >> second >> flags;

    int year_ = year + 1900;
    data.m_DateTime.setDate(QDate(year_, month & 0b00001111, day & 0b00011111));
    data.m_DateTime.setTime(QTime(hour & 0b00011111, minute & 0b00111111, second & 0b00111111));
    data.m_DayOfWeek = (Qt::DayOfWeek)((hour & 0b11100000) >> 5);
    data.m_DateTimeFlags = (QKnxDateTime::DateTimeFlags)flags;

    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const QKnxDateTime &data)
{
    quint8 year, month, day, hour, minute, second;

    QDate date(data.m_DateTime.date());
    QTime time(data.m_DateTime.time());

    day = (quint8)date.day() & 0b00011111;
    month = (quint8)date.month() & 0b00001111;
    year = (quint8)(date.year() - 1900);

    hour = (quint8)time.hour() & 0b00011111;
    hour |= (((quint8)data.m_DayOfWeek) << 5) & 0b11100000;
    minute = (quint8)time.minute() & 0b00111111;
    second = (quint8)time.second() & 0b00111111;

    fs << year << month << day << hour << minute << second << (quint16)data.m_DateTimeFlags;

    return fs;
}

QDebug operator<<(QDebug dbg, const QKnxDateTime &data)
{
    dbg.nospace() << data.m_DateTime << ", " << data.m_DayOfWeek << ", " << data.m_DateTimeFlags;
    return dbg;
}


