#ifndef QKNXDATETIME_H
#define QKNXDATETIME_H

#include "qknxdate.h"
#include "qknxtimeofday.h"

#include <QDateTime>

class QKNX_SHARED_EXPORT QKnxDateTime : public QKnxDpt::ValueTypeCRTP<QKnxDateTime>
{
    Q_GADGET
public:
    enum DateTimeFlag
    {
        // Byte 8
        FlagsNone = 0,
        Reserved1 = 1 << 0,
        Reserved2 = 1 << 1,
        Reserved3 = 1 << 2,
        Reserved4 = 1 << 3,
        Reserved5 = 1 << 4,
        Reserved6 = 1 << 5,
        Reserved7 = 1 << 6,
        QualityOfClock = 1 << 7,
        // Byte 7
        StandardSummerTime = 1 << 8,
        NoTime = 1 << 9,
        NoDayOfWeek = 1 << 10,
        NoDate = 1 << 11,
        NoYear = 1 << 12,
        NoWD = 1 << 13,
        WorkingDay = 1 << 14,
        Fault = 1 << 15
    };
    Q_DECLARE_FLAGS(DateTimeFlags,DateTimeFlag)
    Q_FLAG(DateTimeFlags)

    QKnxDateTime() : QKnxDpt::ValueTypeCRTP<QKnxDateTime>(), m_DateTime(), m_DayOfWeek(NoDay) {}
    QKnxDateTime(const QDateTime& date_time, Qt::DayOfWeek day_of_week = NoDay, DateTimeFlags flags = FlagsNone) : QKnxDpt::ValueTypeCRTP<QKnxDateTime>(), m_DateTime(date_time), m_DayOfWeek(day_of_week), m_DateTimeFlags(flags) {}
    QKnxDateTime(const QDate& date, const QTime& time, Qt::DayOfWeek day_of_week = NoDay, DateTimeFlags flags = FlagsNone) : QKnxDpt::ValueTypeCRTP<QKnxDateTime>(), m_DateTime(QDateTime(date,time)), m_DayOfWeek(day_of_week), m_DateTimeFlags(flags) {}
    QKnxDateTime(const QKnxDate& date, const QKnxTimeOfDay& time, DateTimeFlags flags = FlagsNone) : QKnxDpt::ValueTypeCRTP<QKnxDateTime>(), m_DateTime(date,time), m_DayOfWeek(time), m_DateTimeFlags(flags) {}
    virtual ~QKnxDateTime() {}

    static const Qt::DayOfWeek NoDay = (Qt::DayOfWeek)0;
    static const Qt::DayOfWeek AutoDay = (Qt::DayOfWeek)-1;

    operator QVariant() const { return QVariant::fromValue<QKnxDateTime>(*this); }

    QDateTime dateTime() const { return m_DateTime; }
    void setDateTime(const QDateTime& date_time) { m_DateTime = date_time; }
    operator QDateTime() const { return m_DateTime; }
    QKnxDateTime& operator =(const QDateTime& date_time) { m_DateTime = date_time; return *this; }

    QDate date() const { return m_DateTime.date(); }
    void setDate(const QDate& date) { m_DateTime.setDate(date); }
    operator QDate() const { return m_DateTime.date(); }
    QKnxDateTime& operator =(const QDate& date) { m_DateTime.setDate(date); return *this; }

    QTime time() const { return m_DateTime.time(); }
    void setTime(const QTime& time) { m_DateTime.setTime(time); }
    operator QTime() const { return m_DateTime.time(); }
    QKnxDateTime& operator =(const QTime& time) { m_DateTime.setTime(time); return *this; }

    Qt::DayOfWeek dayOfWeek() const { return m_DayOfWeek; }
    void setDayOfWeek(const Qt::DayOfWeek& day_of_week) { m_DayOfWeek = day_of_week; }
    operator Qt::DayOfWeek() const { return m_DayOfWeek; }
    QKnxDateTime& operator =(const Qt::DayOfWeek& day_of_week) { m_DayOfWeek = day_of_week; return *this; }

    DateTimeFlags flags() const { return m_DateTimeFlags; }
    void setFlags(const DateTimeFlags& date_time_flags) { m_DateTimeFlags = date_time_flags; }
    operator DateTimeFlags() const { return m_DateTimeFlags; }
    QKnxDateTime& operator =(const DateTimeFlags& date_time_flags) { m_DateTimeFlags = date_time_flags; return *this; }

    QKnxDate knxDate() const { return m_DateTime.date(); }
    void setKnxDate(const QKnxDate& date) { m_DateTime.setDate(date); }
    operator QKnxDate() const { return m_DateTime.date(); }
    QKnxDateTime& operator =(const QKnxDate& date) { m_DateTime.setDate(date); return *this; }

    QKnxTimeOfDay knxTimeOfDay() const { return QKnxTimeOfDay(m_DateTime.time(),m_DayOfWeek); }
    void setKnxTimeOfDay(const QKnxTimeOfDay& time) { m_DateTime.setTime(time); m_DayOfWeek = time; }
    operator QKnxTimeOfDay() const { return QKnxTimeOfDay(m_DateTime.time(),m_DayOfWeek); }
    QKnxDateTime& operator =(const QKnxTimeOfDay& time) { m_DateTime.setTime(time); m_DayOfWeek = time; return *this; }

    static QDateTime minDateTime() { return QDateTime(minDate(),minTime()); }
    static QDateTime maxDateTime() { return QDateTime(maxDate(),maxTime()); }

    static QDate minDate() { return QDate(1900,1,1); }
    static QDate maxDate() { return QDate(2155,12,31); }

    static QTime minTime() { return QTime(0,0,0,0); }
    static QTime maxTime() { return QTime(23,59,59,999); }

    static Qt::DayOfWeek minDayOfWeek() { return NoDay; }
    static Qt::DayOfWeek maxDayOfWeek() { return Qt::Sunday; }

    static QKnxDateTime min() { return QKnxDateTime(minDateTime(), minDayOfWeek()); }
    static QKnxDateTime max() { return QKnxDateTime(maxDateTime(), maxDayOfWeek()); }

    bool isValid() const;

private:
    QDateTime m_DateTime;
    Qt::DayOfWeek m_DayOfWeek;
    DateTimeFlags m_DateTimeFlags;

    friend QKnxStream& operator>>(QKnxStream& fs, QKnxDateTime& data);
    friend QKnxStream& operator<<(QKnxStream& fs, const QKnxDateTime& data);

    friend QDebug operator<<(QDebug dbg, const QKnxDateTime &data);
};
Q_DECLARE_OPERATORS_FOR_FLAGS(QKnxDateTime::DateTimeFlags)

template <> QKNX_SHARED_EXPORT QKnxDpt::Type QKnxDpt::type<QKnxDateTime>();

QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QKnxDateTime& data);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const QKnxDateTime& data);

QKNX_SHARED_EXPORT QDebug operator<<(QDebug dbg, const QKnxDateTime &data);

#endif // QKNXDATETIME_H
