#include "qknxfloat16.h"

#include "qknxstream.h"

#include <cmath>

static bool register_knxtype = QKnxDpt::registerType<QKnxFloat16>(&register_knxtype);

QKnxFloat16::QKnxFloat16() : QKnxDpt::ValueTypeCRTP<QKnxFloat16>(), m_Value(0)
{

}

QKnxFloat16::QKnxFloat16(const float &r) : QKnxDpt::ValueTypeCRTP<QKnxFloat16>(), m_Value(0)
{
    setValue(r);
}

QKnxFloat16::~QKnxFloat16()
{

}

QKnxFloat16 &QKnxFloat16::operator =(const float &r)
{
    setValue(r);
    return *this;
}

void QKnxFloat16::setValue(const float &r)
{
    float v = r * 100.0f;
    quint16 e = 0;
    float d = v/pow(2.0,e);
    while(d < -2048.0f || d > 2047.0f) {
        e++;
        d = v/pow(2.0,e);
    }
    //quint16 m = round(d); // TODO: ceil or round? ETS5 seems to use ceiled value here, round fits closer when recovering to float
    quint16 m = roundf(d);
    m_Value =  m & 0x07FF;
    m_Value |= (e & 0x0F) << 11;
    if (r < 0.0f) {
        m_Value |= 0x8000;
    }
}

QKnxFloat16::operator float() const
{
    quint16 e = (m_Value >> 11) & 0x0F;
    quint16 m = m_Value & 0x07FF;
    if (m_Value & 0x8000) {
        return ((-2048 + m) * 0.01) * pow(2.0, e);
    }
    return (m * 0.01) * pow(2.0, e);
}

template<> QKnxDpt::Type QKnxDpt::type<QKnxFloat16>() { return QKnxDpt::Type2ByteFloat; }

QKnxStream &operator>>(QKnxStream &fs, QKnxFloat16 &data)
{
    quint16 d;
    fs >> d;
    data.setData(d);
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const QKnxFloat16 &data)
{
    fs << data.data();
    return fs;
}

QDebug operator<<(QDebug dbg, const QKnxFloat16 &data)
{
    float out = data;
    dbg << out;
    return dbg;
}

