#ifndef QKNXFLOAT16_H
#define QKNXFLOAT16_H

#include "dpt/qknxdpt.h"

class QKNX_SHARED_EXPORT QKnxFloat16 : public QKnxDpt::ValueTypeCRTP<QKnxFloat16>
{
    Q_GADGET
public:
    QKnxFloat16();
    QKnxFloat16(const float& r);
    virtual ~QKnxFloat16();

    operator QVariant() const { return QVariant::fromValue<QKnxFloat16>(*this); }

    QKnxFloat16& operator =(const float& r);

    operator float() const;

    quint16 data() const { return m_Value; }
    void setData(const quint16& data) { m_Value = data; }

    static QKnxFloat16 min() { QKnxFloat16 f; f.setData((quint16)0xF800); return f; }
    static QKnxFloat16 max() { QKnxFloat16 f; f.setData((quint16)0x7FFF); return f; }

private:
    quint16 m_Value;

    void setValue(const float& r);

    friend QKnxStream& operator>>(QKnxStream& fs, QKnxFloat16& data);
    friend QKnxStream& operator<<(QKnxStream& fs, const QKnxFloat16& data);

    friend QDebug operator<<(QDebug dbg, const QKnxFloat16 &data);
};

template <> QKNX_SHARED_EXPORT QKnxDpt::Type QKnxDpt::type<QKnxFloat16>();

QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QKnxFloat16& data);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const QKnxFloat16& data);

QKNX_SHARED_EXPORT QDebug operator<<(QDebug dbg, const QKnxFloat16 &data);

#endif // QKNXFLOAT16_H
