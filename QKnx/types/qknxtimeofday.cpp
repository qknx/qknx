#include "qknxtimeofday.h"

#include "qknxstream.h"

static bool register_knxtype = QKnxDpt::registerType<QKnxTimeOfDay>(&register_knxtype);

template<> QKnxDpt::Type QKnxDpt::type<QKnxTimeOfDay>() { return QKnxDpt::Type3ByteTime; }

QKnxStream &operator>>(QKnxStream &fs, QKnxTimeOfDay &data)
{
    quint8 hour, minute, second;
    fs >> hour >> minute >> second;
    data.m_Time.setHMS(hour & 0b00011111, minute & 0b00111111, second & 0b00111111);
    data.m_DayOfWeek = (Qt::DayOfWeek)((hour & 0b11100000) >> 5);
    return fs;
}

QKnxStream &operator<<(QKnxStream &fs, const QKnxTimeOfDay &data)
{
    quint8 hour, minute, second;
    hour = (quint8)data.m_Time.hour() & 0b00011111;
    hour |= (((quint8)data.m_DayOfWeek) << 5) & 0b11100000;
    minute = (quint8)data.m_Time.minute() & 0b00111111;
    second = (quint8)data.m_Time.second() & 0b00111111;
    fs << hour << minute << second;
    return fs;
}

QDebug operator<<(QDebug dbg, const QKnxTimeOfDay &data)
{
    dbg.nospace() << data.m_Time << ", " << data.m_DayOfWeek;
    return dbg;
}


