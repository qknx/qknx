#ifndef QKNXTIMEOFDAY_H
#define QKNXTIMEOFDAY_H

#include "dpt/qknxdpt.h"

#include <QTime>

class QKNX_SHARED_EXPORT QKnxTimeOfDay : public QKnxDpt::ValueTypeCRTP<QKnxTimeOfDay>
{
    Q_GADGET
public:
    QKnxTimeOfDay() : QKnxDpt::ValueTypeCRTP<QKnxTimeOfDay>(), m_Time(), m_DayOfWeek(NoDay) {}
    QKnxTimeOfDay(const QTime& time, Qt::DayOfWeek day_of_week = NoDay) : QKnxDpt::ValueTypeCRTP<QKnxTimeOfDay>(), m_Time(time), m_DayOfWeek(day_of_week) {}
    virtual ~QKnxTimeOfDay() {}

    static const Qt::DayOfWeek NoDay = (Qt::DayOfWeek)0;

    operator QVariant() const { return QVariant::fromValue<QKnxTimeOfDay>(*this); }

    QTime time() const { return m_Time; }
    void setTime(const QTime& time) { m_Time = time; }
    operator QTime() const { return m_Time; }
    QKnxTimeOfDay& operator =(const QTime& time) { m_Time = time; return *this; }

    Qt::DayOfWeek dayOfWeek() const { return m_DayOfWeek; }
    void setDayOfWeek(const Qt::DayOfWeek& day_of_week) { m_DayOfWeek = day_of_week; }
    operator Qt::DayOfWeek() const { return m_DayOfWeek; }
    QKnxTimeOfDay& operator =(const Qt::DayOfWeek& day_of_week) { m_DayOfWeek = day_of_week; return *this; }

    static QTime minTime() { return QTime(0,0,0,0); }
    static QTime maxTime() { return QTime(23,59,59,999); }

    static Qt::DayOfWeek minDayOfWeek() { return NoDay; }
    static Qt::DayOfWeek maxDayOfWeek() { return Qt::Sunday; }

    static QKnxTimeOfDay min() { return QKnxTimeOfDay(minTime(), minDayOfWeek()); }
    static QKnxTimeOfDay max() { return QKnxTimeOfDay(maxTime(), maxDayOfWeek()); }

    bool isValid() const { return (m_Time.isValid() && m_DayOfWeek >= minDayOfWeek() && m_DayOfWeek <= maxDayOfWeek()); }

private:
    QTime m_Time;
    Qt::DayOfWeek m_DayOfWeek;

    friend QKnxStream& operator>>(QKnxStream& fs, QKnxTimeOfDay& data);
    friend QKnxStream& operator<<(QKnxStream& fs, const QKnxTimeOfDay& data);

    friend QDebug operator<<(QDebug dbg, const QKnxTimeOfDay &data);
};

template <> QKNX_SHARED_EXPORT QKnxDpt::Type QKnxDpt::type<QKnxTimeOfDay>();

QKNX_SHARED_EXPORT QKnxStream& operator>>(QKnxStream& fs, QKnxTimeOfDay& data);
QKNX_SHARED_EXPORT QKnxStream& operator<<(QKnxStream& fs, const QKnxTimeOfDay& data);

QKNX_SHARED_EXPORT QDebug operator<<(QDebug dbg, const QKnxTimeOfDay &data);

#endif // QKNXTIMEOFDAY_H
