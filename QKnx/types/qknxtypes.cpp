#include "qknxtypes.h"

#include <climits>

template<> QKnxDpt::Type QKnxDpt::type<QKnxChar>() { return QKnxDpt::Type1ByteChar; }
QDebug operator<<(QDebug dbg, const QKnxChar &data) { dbg.nospace() << (qint8)data.data(); return dbg; }
static bool register_knxtype_char = QKnxDpt::registerType<QKnxChar>(&register_knxtype_char);

// int8
template<> QKnxDpt::Type QKnxDpt::type<QKnxInt8>() { return QKnxDpt::Type1ByteInt; }
static bool register_knxtype_int8 = QKnxDpt::registerType<QKnxInt8>(&register_knxtype_int8);

// uint8
template<> QKnxDpt::Type QKnxDpt::type<QKnxUInt8>() { return QKnxDpt::Type1ByteUInt; }
static bool register_knxtype_uint8 = QKnxDpt::registerType<QKnxUInt8>(&register_knxtype_uint8);

// int16
template<> QKnxDpt::Type QKnxDpt::type<QKnxInt16>() { return QKnxDpt::Type2ByteInt; }
static bool register_knxtype_int16 = QKnxDpt::registerType<QKnxInt16>(&register_knxtype_int16);

// uint16
template<> QKnxDpt::Type QKnxDpt::type<QKnxUInt16>() { return QKnxDpt::Type2ByteUInt; }
static bool register_knxtype_uint16 = QKnxDpt::registerType<QKnxUInt16>(&register_knxtype_uint16);

// int32
template<> QKnxDpt::Type QKnxDpt::type<QKnxInt32>(){ return QKnxDpt::Type4ByteInt; }
static bool register_knxtype_int32 = QKnxDpt::registerType<QKnxInt32>(&register_knxtype_int32);

// uint32
template<> QKnxDpt::Type QKnxDpt::type<QKnxUInt32>() { return QKnxDpt::Type4ByteUInt; }
static bool register_knxtype_uint32 = QKnxDpt::registerType<QKnxUInt32>(&register_knxtype_uint32);

// float 32
template<> QKnxDpt::Type QKnxDpt::type<QKnxFloat32>() { return QKnxDpt::Type4ByteFloat; }
template<> QKnxFloat32 QKnxFloat32::min() { return -std::numeric_limits<float>::max(); }
static bool register_knxtype_float32 = QKnxDpt::registerType<QKnxFloat32>(&register_knxtype_float32);

