#ifndef QKNXTYPES_H
#define QKNXTYPES_H

#include "qknxbool.h"
#include "qknxfloat16.h"
#include "qknxdatetime.h"

template <class T> class QKnxType : public QKnxDpt::ValueTypeCRTP<QKnxType<T>>
{

public:
    QKnxType() : QKnxDpt::ValueTypeCRTP<QKnxType<T>>(), m_Value(0) { }
    QKnxType(const T& val) : QKnxDpt::ValueTypeCRTP<QKnxType<T>>(), m_Value(val) { }
    virtual ~QKnxType() {}

    operator QVariant() const { return QVariant::fromValue<QKnxType<T>>(*this); }

    QKnxType<T>& operator =(const T& val) { m_Value = val; return *this; }

    operator T() const { return m_Value; }

    T data() const { return m_Value; }
    void setData(const T& val) { m_Value = val; }

    static QKnxType<T> min() { return QKnxType<T>(std::numeric_limits<T>::min()); }
    static QKnxType<T> max() { return QKnxType<T>(std::numeric_limits<T>::max()); }

private:
    T m_Value;

    template <class U> friend QKnxStream& operator>> (QKnxStream& fs, QKnxType<U>& data);
    template <class U> friend QKnxStream& operator<< (QKnxStream& fs, const QKnxType<U>& data);

    template <class U> friend QDebug operator<<(QDebug dbg, const QKnxType<U> &data);
};

template <typename T> QKnxStream& operator>>(QKnxStream& fs, QKnxType<T>& data)
{
    fs >> data.m_Value;
    return fs;
}

template <typename T> QKnxStream& operator<<(QKnxStream& fs, const QKnxType<T>& data)
{
    fs << data.m_Value;
    return fs;
}

template <typename T> QDebug operator<<(QDebug dbg, const QKnxType<T>& data)
{
    dbg << data.m_Value;
    return dbg;
}

// char
typedef QKnxType<char> QKnxChar;
template <> QKNX_SHARED_EXPORT QKnxDpt::Type QKnxDpt::type<QKnxChar>();
QKNX_SHARED_EXPORT QDebug operator<<(QDebug dbg, const QKnxChar& data);
Q_DECLARE_METATYPE(QKnxChar)

// int8
typedef QKnxType<qint8> QKnxInt8;
template <> QKNX_SHARED_EXPORT QKnxDpt::Type QKnxDpt::type<QKnxInt8>();
Q_DECLARE_METATYPE(QKnxInt8)

// uint8
typedef QKnxType<quint8> QKnxUInt8;
template <> QKNX_SHARED_EXPORT QKnxDpt::Type QKnxDpt::type<QKnxUInt8>();
Q_DECLARE_METATYPE(QKnxUInt8)

// int16
typedef QKnxType<qint16> QKnxInt16;
template <> QKNX_SHARED_EXPORT QKnxDpt::Type QKnxDpt::type<QKnxInt16>();
Q_DECLARE_METATYPE(QKnxInt16)

// uint16
typedef QKnxType<quint16> QKnxUInt16;
template <> QKNX_SHARED_EXPORT QKnxDpt::Type QKnxDpt::type<QKnxUInt16>();
Q_DECLARE_METATYPE(QKnxUInt16)

// int32
typedef QKnxType<qint32> QKnxInt32;
template <> QKNX_SHARED_EXPORT QKnxDpt::Type QKnxDpt::type<QKnxInt32>();
Q_DECLARE_METATYPE(QKnxInt32)

// uint32
typedef QKnxType<quint32> QKnxUInt32;
template <> QKNX_SHARED_EXPORT QKnxDpt::Type QKnxDpt::type<QKnxUInt32>();
Q_DECLARE_METATYPE(QKnxUInt32)

// float 32
typedef QKnxType<float> QKnxFloat32;
template <> QKNX_SHARED_EXPORT QKnxDpt::Type QKnxDpt::type<QKnxFloat32>();
template <> QKNX_SHARED_EXPORT QKnxFloat32 QKnxFloat32::min();
Q_DECLARE_METATYPE(QKnxFloat32)

#endif // QKNXTYPES_H
