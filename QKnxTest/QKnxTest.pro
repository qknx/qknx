#-------------------------------------------------
#
# Project created by QtCreator 2017-05-22T17:41:41
#
#-------------------------------------------------

include(../QKnx/defaults.pri)

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QKnxTest
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    netipservices.cpp

HEADERS  += mainwindow.h \
    netipservices.h

FORMS    += mainwindow.ui \
    netipservices.ui

# QKnx library
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../QKnx/release/ -lQKnx
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../QKnx/debug/ -lQKnx
else:unix: LIBS += -L$$OUT_PWD/../QKnx/ -lQKnx

INCLUDEPATH += $$PWD/../QKnx
DEPENDPATH += $$PWD/../QKnx
