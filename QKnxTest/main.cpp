#include "mainwindow.h"
#include <QApplication>

#include "qknxglobal.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("NewCron");
    QCoreApplication::setOrganizationDomain("newcron.org");
    QCoreApplication::setApplicationName("QKnxTest");

    // Initialize QKnx
    QKnxGlobal::init();

    MainWindow w;
    w.show();

    return a.exec();
}
