#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    qDebug() << "Initializing test program main window";

    // quit
    connect(ui->actionQuit,SIGNAL(triggered(bool)),qApp,SLOT(quit()));

    // log
    connect(QKnxLog::instance(),SIGNAL(logReceived(QKnxLogMessage)),this,SLOT(acceptLog(QKnxLogMessage)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::acceptLog(QKnxLogMessage msg)
{
    ui->textLog->append(msg.message);
}

