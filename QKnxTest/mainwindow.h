#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "log/qknxlog.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

private slots:
    void acceptLog(QKnxLogMessage msg);

};

#endif // MAINWINDOW_H
