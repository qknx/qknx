#include "netipservices.h"
#include "ui_netipservices.h"

/*

// search
#include "netip/service/qknxnetipsearchrequest.h"
#include "netip/service/qknxnetipsearchresponse.h"

// description
#include "netip/service/qknxnetipdescriptionrequest.h"
#include "netip/service/qknxnetipdescriptionresponse.h"

// connect
#include "netip/service/qknxnetipconnectrequest.h"
#include "netip/service/qknxnetipconnectresponse.h"

// connectionstate
#include "netip/service/qknxnetipconnectionstaterequest.h"
#include "netip/service/qknxnetipconnectionstateresponse.h"

// disconnect
#include "netip/service/qknxnetipdisconnectrequest.h"
#include "netip/service/qknxnetipdisconnectresponse.h"

// tunneling
#include "netip/service/qknxnetiptunnelingrequest.h"

// dibs
#include "netip/data/qknxnetipdeviceinfo.h"
#include "netip/data/qknxnetipsuppsvc.h"

// cri/crdb
#include "netip/data/qknxnetipconnectionrequestinformation.h"
#include "netip/data/qknxnetipconnectionresponsedatablock.h"

// cemi
#include "netip/cemi/qknxnetipldatareq.h"

// frame
#include "netip/qknxnetipframe.h"

*/

#include <QDebug>
#include <QElapsedTimer>


NetIpServices::NetIpServices(QWidget *parent) : QWidget(parent), ui(new Ui::NetIpServices)
{
    ui->setupUi(this);

    qDebug() << "Initializing NetIpServices test";
}

NetIpServices::~NetIpServices()
{
    delete ui;
}

/*

void NetIpServices::on_buttonSearchRequest_clicked()
{
    test<QKnxNetIpSearchRequest>("06100201000e0801c0a80a02c002");
}

void NetIpServices::on_buttonSearchResponse_clicked()
{
    test<QKnxNetIpSearchResponse>("06100202004c0801c0a80ac80e573601020000010000010203040506e000170c08002776d05d6b6e786400000000000000000000000000000000000000000000000000000802020104010501");
}

void NetIpServices::on_buttonDescriptionRequest_clicked()
{
    test<QKnxNetIpDescriptionRequest>("06100203000e0801102030400001");
}

void NetIpServices::on_buttonDescriptionResponse_clicked()
{
    test<QKnxNetIpDescriptionResponse>("0610020400463601020000010000000000000000e000170c08002776d05d6b6e786400000000000000000000000000000000000000000000000000000a020201030104010501");
}

void NetIpServices::on_buttonConnectRequest_clicked()
{
    test<QKnxNetIpConnectRequest>("06100205001a0801c0a80a02c0060801c0a80a02c00704040200");
}

void NetIpServices::on_buttonConnectResponse_clicked()
{
    test<QKnxNetIpConnectResponse>("06100206001401000801c0a80ac80e5704040003");
}

void NetIpServices::on_buttonConnectionStateRequest_clicked()
{
    test<QKnxNetIpConnectionStateRequest>("06100207001001000801c0a80a02c006");
}

void NetIpServices::on_buttonConnectionStateResponse_clicked()
{
    test<QKnxNetIpConnectionStateResponse>("0610020800080100");
}

void NetIpServices::on_buttonDisconnectRequest_clicked()
{
    test<QKnxNetIpDisconnectRequest>("06100209001002000801c0a80a02c006");
}

void NetIpServices::on_buttonDisconnectResponse_clicked()
{
    test<QKnxNetIpDisconnectResponse>("0610020a00080200");
}

void NetIpServices::on_buttonTunnelingRequest_clicked()
{
    test<QKnxNetIpTunnelingRequest>("061004200015040100001100bce000000001010080");
}

void NetIpServices::on_buttonTunnelingAck_clicked()
{

}


template<class T>
void NetIpServices::test(const QString &data)
{
    int meta_id = qMetaTypeId<T>();
    QKnxNetIpService::ServiceId svc_id = QKnxNetIpService::metaIdsByServiceIds().key(meta_id,QKnxNetIpService::SERVICE_NONE);
    if (svc_id == QKnxNetIpService::SERVICE_NONE) {
        qDebug() << "Test failed!";
        return;
    }

    int repeat = ui->spinBoxRepeat->value();

    qDebug() << "=== TEST BEGIN ===";
    qDebug() << " - Service id:" << QKnxNetIpService::serviceIdString(svc_id).toStdString().c_str();
    qDebug() << " - Class:" << QMetaType::typeName(meta_id);
    qDebug() << " - Meta id:" << meta_id;
    qDebug() << " - Input data:" << data.toStdString().c_str();
    qDebug() << " - Repeat:" << repeat;

    QByteArray byte_data = QByteArray::fromHex(data.toLatin1());
    QElapsedTimer t;
    t.start();

    // original data
    QByteArray orig_data;

    for(int x=0; x < repeat; x++) {
        QKnxNetIpFrame netip_data;
        netip_data.setFrame(byte_data);

        if (!netip_data.hasServiceId(svc_id)) {
            qDebug() << " === ERROR ===";
            qDebug() << " - Failed to create QKnxNetIpData object. Data invalid or wrong service type?";
            return;
        }

        QKnxNetIpService* req = (QKnxNetIpService*)netip_data.servicePtr<T>();
        if (!req) {
            qDebug() << " === ERROR ===";
            qDebug() << " - Failed to get valid servicePtr";
            return;
        }

        if (!x)
        {
            qDebug() << "=== DATA ===";

            // structure length
            if (req->hasField(QKnxNetIpService::FIELD_STRUCTURE_LENGTH))
            {
                quint8 struct_len = req->fieldData(QKnxNetIpService::FIELD_STRUCTURE_LENGTH).value<quint8>();
                qDebug() << "Structure Length:" << struct_len;
            }

            // channel id
            if (req->hasField(QKnxNetIpService::FIELD_CHANNEL_ID))
            {
                quint8 channel_id = req->fieldData(QKnxNetIpService::FIELD_CHANNEL_ID).value<quint8>();
                qDebug() << "Channel Id:" << channel_id;
            }

            // sequence counter
            if (req->hasField(QKnxNetIpService::FIELD_SEQ_COUNTER))
            {
                quint8 seq_count = req->fieldData(QKnxNetIpService::FIELD_SEQ_COUNTER).value<quint8>();
                qDebug() << "Sequence Counter:" << seq_count;
            }

            // error
            if (req->hasField(QKnxNetIpService::FIELD_ERROR))
            {
                QKnxNetIpService::ErrorCode error = req->fieldData(QKnxNetIpService::FIELD_ERROR).value<QKnxNetIpService::ErrorCode>();
                qDebug() << "Error:" << req->errorCodeString(error);
                qDebug() << " - Description:" << req->errorCodeDescription(error);
            }

            // endpoints
            QList<QKnxNetIpService::Field> endpointlist;
            endpointlist << QKnxNetIpService::FIELD_CONTROL_ENDPOINT << QKnxNetIpService::FIELD_DISCOVERY_ENDPOINT << QKnxNetIpService::FIELD_DATA_ENDPOINT;
            foreach (QKnxNetIpService::Field epf, endpointlist)
            {
                if (req->hasField(epf))
                {
                    qDebug() << "Endpoint data:";
                    qDebug() << " - Type:" << req->fieldString(epf);
                    qDebug() << " - HostAddress:" << req->fieldData(epf).value<QKnxNetIpEndpoint>().hostAddress();
                    qDebug() << " - Port:" << req->fieldData(epf).value<QKnxNetIpEndpoint>().port();
                    qDebug() << " - Host Protocol Code:" << req->fieldData(epf).value<QKnxNetIpEndpoint>().hostProtocolCodeString();
                    qDebug() << " - isValid?" << req->fieldData(epf).value<QKnxNetIpEndpoint>().isValid();
                }
            }

            // dibs
            if (req->hasField(QKnxNetIpService::FIELD_DIBS)) {
                // device info
                if (req->hasDib<QKnxNetIpDeviceInfo>())
                {
                    qDebug() << "Device Info:";
                    qDebug() << " - Medium:" << req->dib<QKnxNetIpDeviceInfo>().mediumsString();
                    qDebug() << " - Status:" << req->dib<QKnxNetIpDeviceInfo>().statusesString();
                    qDebug() << " - KNX Address:" << req->dib<QKnxNetIpDeviceInfo>().address().toString();
                    qDebug() << " - Project Installation:" << QString::number(req->dib<QKnxNetIpDeviceInfo>().projectInstallation(),16).prepend("0x").toStdString().c_str();
                    qDebug() << " - Serial:" << req->dib<QKnxNetIpDeviceInfo>().serialString();
                    qDebug() << " - MAC Address:" << req->dib<QKnxNetIpDeviceInfo>().macAddress().toString();
                    qDebug() << " - Friendly Name:" << req->dib<QKnxNetIpDeviceInfo>().friendlyName();
                }

                // supp svcs
                if (req->hasDib<QKnxNetIpSuppSvc>())
                {
                    qDebug() << "SuppSvcs:";
                    QKnxNetIpSuppSvc::SuppSvcMap supp_svcs = req->dib<QKnxNetIpSuppSvc>().suppSvcs();
                    foreach (QKnxNetIpSuppSvc::SuppSvc supp_svc, supp_svcs.keys())
                    {
                        QString supp_svc_str = " - %1 (%2): Version %3";
                        supp_svc_str = supp_svc_str.arg(QKnxNetIpSuppSvc::suppSvcDescription(supp_svc));
                        supp_svc_str = supp_svc_str.arg(QKnxNetIpSuppSvc::suppSvcString(supp_svc));
                        supp_svc_str = supp_svc_str.arg(supp_svcs.value(supp_svc));
                        qDebug() << supp_svc_str.toStdString().c_str();
                    }
                }
            }

            // cri
            if (req->hasField(QKnxNetIpService::FIELD_CRI))
            {
                QKnxNetIpConnectionRequestInformation cri = req->fieldData(QKnxNetIpService::FIELD_CRI).value<QKnxNetIpConnectionRequestInformation>();
                qDebug() << "Connection Request Information";
                qDebug() << "Connection type:" << cri.connectionTypeString();
                if (cri.connectionType() == QKnxNetIpService::TUNNEL_CONNECTION)
                {
                    qDebug() << "Link layers:" << cri.linkLayersString();
                    qDebug() << "Reserved:" << cri.reserved();
                }
            }

            // crdb
            if (req->hasField(QKnxNetIpService::FIELD_CRDB))
            {
                QKnxNetIpConnectionResponseDataBlock crdb = req->fieldData(QKnxNetIpService::FIELD_CRI).value<QKnxNetIpConnectionResponseDataBlock>();
                qDebug() << "Connection Response Data Block";
                qDebug() << "Connection type:" << crdb.connectionTypeString();
                if (crdb.connectionType() == QKnxNetIpService::TUNNEL_CONNECTION)
                {
                    qDebug() << "Address:" << crdb.address().toString();
                }
            }

            // cemi
            if (req->hasField(QKnxNetIpService::FIELD_CEMI))
            {
                if (req->hasCemi<QKnxNetIpLDataReq>()) {
                    QKnxNetIpLDataReq* l_data_req = req->cemiPtr<QKnxNetIpLDataReq>();
                    if (l_data_req) {
                        qDebug() << "cEMI type:" << l_data_req->messageCode();
                        qDebug() << "Telegram:";
                        qDebug() << " - Frame type:" << l_data_req->telegram().frameTypeString();
                        qDebug() << " - Repeated:" << l_data_req->telegram().repeatedString();
                        qDebug() << " - Broadcast:" << l_data_req->telegram().broadcastString();
                        qDebug() << " - Priority:" << l_data_req->telegram().priorityString();
                        qDebug() << " - AckReq:" << l_data_req->telegram().ackRequest();
                        qDebug() << " - Error:" << l_data_req->telegram().error();
                        qDebug() << " - Routing count:" << l_data_req->telegram().routingCountString();
                        qDebug() << " - LTExt:" << l_data_req->telegram().ltextString();
                        qDebug() << " - Source:" << l_data_req->telegram().source().toString();
                        qDebug() << " - Target:" << l_data_req->telegram().target().toString();
                        qDebug() << " - Command:" << l_data_req->telegram().commandString();
                        qDebug() << " - Content Length:" << l_data_req->telegram().contentLength();
                        qDebug() << " - Content:" << QString(l_data_req->telegram().content().toHex());
                    }
                }
            }
        }

        // write back netip to orig data
        orig_data = netip_data.toByteArray();
    } // for loop

    // result time
    float msecs = (float)t.nsecsElapsed() / 1000000;
    float msecs_per_run = (float)msecs/repeat;
    float runs_per_msec = (float)repeat/msecs;

    // serialization
    qDebug() << "=== SERIALIZATION ===";
    qDebug() << "Serialized Data:" << QString(orig_data.toHex());
    qDebug() << " => Matches input data?" << (orig_data == byte_data);

    // results
    qDebug() << "=== RESULT ===";
    qDebug() << QString("Elapsed %1 tests in %2 msecs").arg(repeat).arg(msecs).toStdString().c_str();
    qDebug() << QString(" => %1 msecs/test").arg(msecs_per_run).toStdString().c_str();
    qDebug() << QString(" => %2 tests/msec").arg(runs_per_msec).toStdString().c_str();
    qDebug() << "=== TEST END ===";
}



*/
