#ifndef NETIPSERVICES_H
#define NETIPSERVICES_H

#include <QWidget>

namespace Ui {
class NetIpServices;
}

class NetIpServices : public QWidget
{
    Q_OBJECT

public:
    explicit NetIpServices(QWidget *parent = 0);
    ~NetIpServices();
/*
private slots:
    void on_buttonSearchRequest_clicked();
    void on_buttonSearchResponse_clicked();

    void on_buttonDescriptionRequest_clicked();
    void on_buttonDescriptionResponse_clicked();

    void on_buttonConnectRequest_clicked();
    void on_buttonConnectResponse_clicked();

    void on_buttonConnectionStateRequest_clicked();
    void on_buttonConnectionStateResponse_clicked();

    void on_buttonDisconnectRequest_clicked();
    void on_buttonDisconnectResponse_clicked();

    void on_buttonTunnelingRequest_clicked();
    void on_buttonTunnelingAck_clicked();
*/
private:
    Ui::NetIpServices *ui;

  //  template <class T>
  //  void test(const QString& data);
};

#endif // NETIPSERVICES_H
