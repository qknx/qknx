Multi-Platform EIB/KNX Library using TP-UART and UDP
=====================================

## Intro ##

* Version 0.0.4
* This library is going to be a multi-platform and multi-driver KNX/EIB access library.  
* THIS IS EARLY ALPHA. DO NOT USE PRODUCTIVE!  
* Requires: Qt 5.7.1 (Default Qt5 on Debian 9)

## Hardware ##

### Siemens BCU 5WG1 117-2AB12 @ Raspberry Pi GPIO UART ###

* Using level shifter and voltage divider (circuit diagram coming soon)  
* Can be placed nicely into a din rail enclosure (pictures coming soon)  

### Any Hardware which is able to run Qt apps and has an UDP stack, e.g. ###

* Raspberry Pi  
 * Windows PC  
 * Linux PC  
 * Mac  
 * Android smartphones or tablets  
 * iPhones and iPads  

## Software ##

### The software consists of 3 parts: ###

* QKNXBase - the library
* QKNXTest - test suite for developing and testing
* QHydroGrow - A hydroponics controller application
* QCareTaker - Home automation app for pc and android phones

## Issues, Comments and Suggestions ##

* Just contact me :)
